# How to contribute

[< back to readme](README.md)

You want to contribute? Awesome. Lets get through the gritty details so that we can get your code
home! This guide should help you at least a little bit in getting there, you might need to add some
elbow-grease and some patience as well.

## TOC

1. [Getting Started](#getting-started)
1. [Set Up Local Environment](#set-up-local-environment)
1. [Making Changes](#making-changes)
1. [Testing Changes](#testing-changes)
1. [Submitting Changes](#submitting-changes)
1. [Get Help](#get-help)

## Getting Started

Submitting changes follows a simple process can be summarized in the following diagram:

```mermaid
%%{init: {'flowchart': {'htmlLabels': false}, 'theme': 'dark'}}%%
flowchart LR

fork ---> code ---> test ---> lint ---> pr[pull request]
```

Your first step will be to fork the repository, which means you will need a
[gitlab account](https://gitlab.com/). Once you fork the repository you will need to can begin
making your changes.

## Set Up Local Environment

To setup a local environment for development you will need to have the following installed:

- [Git](https://git-scm.com)
- [Python (3.10+)](https://www.python.org/)
- [Poetry](https://python-poetry.org)

Once the prerequisites are installed, you can setup the environment by following the steps bellow:

1. Clone git repository

   ```bash
   git clone https://gitlab.com/valsr/mikrotik-firewall2mermaid /some/path
   ```

1. Open terminal/shell inside the cloned repository and then run the following to setup the project:

   ```bash
   poetry install
   ```

## Generating Graphs/Running

Once you have setup the system you can execute the application by running `poetry run app ...`.

```bash
poetry run app --input mikrotik_export.rsc

# will only output graph details for the filter firwall section and only input chain (and sub-chains)
poetry run app --input mikrotik_export.rsc --graph filter:input,-:
```

This will parse the export file `mikrotik_export.rsc` and generate a mermaid file
`mikrotik_export.mmd` based on the firewall rules.

For other options see [Command Line Options](README.md#command-line-options).

## Making Changes

How you do your work is up to you, we don't have many rules to follow but if you would want to make
the entire PR process smoothers follow the general steps below:

1. Create a topic branch from where you want to base your work
   - This is usually the **main** branch
   - Only target **release branches** if you are certain your fix must be on that branch
1. Make commits of *logical* and *atomic* units
1. Make sure your commit messages are in the proper format
1. Make sure you have added the necessary tests for your changes

### Making Trivial Changes

For trivial changes (documentation updates, typo corrections, any small non-code changes) you can
omit most of the requirements - just fork/branch-commit-pull request will suffice. If the change is
not a minor one, then your pull request will be rejected and you will need to resubmit using the
standard process.

## Testing Changes

Before submitting your changes, make sure that all tests pass. To run the tests:

```bash
poetry run pytest src
```

In addition add tests for your modifications and ensure that a good enough coverate is followed
(70%+ for non-critical code, 90%+ for critical code).

## Submitting Changes

Once your changes are ready to go you will need to do the following:

1. Rebase your changes on top of the most recent version of the branch you want to merge to
1. Verify your changes are okay:
   - Make sure they are well documented
   - Make sure you have tested them locally
   - Make sure the project builds and installs/updates properly
1. Create a pull request and follow the request template
1. Once your changes have been reviewed you might need to perform corrections based on the feedback
  from the PR

If any of these stages fail, you will need to amend your PR to address the issues found.

## Get Help

If you need more help with any aspect of the project or how to submit your work you can create an
issue and tag it as a `question`!
