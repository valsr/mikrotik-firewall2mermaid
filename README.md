# firewall2mermaid

[![pipeline status](https://gitlab.com/valsr/mikrotik-firewall2mermaid/badges/main/pipeline.svg)](https://gitlab.com/valsr/mikrotik-firewall2mermaid/-/commits/main)
[![coverage report](https://gitlab.com/valsr/mikrotik-firewall2mermaid/badges/main/coverage.svg)](https://gitlab.com/valsr/mikrotik-firewall2mermaid/-/commits/main)
[![Latest Release](https://gitlab.com/valsr/mikrotik-firewall2mermaid/-/badges/release.svg)](https://gitlab.com/valsr/mikrotik-firewall2mermaid/-/releases)

Generate mermaid graphs for MikroTik firewall rules.

## Installing Application

Download the latest release from https://gitlab.com/valsr-personal/mikrotik-firewall2mermaid/-/releases.
Once downloaded you can install it by

```bash
pip install firweall2mermaid-x.x.x-py3-none-any.whl
```

## Generating Graphs/Running

Once you have setup the system you can simply execute the poetry scripts as follow:

```bash
firewall2mermaid --input mikrotik_export.rsc
```

This will parse the export file `mikrotik_export.rsc` and generate a mermaid file
`mikrotik_export.mmd` based on the firewall rules. For advance usage see
[Advance Usage](#advance-usage).

### Command Line Options

> You can always use `-h` to see command help

You can customize the output generation by setting the following option/flags:

| argument                            | description                                                        |
| ----------------------------------- | ------------------------------------------------------------------ |
| -h, --help                          | The build in help screen                                           |
| --version                           | Application version                                                |
| --input                             | Miktoritk input file                                               |
| --output                            | Path to the output file (defaults to `<input>.mmd`)                |
| --graph                             | Graph selector/specified (See [Graph Selectors](#graph-selectors)) |
| --comments                          | Determine how to handle comments on graph nodes                    |
| --show-logs, --no-show-logs         | Whether to show nodes of action="log"                              |
| --show-disabled, --no-show-disabled | whether to show disabled nodes                                     |
| --show-legend, --no-show-legend     | Whether to shoe the node type legend                               |
| --direction                         | Graph direction                                                    |
| --log-rules                         | Whether to log the rules as comments at the start of the graph     |
| --flow, --add-flow-elements         | Add upstream/downstream flow elements                              |
| --colapse-as-rule                   | Each collapse element will be modeled as rule                      |
| --rootless                          | Do not render the root outline/box                                 |
| --log-command                       | Log the executing command as comment in the graph                  |
| -v, --verbose                       | Increase verbosity                                                 |

## Notes

### Output file

When not specified the output file will be generated based on the input file by replacing the
file extension, typically `.rsc`, with `.mmd`

```bash
firewall2mermaid --input /path/to/my/file.rsc # Output will be `/path/to/my/file.mmd`
```

## Advance Usage

### Graph Selectors

You can use graph selectors to alter how the firewall rules are render. With selectors you can set
which sections/chains are rendered, which are hidden, which are collapsed and which are splatted.
Selectores are specified in the `--graph` flag in a comma separated value list. Selectors are
applied in the order they are read, with first selector having the highest precedence, while the
last selector having the lower precedence. If, by chance, no selector matches the section/chain, it
will default to render/show it. For that reason, when using `--graph` flag alway include the default
`:` selector as the very last one.

Each selector is broken down into three parts - `[mod][section]:[chain]`. The modifier `[mod]` is
operation to apply to the given section/chain and is one of the following:

- +: Render the section/chain normally (can also be omitted)
- *: Splat the given section/chain - [Splatting Sections/Chains](#splatting-sectionschains)
- %: Collapsed the given section/chain - [Collapsing Sections/Chains](#collapsing-sectionschains)
- -: Hide the given section/chain - [Hidding Sections/Chains](#hidding-sectionschains)

The `[section]`/`[chain]` specifier will match the given section and/or chains. If omitted, the
selector will assume to match all section/chains. Note that in order to omit the section specifier
you will either need to specify a chain `:input` or the global selectors `:`, `-` (any modification
character on their own).

```bash
... --graph filter # Renders the filter section
... --graph filter:input,-:input # Render the filter input chain, hides all other input chains
... --graph filter:input,-:input,%: # Render the filter input chain, hides all other input chains, collapsed all other chains
```

```bash
firewall2mermaid --input file.rsc --graph +filter,-:input,*mangle:,%:
# The selectors above will do the following:
# 1. Render `filter` section
# 2. Hide/remove all chains named `input`
# 3. Splat all chains in `mangle` (except input due to precedence)
# 4. Collapse remaining chains
```

### Hidding Sections/Chains

Hidding sections and chains removes them from the rendered graph completely. This include removing
links, rules that point to sections/chains. In many cases the global selector `-:` is used to remove
all other chains not already part of the preceding selectors.

```bash
firewall2mermaid --input file.rsc --graph -filter,-:input,+:
# In here `-filter`: Removes filter section and its decendants (chains/rules) as well as their
# links. `-:input` removes all chains named `input` as well as its decendants (chains/rules).
```

### Collapsing Sections/Chains

Collapsing a section/chain replace the entire section/chain with a single node with the chain name.
This is useful to simplify the output diagram by removing nodes while still maintaining
relationships between all nodes. When collapsing nodes, all links will be repointed to the collapsed
node - e.g. jump rule link will now be pointing form the collapsed parent.

Note `collapsed-as-rule` modifies how collapsed nodes are rendered. This option will model collapsed
nodes as a rule place in the chains where the node is linked from thus forming a continuation of the
chain.

### Splatting Sections/Chains

Splatting allows to take a chain and render it as its own parent node - i.e. treat it as a section
node. This is useful when wanting to simplify the diagram by decreesing the nesting of chains inside
sections.

### Flow

This option will add default the missing setions and chains from the packet flow perspective. That
is it will add a node for the following missed chains:

- Prerouting chians for Mangle, and NAT (called dstnat) sections
- Input chains for Mangle and Filter sections
- Forward chains for Mangle and Filter sections
- Output chains for Mangle and Filter sections
- Postrouting chains for Mangle and NAT (called srcnat)

The addition will respect all graph selectors except the global graph selectors.

### Contributing

See [Contributing](Contributing.md) if you are planning to contribute (i.e. submit PR/MR/Patches).
