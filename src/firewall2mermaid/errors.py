"""Error definitions for application."""


class ApplicationError(Exception):
    """Custom exception class for application errors.

    This class represents an exception that is raised when an error occurs within the application.
    It inherits from the base Exception class.

    Attributes:
        message (str): The error message associated with the exception.
    """

    def __init__(self, message):
        self.message = message

    def __str__(self) -> str:
        return self.message


class InvalidGraphSelector(ApplicationError):
    """Invalid graph selector."""

    def __init__(self, selector):
        super().__init__(f"Invalid graph selector '{selector}'")
        self.selector = selector


class ElementNotFound(ApplicationError):
    """Element not found exception."""

    def __init__(self, name):
        super().__init__(f"Invalid element '{name}'")
        self.element = name


class SectionAlreadyExistsError(ApplicationError):
    """Raised when attempted to add a section that already exists in the current root node."""

    def __init__(self, name):
        super().__init__(f"Section '{name}' already exists in the root node")
        self.name = name


class ChainAlreadyExistsError(ApplicationError):
    """Raised when attempted to add a chain that already exists in the current section."""

    def __init__(self, name, section):
        super().__init__(f"Chain '{name}' already exists in {section}")
        self.name = name
