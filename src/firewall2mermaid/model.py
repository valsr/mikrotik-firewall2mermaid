"""Graph AST model"""
from __future__ import annotations
from functools import total_ordering
import logging
from typing import Any, Mapping

from firewall2mermaid.errors import (
    ChainAlreadyExistsError,
    ElementNotFound,
    SectionAlreadyExistsError,
)


DETACHED = "detached"


class GraphSelector:
    """Used to modify graph representation for specific nodes. The selector is divided into two
    parts - the node selection (id_selector) and graph modifier (mod). The id_selector is used to
    denote which nodes should be modified by this selector and follows the format of
    section:chain:rule (assuming wildcard when any of them are missing). The modifiers are any
    combination of modifying characters (%, ^, *, etc). Note that this class doesn't directly modify
    the graph but is used by the TreeMaker and Renderer to determine how to create/render the
    graph."""

    def __init__(self, id_selector: str, mod: str = ""):
        """
        Initializes a GraphSelector object.

        Args:
            id_selector (str): Selectors.
            mod (str, optional): Modifier characters. Defaults to "".
        """
        self._mod = mod or ""
        """Modifier characters"""
        self._id_components = (id_selector or "").strip().split(":")
        """Identifier name"""
        self._empty = not any(x for x in self._id_components if x)
        self._depth = len(self._id_components)

    def __repr__(self):
        selector = ":".join(x or "*" for x in self._id_components)
        return f"GraphSelector({selector}=>{self._mod})"

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, GraphSelector):
            return NotImplemented
        return self._mod == other._mod and self._id_components == other._id_components

    @property
    def selectors(self):
        """Returns a list of the id components used as selectors."""
        return list(self._id_components)

    @property
    def depth(self):
        """Returns the selector depth (number of id components)."""
        return self._depth

    @property
    def empty(self):
        """Returns True if all id components are empty."""
        return self._empty

    @property
    def default(self):
        """Returns True if this is the default selector (::)."""
        return self.empty

    @property
    def splat(self):
        """Returns True if the selected nodes should be splatted."""
        return "*" in self._mod

    @property
    def show(self):
        """Returns True if the selected nodes should be shown."""
        return not self._mod or "+" in self._mod

    @property
    def remove(self):
        """Returns True if the selected nodes should be removed."""
        return "-" in self._mod

    @property
    def collapse(self):
        """Returns True if the selected nodes should be collapsed."""
        return "%" in self._mod

    @property
    def render(self):
        """Check if the element should be rendered based on the modifier character."""
        if self.remove:
            return False
        return self.show or self.splat or self.collapse


class Link:
    """Link between two graph nodes."""

    def __init__(
        self,
        start: LinkableTreeNode,
        end: LinkableTreeNode,
        style: str | None = None,
        label: str | None = None,
    ):
        """Constructor

        Args:
            start (Element): Starting node (tail end)
            end (Element): Ending node (arrow end)
            style (str, optional): Link style to use (if None it will be determined based on default
                rules). Defaults to None.
            label (str, optional): Link label (to use during rendering). Defaults to "".
        """
        self.start = start
        """Starting node (tail end)."""
        self.end = end
        """Ending node (arrow end)."""
        self.style = style
        """Link style to use (if None it will be determined based on default rules)."""
        self.label = label
        """Link label."""
        self.render = True
        """Whether the link should be rendered."""
        self.id = str(id(self))
        """Link identifier."""

    def __str__(self):
        return f"Link from {self.start} to {self.end}"

    def __repr__(self):
        s = f"Link from {self.start} to {self.end}"
        if not self.render:
            s += "[!render]"
        return s

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Link):
            return False

        return (
            self.start == other.start
            and self.end == other.end
            and self.style == other.style
            and self.label == other.label
        )

    def __hash__(self):
        return hash((self.start.id, self.end.id))

    def clone(self):
        """Clones link."""
        return Link(self.start, self.end, self.style, self.label)


class ParserRule:
    """Structure to hold rule information after being parsed by the parser"""

    def __init__(self, line: str, attributes: dict[str, str]):
        """Constructor

        Args:
            line (str): Parser line (rule line)
            attributes (dict[str, str]): Parsed attributes
        """
        self.line = line
        """Raw line"""
        self.attributes = attributes
        """Rule attributes"""

    @property
    def section(self) -> str:
        """Section name (determined from raw line)

        Returns:
            str:
        """
        return self.line.split()[2]

    @property
    def chain(self) -> str:
        """Chain name as determine from parsed attributes

        Returns:
            str:
        """
        return self.attributes["chain"]

    def __repr__(self) -> str:
        attrs = [f"{k}={v}" for k, v in self.attributes.items() if k != "chain"]
        return f"Rule section={self.section} chain={self.chain} {' '.join(attrs)}"


class TreeNode:
    """Base tree node. All tree elements (Section, Chain, Rule) inherit from this class."""

    def __init__(self, parent: TreeNode | None = None):
        self.parent: TreeNode | None = parent
        """Parent node"""

    @property
    def id(self) -> str:
        """Node identifier"""
        return "unidentified tree node"

    @property
    def tree_root(self) -> RootNode:
        """Root tree node

        Returns:
            Root:
        """
        if isinstance(self, RootNode):
            return self

        if self.parent is None:
            raise ValueError("Parent is null")

        return self.parent.tree_root

    def get_parents(self) -> list[TreeNode]:
        """Get all parent nodes

        Returns:
            list[TreeNode]: List of parent nodes
        """
        parents = []
        node = self
        while node.parent:
            parents.append(node.parent)
            node = node.parent
        return parents

    @property
    def parents(self) -> list[TreeNode]:
        """
        Returns a list of parent nodes for the current node.

        Returns:
            A list of TreeNode objects representing the parent nodes.
        """
        return self.get_parents()


class LinkableTreeNode(TreeNode):
    """Base class for tree nodes that can have links (i.e. Section, Chain, Rule)."""

    def __init__(self, parent: TreeNode):
        super().__init__(parent)
        self.links: list[Link] = []
        """Node links"""

    def add_link(self, to: LinkableTreeNode, style: str | None = None, label: str | None = None):
        """Add link to tree.

        Args:
            link (Link): Link to add
        """
        if not self.tree_root.has_tree_element(to):
            raise ElementNotFound(f"Tree does not contain end node {to} of link")

        self.links.append(Link(self, to, style, label))

    def add_existing_link(self, link: Link):
        """Add (an existing) link to tree replacing the start with this rule.

        Args:
            link (Link): Link to add
        """
        if not self.tree_root.has_tree_element(link.end):
            raise ElementNotFound(f"Tree does not contain end node {link.end} of link")

        if link in link.start.links:
            link.start.remove_link(link)
        link.start = self
        self.links.append(link)

    def remove_link(self, link: Link):
        """Remove existing link

        Args:
            link (Link): Link to add
        """
        self.links.remove(link)

    def remove_links_to(self, to: LinkableTreeNode):
        """Remove all existing links to given end node

        Args:
            to (LinkableNode): End node
        """
        links = [l for l in self.links if l.end == to]
        for link in links:
            self.links.remove(link)

    def clone_links(self, new_start: LinkableTreeNode | None = None):
        """Clone links and replace start with given node.

        Args:
            new_start (LinkableTreeNode | None, optional): New start node or None to use
                current/self. Defaults to None.

        Returns:
            list[Link]: Cloned links
        """
        links = [l.clone() for l in self.links]
        for l in links:
            l.start = new_start or self
        return links


@total_ordering
class Rule(LinkableTreeNode):
    """Element representing a firewall rule. When a rule is parsed each of its attributes (i.e.
    action, jump-target) will be added as property (replacing - with _) and can be accessed plainly
    as `rule.action` or `rule.jump_target`.
    """

    def __init__(self, line: str, index: int, attributes: Mapping[str, Any | None]):
        """Constructor

        Args:
            line (str): full firewall rule e.g. `/ip firewall filter ....`
            index (int): rule index (usually line number)
            attributes (dist[str, Any|None]) : Parsed attributes
        """
        super().__init__(Chain(DETACHED))
        self.parent: Chain
        """Parent chain"""
        self.parent.add_rule(self)
        self.known_attributes: list[str] = list(attributes.keys())
        """List of know/parsed attributes"""
        self.loggable = True
        """Whether rule is loggable to the rule log"""
        self.index = index
        """Rule index (based on parsed file)"""
        self.raw_line = line.strip()
        """The rule line as written in the input file"""

        for key in attributes:
            self.__dict__[key] = attributes[key]

    @property
    def id(self) -> str:
        """Rule identifier

        Returns:
            str:
        """
        return f"{self.parent.id}:rule{self.index}"

    @property
    def chain(self) -> str:
        """Rule chain name. This is a short hand for writing self.parent.name.

        Returns:
            str: Rule chain name
        """
        return self.parent.name

    @property
    def is_terminal_rule(self) -> bool:
        """Whether the rule is flow ending (that is no more processing after it). Note this applies
        to the rule only and not the whole flow and it mostly relies on the rule action and whether
        rule has matchers.

        Returns:
            bool: True if packet flow stops/terminates at the rule.
        """
        return self.action in ["drop", "accept", "reject", "tarpit"] and not self.has_rule_matchers

    @property
    def has_rule_matchers(self) -> bool:
        """Whether rule has matchers properties (any property other than action, comment, chain,
        log_prefix, log, jump_target)

        Returns:
            bool:
        """
        return (
            set(self.known_attributes).difference(
                {"action", "comment", "chain", "log_prefix", "log", "jump_target"}
            )
            != set()
        )

    @property
    def line(self) -> str:
        """Raw line text"""
        return self.raw_line

    def __getattribute__(self, attr):
        try:
            return object.__getattribute__(self, attr)
        except AttributeError:
            return None

    def next_rule(self) -> Rule | None:
        """Return the next rule (by index) in the chain. This is a shortcut for
        parent.next_rule(self).

        Returns:
            Rule|None: Next rule in the chain or None.
        """
        return self.parent.next_rule(self)

    def detach(self) -> Rule:
        """Remove rule from parent chain. Note does not remove links to rule."""
        self.parent.remove_rule(self)
        return self

    def __gt__(self, other):
        if not isinstance(other, Rule):
            raise TypeError(f"Unsupported operation for type {type(other)}")
        return self.index > other.index

    def __str__(self):
        return f"Rule {self.index}"

    def __eq__(self, other):
        if not isinstance(other, Rule):
            return False

        if id(self) == id(other):
            return True

        if (
            self.index != other.index
            or self.loggable != other.loggable
            or self.known_attributes != other.known_attributes
        ):
            return False

        for attribute in self.known_attributes:
            if self.__getattribute__(attribute) != other.__getattribute__(attribute):
                return False

        return True

    def __repr__(self):
        return f"Rule {self.index} in {self.chain}"

    def jump_target_chain(self) -> Chain | None:
        """Return the chain node this rule jumps to. If rule isn't a jump rule, then None will be
        returned.

        Returns:
            Chain | None: Jump chain
        """
        return self.parent.parent.chain(self.jump_target) if self.jump_target else None

    def clone(self) -> Rule:
        """Copy/clone rule. Note that the copied rule's index and raw line will be the same as this
        rule.

        Returns:
            Rule: Rule clone
        """
        attr = {x: getattr(self, x) for x in self.known_attributes}
        r = Rule(self.raw_line, self.index, attr)
        r.loggable = r.loggable
        r.links = self.clone_links(r)
        return r


class Chain(LinkableTreeNode):
    """Element representing a firewall chain. Rule can be accessed by subscript with the rule id -
    e.g. chain["rule1"]."""

    def __init__(self, name: str):
        """Constructor

        Args:
            name (str): Chain name
        """
        self.name = name
        """Chain name."""
        super().__init__(Section(DETACHED))
        self.parent: Section
        """Section chain belong to."""
        self.parent.add_chain(self)
        self.rules: list[Rule] = []
        """Dictionary of rules found in this chain (key = rule id)"""
        self.render = True
        """Whether the chain should be rendered"""

    @property
    def id(self) -> str:
        """Chain identifier

        Returns:
            str:
        """
        return f"{self.parent.id}:{self.name}"

    def _sort_rules(self):
        if self.rules:
            self.rules.sort(key=lambda x: x.index)

    def first_rule(self) -> Rule | None:
        """Return the first rule in the chain.

        Returns:
            Rule: Rule or None
        """
        self._sort_rules()
        return self.rules[0] if self.rules else None

    def last_rule(self) -> Rule | None:
        """Return the last rule in the chain.

        Returns:
            Rule: Rule or None
        """
        self._sort_rules()
        return self.rules[-1] if self.rules else None

    def next_rule(self, rule: Rule) -> Rule | None:
        """Return the next rule in the chain for given rule (if part of the chain). Otherwise
        returns None.

        Args:
            rule (Rule): Chain rule

        Returns:
            Rule | None: Next rule in the chain or None
        """
        self._sort_rules()
        try:
            index = [x.index for x in self.rules].index(rule.index)
            index += 1
            return self.rules[index] if index < len(self.rules) else None
        except ValueError:
            return None

    def add_rule(self, rule: Rule):
        """Add rule to chains. Note it will not add rule links to the parent.

        Args:
            rule (Rule): Rule to be added.
        """
        # find existing rule by index
        existing_rule = next(iter(x for x in self.rules if x.index == rule.index), None)

        if existing_rule:
            logging.warning("Attempted to add rule with rule index already chain %s", rule)
            return None

        self.rules.append(rule)
        rule.parent = self
        return rule

    def remove_rule(self, rule: Rule) -> Rule | None:
        """Remove given rule from chain. Removed rule will be re-attached to a new empty
        root/section/chain.

        Args:
            rule (Rule): Rule to remove.

        Returns:
            Rule | None: Removed rule or None if rule isn't part of the chain's rules.
        """
        if rule in self.rules:
            self.rules.remove(rule)
            Chain(DETACHED).add_rule(rule)
            return rule

        logging.warning("Asked to remove non-existing rule %s", rule)
        return None

    def detach(self):
        """Remove self from parent section."""
        self.parent.remove_chain(self)

    def __str__(self):
        return f"Chain {self.name}"

    def __repr__(self):
        s = f"Chain {self.name} in {self.parent.name}"
        if not self.render:
            s += "[!render]"
        return s

    def __eq__(self, other):
        if not isinstance(other, Chain):
            return False

        if id(self) == id(other):
            return True

        if self.name != other.name:
            return False

        if self.render != other.render:
            return False

        if self.rules != other.rules:
            return False

        return True

    def clone(self) -> Chain:
        """Clone self.

        Returns:
            Chain: Clone of self.
        """
        c = Chain(self.name)
        c.render = self.render

        for r in self.rules:
            c.add_rule(r.clone())

        c.links = self.clone_links(c)
        return c


class Section(LinkableTreeNode):
    """Element representing a firewall section. Chains can be accessed by subscript with the chain
    name - e.g. section["input"].
    """

    def __init__(self, name: str):
        """Constructor

        Args:
            name: Section name
        """
        self.name: str = name
        """Section name - just the id"""
        super().__init__(RootNode())
        self.parent: RootNode
        """Section identifier"""
        self.parent.add_section(self)
        self.chains: list[Chain] = []
        """Child chain elements."""
        self.render = True
        """Whether the section should be rendered"""

    @property
    def id(self) -> str:
        """Section identifier.

        Returns:
            str:
        """
        return self.name

    def get_or_add_chain(self, name: str) -> Chain:
        """Get or add chain by name.

        Args:
            name (str): Chain name.

        Returns:
            Chain
        """

        chain = self.chain(name)
        if not chain:
            logging.debug("Adding chain %s to %s", name, self.parent)
            return self.add_chain(name)
        return chain

    def chain(self, name: str) -> Chain | None:
        """Get chain by name.

        Args:
            name (str): Chain name

        Returns:
            Chain|None: Found chain or None if chain isn't found.
        """
        chain = [c for c in self.chains if c.name == name]
        return chain[0] if chain else None

    def has_chain(self, name: str) -> bool:
        """Checks if given chain exists in tree.

        Args:
            name (str): Chain name.

        Returns:
            bool
        """
        if [c for c in self.chains if c.name == name]:
            return True
        return False

    def add_chain(self, chain: Chain | str) -> Chain:
        """Add chain to section. If chain is given as string, then a new chain will be created.

        Args:
            chain (Chain | str): Chain to add or chain name to create.

        Returns:
            Chain: Added chain. Raises ChainAlreadyExistsError if chain with given name already
                exists.
        """
        name = chain if isinstance(chain, str) else chain.name
        if self.has_chain(name):
            raise ChainAlreadyExistsError(name, self.parent)

        c = Chain(chain) if isinstance(chain, str) else chain
        c.parent = self

        self.chains.append(c)
        return c

    def remove_chain(self, chain: Chain) -> Chain | None:
        """Remove given chain from section. Removed chain will be attached to a new default parent.

        Args:
            chain (Chain): Chain

        Returns:
            Chain | None: Removed chain, or None if chain not found in section.
        """
        if chain not in self.chains:
            logging.warning("Asked to remove non-existing chain %s", chain)
            return None

        self.chains.remove(chain)
        Section(DETACHED).add_chain(chain)
        return chain

    def next_section(self) -> Section | None:
        """Return next section in tree.

        Returns:
            Section: Next section or None if none found
        """
        return self.parent.next_section(self)

    def detach(self):
        """Detach self from tree parent."""
        self.parent.remove_section(self)

    def __str__(self):
        return f"Section {self.name}"

    def __repr__(self):
        s = f"Section {self.name}"
        if not self.render:
            s += "[!render]"
        return s

    def clone(self) -> Section:
        """Clone self.

        Returns:
            Section: Clone section.
        """
        s = Section(self.name)
        s.render = self.render

        for c in self.chains:
            s.add_chain(c.clone())

        s.links = self.clone_links(s)
        return s

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Section):
            return False

        if id(self) == id(other):
            return True

        if self.name != other.name:
            return False
        if self.render != other.render:
            return False
        if self.chains != other.chains:
            return False
        return True


class RootNode(TreeNode):
    """Represents the top of the tree structure"""

    def __init__(self):
        super().__init__()
        self.sections: list[Section] = []
        """List of child section elements"""

    def has_section(self, name: str) -> bool:
        """Checks if given section exists in tree.

        Args:
            name (str): Section name.

        Returns:
            bool
        """
        return bool([x for x in self.sections if x.name == name])

    def section(self, name: str) -> Section | None:
        """Get section by name.

        Args:
            name (str): Section name.

        Returns:
            Section|None: Found section or None if no section is found.
        """
        section = [x for x in self.sections if x.name == name]
        if not section:
            return None

        return section[0]

    def get_or_add_section(self, name: str) -> Section:
        """Get or create section

        Args:
            name (str): Section name
        """
        section = self.section(name)
        if not section:
            logging.debug("Adding section %s to tree", name)
            return self.add_section(name)
        return section

    def has_tree_element(self, element: TreeNode) -> bool:
        """
        Checks if the given element exists in the tree.

        Args:
            element (TreeNode): The element to check.

        Returns:
            bool: True if the element exists in the tree, False otherwise.
        """
        if isinstance(element, Section):
            return element in self.sections
        if isinstance(element, Chain):
            return element in self.chains
        return element in self.rules

    @property
    def chains(self) -> list[Chain]:
        """List all chains in tree"""
        return [c for s in self.sections for c in s.chains]

    @property
    def rules(self) -> list[Rule]:
        """List all rules in subelements.

        Returns:
            list[Rule]:
        """ """"""
        return [r for c in self.chains for r in c.rules]

    def next_section(self, section: Section) -> Section | None:
        """Return the next section based on packet flow order in the tree.

        Args:
            section (Section): Current section.

        Returns:
            Section | None: Next section or None if section is not part of this tree or there is the
            last section.
        """
        from firewall2mermaid.common import section_sort_key

        if section not in self.sections:
            return None

        self.sections.sort(key=lambda x: section_sort_key(x.name.lower()))
        index = self.sections.index(section)
        index += 1
        return self.sections[index] if index < len(self.sections) else None

    def first_section(self) -> Section | None:
        """Return the first section based on packet flow order in this tree.

        Returns:
            Section | None: First section or None
        """
        from firewall2mermaid.common import section_sort_key

        if not self.sections:
            return None

        return min(self.sections, key=lambda x: section_sort_key(x.name.lower()))

    def add_section(self, section: Section | str) -> Section:
        """Add section to tree. If section is given as string, then a new section will be created.

        Args:
            section (Section | str): section to add or section name to create.

        Returns:
            Section: Added section. Raises SectionAlreadyExistsError if the section by same name
                already exists.
        """
        name = section if isinstance(section, str) else section.name
        if self.has_section(name):
            raise SectionAlreadyExistsError(name)

        s = Section(section) if isinstance(section, str) else section
        s.parent = self
        self.sections.append(s)
        return s

    def remove_section(self, section: Section) -> Section | None:
        """Remove section from tree.

        Args:
            section (Section): Section to remove.

        Returns:
            Section | None: Removed section or None if no section has been removed.
        """
        if section not in self.sections:
            logging.warning("Asked to remove non-existing section %s", section)
            return None

        self.sections.remove(section)
        RootNode().add_section(section)
        return section

    def next_rule_index(self) -> int:
        """Get the next available rule index.

        Returns:
            int: Next available rule index.
        """
        return max((x.index for x in self.rules), default=-1) + 1

    def clone(self) -> RootNode:
        """Clone tree.

        Returns:
            RootNode: Cloned root.
        """

        r = RootNode()
        for s in self.sections:
            r.add_section(s.clone())
        return r

    def __eq__(self, other):
        if not isinstance(other, RootNode):
            return False

        if id(self) == id(other):
            return True

        if self.sections != other.sections:
            return False
        return True


class PacketFlowIterator:
    """Iterator for PacketFlowElement."""

    def __init__(self, flow: PacketFlowElement):
        """Constructor.

        Args:
            flow (PacketFlowElement): Flow element to iterate over.
        """
        self.head: PacketFlowElement | None = flow
        self.reversed = reversed

    def __iter__(self):
        return self

    def __next__(self) -> PacketFlowElement:
        if not self.head:
            raise StopIteration

        element = self.head
        self.head = self.head.next
        return element


class PacketFlowElement:
    """Packet flow element/leaf"""

    def __init__(self, section: str, chain: str):
        """Constructor.

        Args:
            section (str): Section name.
            chain (str): Chain name
        """

        self.section = section
        """Section name"""
        self.chain = chain
        """Chain Name"""
        self.next: PacketFlowElement | None = None
        """Next flow element (downstream)"""

    def __iter__(self):
        return PacketFlowIterator(self)

    def __reversed__(self):
        nodes: list[PacketFlowElement] = [self]
        node = self
        while node.next:
            node = node.next
            nodes.append(node)

        nodes = [PacketFlowElement(x.section, x.chain) for x in nodes]
        nodes.reverse()

        for i in range(0, len(nodes) - 1):
            nodes[i].next = nodes[i + 1]

        return PacketFlowIterator(nodes[0])

    def last_node(self) -> PacketFlowElement:
        """Get the last node in the flow.

        Returns:
            PacketFlowElement: _description_
        """
        return self.next.last_node() if self.next else self
