"""Containt rendering logic to render tree model into mermaid graph."""
from __future__ import annotations
import logging

from firewall2mermaid.common import (
    CLASS_DEF,
    LINK_STYLE_INVISIBLE,
    get_effective_selector,
)
from firewall2mermaid.model import (
    Chain,
    GraphSelector,
    Link,
    LinkableTreeNode,
    RootNode,
    Section,
    TreeNode,
)
from firewall2mermaid.render.common import determine_rule_label, determine_rule_style
from firewall2mermaid.render.model import (
    CommentNode,
    GraphRootNode,
    LinkNode,
    LiteralNode,
    Node,
    NodeRenderState,
    RuleNode,
    StyleNode,
    SubgraphNode,
)


class Renderer:
    """Graph renderer"""

    def __init__(
        self,
        model: RootNode,
        graph_selector: list[GraphSelector],
        node_render_mode="auto",
        show_legend=True,
        graph_direction="TB",
        log_rules=False,
        collapse_as_rule=False,
        show_root=True,
        extra_logs: list[str] | None = None,
    ):
        """
        Initialize the Render object.

        Args:
            model (RootNode): The tree model.
            graph_selector (list[GraphSelector]): The list of graph selectors.
            node_render_mode (str, optional): The node comments rendering mode. Defaults to "auto".
            show_legend (bool, optional): Whether to generate/show legend nodes. Defaults to True.
            graph_direction (str, optional): The graph direction. Defaults to "TB".
            log_rules (bool, optional): Whether to add rule log at the top of graph. Defaults to
                False.
            collapse_as_rule (bool, optional): Whether to render collapsed chains as rules instead.
                Defaults to False.
            show_root (bool, optional): Whether to show the root node. Defaults to True.
            extra_logs (list[str], optional): The extra logs to add to the graph. Defaults to [].
        """
        self.model = model
        """Tree model"""
        self.node_render_mode = node_render_mode
        """Node comments rendering mode"""
        self.show_legend = show_legend
        """Generate/show legend nodes"""
        self.graph_direction = graph_direction
        """Graph direction"""
        self.log_rules = log_rules
        """Add rule log at the top of graph"""
        self.collapse_as_rule = collapse_as_rule
        """Render collapsed chains as rules instead"""
        self._last_rule_index = 0
        self._link_remap: dict[Link, tuple[str | None, str | None]] = {}
        """List of link remaps (start/end id)"""
        self.extra_logs = list(extra_logs or [])
        """Extra logs to add to the graph"""

        self.model_links = [l for section in self.model.sections for l in section.links]
        self.model_links.extend(l for chain in self.model.chains for l in chain.links)
        self.model_links.extend(l for rule in self.model.rules for l in rule.links)

        self.show_root = show_root
        self.graph_selector = graph_selector
        self.has_collapse_selectors = any(x.collapse for x in graph_selector)

    def _next_rule_index(self):
        """Helper method to keep track of ephemeral rules"""
        if self._last_rule_index == 0:
            self._last_rule_index = self.model.next_rule_index()
        self._last_rule_index += 1
        return self._last_rule_index

    def render_graph(self) -> str:
        """Generate Mermaid graph text for the passed model and configuration.

        Returns:
            str: The Mermaid graph text.
        """
        graph: GraphRootNode = self._generate_render_tree()
        if self.show_legend:
            self._add_legend_node(graph)
        logging.info("Rendering graph")
        return graph.render()

    def _generate_render_tree(self):
        graph = GraphRootNode()

        top_direction = "TB" if self.show_legend else self.graph_direction
        if self.extra_logs:
            for log in self.extra_logs:
                graph.add_child(CommentNode(log))

        graph.add_child(
            LiteralNode(r"%%{init: {'flowchart': {'htmlLabels': false}, 'theme': 'dark'}}%%")
        )
        graph.add_child(LiteralNode(rf"flowchart {top_direction}"))
        graph.add_child(LiteralNode())

        if self.log_rules:
            self._add_rule_log_nodes(graph)
            graph.add_child(LiteralNode())

        self._add_style_nodes(graph)
        graph.add_child(LiteralNode())
        self._add_model_nodes(graph)
        if self.collapse_as_rule:
            self._add_collapsed_as_rule_nodes(graph)
        self._add_link_nodes(graph)
        return graph

    def _add_link_nodes(self, graph: GraphRootNode):
        logging.info("Adding link nodes")
        graph.add_child(LiteralNode())
        graph.add_child(CommentNode("Relationships"))

        if self.model_links:
            for link in self.model_links:
                graph_link = self._create_link_node(link)
                # Drop self links
                if graph_link.start_id == graph_link.end_id:
                    logging.debug("Self link from/to '%s', skipping link", graph_link.start_id)
                    continue
                if not graph.find_node_by_id(graph_link.start_id, True):
                    # if start is collapsed
                    logging.debug(
                        "Start node (%s) is not rendered, skipping link(%s)",
                        graph_link.start_id,
                        link,
                    )
                    continue
                if not graph.find_node_by_id(graph_link.end_id, True):
                    logging.debug(
                        "End node (%s) is not rendered, skipping link(%s)", graph_link.end_id, link
                    )
                    continue
                logging.debug("Adding link node: %s", graph_link)
                graph.add_child(graph_link)

    def _create_link_node(self, link: Link):
        if link in self._link_remap:
            remapped = self._link_remap[link]
            logging.debug("Link (%s) remapped from to %s", link, remapped)
            start_id = remapped[0]
            end_id = remapped[1]
        else:
            start_id = link.start.id
            end_id = link.end.id
        return LinkNode(link, start_id=start_id, end_id=end_id)

    def _add_collapsed_as_rule_nodes(self, graph: GraphRootNode):
        logging.info("Adding collapsed sections/chains as rule nodes")

        collapsed_map = self._generate_collapsed_node_map()

        for link in self.model_links:
            if link.style == LINK_STYLE_INVISIBLE:
                continue

            if link.start.id in collapsed_map and link.end.id in collapsed_map:
                logging.debug(
                    "Both link start (%s) and end node (%s) are collapsed, skipping adding node",
                    link.start.id,
                    link.end.id,
                )
                continue

            if link.start.id in collapsed_map:
                element = collapsed_map[link.start.id]
                target = graph.find_node_by_id(link.end.id, recurse=True)
                assert target
                parent = target.parent
                assert parent
                rule = self._create_collapsed_node(element.id, element.id)
                index = parent.child_index(target.id) or 0
                parent.insert_child(index, rule)
                logging.debug("Added node (%s) as collapsed rule (%s)", element.id, rule.id)
                self._update_link_remap(link, rule.id, link.end.id)
            elif link.end.id in collapsed_map:
                element = collapsed_map[link.end.id]
                target = graph.find_node_by_id(link.start.id, recurse=True)
                assert target
                parent = target.parent
                assert parent
                rule = self._create_collapsed_node(element.id, element.id)
                index = parent.child_index(target.id) or -1
                parent.insert_child(index + 1, rule)
                logging.debug("Added node (%s) as collapsed rule (%s)", element.id, rule.id)
                self._update_link_remap(link, link.start.id, rule.id)

    def _generate_collapsed_node_map(self):
        collapsed_map: dict[str, LinkableTreeNode] = {}
        for section in self.model.sections:
            render = self._determine_rendering_status(section)
            if not render.collapse:
                continue

            collapsed_map[section.id] = section
            for chain in section.chains:
                render = self._determine_rendering_status(chain)
                if render.remove or render.splat:
                    continue

                collapsed_map[chain.id] = section
                for rule in chain.rules:
                    collapsed_map[rule.id] = section

        for chain in self.model.chains:
            if chain.id in collapsed_map:
                continue
            render = self._determine_rendering_status(chain)
            if render.remove or render.splat:
                continue
            if render.collapse:
                collapsed_map[chain.id] = chain
                for rule in chain.rules:
                    collapsed_map[rule.id] = chain
        return collapsed_map

    def _add_model_nodes(self, graph: GraphRootNode):
        logging.info("Rendering subgraphs")
        root: Node = graph
        if self.show_root:
            root = SubgraphNode(self.graph_direction, node_id="root")
            graph.add_child(root)

        for section in self.model.sections:
            self._add_section_node(section, root)

    def _add_section_node(self, section: Section, root: Node):
        section_node = None

        section_node = SubgraphNode(
            direction=self.graph_direction, node_id=section.id, name=section.name
        )

        for chain in section.chains:
            self._add_chain_node(chain, section_node, root)
        render = self._determine_rendering_status(section)
        if render.collapse:
            if self.collapse_as_rule:
                logging.debug(
                    "Section '%s' is collapsed as rule, deferring adding node", section.name
                )
                return
            logging.debug("Section '%s' is collapsed, adding as collapsed node", section.name)
            collapsed_node = self._create_collapsed_node(section.id, section.name)
            root.add_child(collapsed_node)
            self._update_remap_for_collapsed_node(section.id, collapsed_node.id)
            # update chain and rule links to point to collapsed node
            for chain in section.chains:
                self._update_remap_for_collapsed_node(chain.id, collapsed_node.id)
                for rule in chain.rules:
                    self._update_remap_for_collapsed_node(rule.id, collapsed_node.id)
        elif not render.render:
            logging.debug("Section '%s' is removed/hidden, skipping adding node", section.name)
        elif section_node.children:
            root.add_child(section_node)
            logging.debug("Added section '%s'", section)
        else:
            logging.debug("Section '%s' has no children, skipping adding to graph", section.name)

    def _add_chain_node(self, chain: Chain, parent: Node, root: Node):
        render = self._determine_rendering_status(chain)
        if not render.render:
            logging.debug("Chain '%s' is removed or hidden, skipping adding node", chain.name)
            return

        parent = root if render.splat else parent
        name = chain.id if render.splat else chain.name
        if render.collapse:
            self._add_chain_node_collapsed(chain, parent, name)
        else:
            self._add_chain_node_normal_render(chain, parent, name)

    def _add_chain_node_normal_render(self, chain, parent: Node, name):
        logging.debug("Rendering chain '%s' normally", chain.name)
        chain_node = SubgraphNode(direction=self.graph_direction, node_id=chain.id, name=name)
        parent.add_child(chain_node)
        logging.debug("Added chain '%s'", chain.name)

        for rule in chain.rules:
            rule_node = RuleNode(
                rule.id,
                determine_rule_label(rule, self.node_render_mode),
                determine_rule_style(rule),
            )
            chain_node.add_child(rule_node)
            logging.debug("Added rule node '%s' to chain", rule)

    def _add_chain_node_collapsed(self, chain: Chain, parent: Node, name: str):
        if self.collapse_as_rule:
            logging.debug("Chain '%s' is collapsed as rule, deferring adding node", chain.name)
            return
        logging.debug("Chain '%s' is collapsed, adding as collapsed node", chain.name)
        chain_node = self._create_collapsed_node(chain.id, name)
        self._update_remap_for_collapsed_node(chain.id, chain_node.id)
        for rule in chain.rules:
            self._update_remap_for_collapsed_node(rule.id, chain_node.id)
        parent.add_child(chain_node)
        logging.debug("Added chain '%s' as collapsed node", chain.name)

    def _create_collapsed_node(self, node_id: str, name: str):
        rule_id = f"{node_id}:collapsedrule{self._next_rule_index()}"
        return RuleNode(rule_id, f"{name} *collapsed", "collapsed")

    def _update_remap_for_collapsed_node(self, old_id: str, new_id: str):
        for link in self.model_links:
            if link.start.id == old_id:
                self._update_link_remap(link, new_id, None)
            if link.end.id == old_id:
                self._update_link_remap(link, None, new_id)

    def _update_link_remap(self, link: Link, start_id: str | None, end_id: str | None):
        if link in self._link_remap:
            self._link_remap[link] = (
                start_id or self._link_remap[link][0],
                end_id or self._link_remap[link][1],
            )
        else:
            self._link_remap[link] = (start_id, end_id)
        logging.debug("Link remap updated: %s -> %s", link, self._link_remap[link])

    def _get_styles_used(self) -> list[str]:
        """Return list of nodes styles used by renderable rules.

        Returns:
            list[str]: List of node styles.
        """
        styles: list[str] = []
        for section in self.model.sections:
            section_renderer = self._determine_rendering_status(section)
            for chain in section.chains:
                chain_renderer = self._determine_rendering_status(chain)
                render_rule = chain_renderer.splat or (
                    chain_renderer.render and section_renderer.render
                )
                if render_rule:
                    for rule in chain.rules:
                        style = determine_rule_style(rule)
                        if style and style not in styles:
                            styles.append(style)

        if self.has_collapse_selectors:
            styles.append("collapsed")

        return styles

    def _add_style_nodes(self, graph: GraphRootNode):
        logging.info("Adding styles")
        for style in self._get_styles_used():
            logging.debug("Adding style node: %s", style)
            graph.add_child(StyleNode(style, CLASS_DEF[style]))

    def _add_rule_log_nodes(self, graph: GraphRootNode):
        logging.info("Adding rule logs")
        for rule in self.model.rules:
            logging.debug("Adding rule: %s", rule)
            graph.add_child(CommentNode(rule.raw_line))

    def _add_legend_node(self, graph: GraphRootNode):
        logging.info("Adding legend node")
        legend_node = SubgraphNode("TB", node_id="legend", name="Legend")
        for style in self._get_styles_used():
            node = RuleNode(f"legend:rule{self._next_rule_index()}", f"*{style}", style)
            legend_node.add_child(node)

        index = 0
        root = graph
        if self.show_root:
            root = graph.find_node_by_id("root")
            if not root:
                raise ValueError("Can't find root node to add legend to")
        else:
            attachment_node: Node | None = next(graph.walk_nodes(SubgraphNode), None)
            if not attachment_node:
                attachment_node = next(graph.walk_nodes(RuleNode), None)
            index = graph.child_index(attachment_node.id) if attachment_node else 0
        root.insert_child(index or 0, legend_node)

        # legend was manually added during rendering so we will need to add links to it
        self._add_legend_link_nodes(graph, root)

    def _add_legend_link_nodes(self, graph: Node, root: Node):
        dummy = Section("dummy")
        for child in [x for x in root.children if isinstance(x, (RuleNode, SubgraphNode))]:
            if child.id == "legend":
                continue
            # start node doesn't matter as we will be overwritting its id
            link = Link(dummy, dummy, style=LINK_STYLE_INVISIBLE, label="")
            graph.add_child(LinkNode(link, start_id="legend", end_id=child.id))

    def _determine_rendering_status(self, element: TreeNode):
        if isinstance(element, Chain):
            return self._determine_chain_rendering_status(element)

        section_selector = get_effective_selector(self.graph_selector, element)
        if section_selector.remove:
            return NodeRenderState(remove=True)
        return NodeRenderState(collapse=section_selector.collapse)

    def _determine_chain_rendering_status(self, chain: Chain):
        chain_selector = get_effective_selector(self.graph_selector, chain)
        if chain_selector.remove:
            return NodeRenderState(remove=True)

        section_selector = get_effective_selector(self.graph_selector, chain.parent)
        if chain_selector.splat:
            return NodeRenderState(collapse=chain_selector.collapse, splat=True)
        if chain_selector.show and not chain_selector.default:
            return NodeRenderState(splat=not section_selector.show)
        if chain_selector.default:
            return NodeRenderState(
                splat=section_selector.splat,
                remove=section_selector.remove,
            )

        # section is collapsed but normal rendering for chain
        return NodeRenderState(
            collapse=True,
            splat=(section_selector.splat or section_selector.remove),
        )
