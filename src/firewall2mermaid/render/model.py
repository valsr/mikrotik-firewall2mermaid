"""Render model classes used by Renderer to construct and render the graph from the AST."""
from __future__ import annotations
import logging
from typing import Callable, Generator, Type, TypeVar
import uuid

from firewall2mermaid.common import LINK_STYLE_INVISIBLE, TAB_CHARACTER
from firewall2mermaid.model import Link
from firewall2mermaid.render.common import get_link_style, determine_link_label


NodeT = TypeVar("NodeT")


class Node:
    """Node in the graph."""

    def __init__(self, node_id: str | int | None = None, direction="TB", parent=None):
        if not node_id:
            self.id = uuid.uuid4().hex
        elif isinstance(node_id, int):
            self.id = str(node_id)
        else:
            self.id = node_id
        self.direction = direction
        self.parent: Node | None = parent
        self.children: list[Node] = []

    def format_graph_line(self, line: str, level=0, line_ending: bool | str = False):
        """Line render helper (for tabulation and formatting).

        Args:
            line (str): Graph line to render
            level (int, optional): Indentation level. Defaults to 0.
            line_ending (int|str, optional): Whether to add line ending to the line. If True or any
                string value then it will append the given line ending (\n for True)

        Returns:
            str: Formatted line
        """

        ending = ""
        if line_ending:
            if isinstance(line_ending, bool):
                ending = "\n"
            else:
                ending = line_ending
        return f"{TAB_CHARACTER*level}{line}{ending}"

    def normalize_render_text(self, text: str, level: int):
        """
        Normalize the render text by formatting it and adding the appropriate level of indentation.

        Args:
            text (str): The render text to be normalized.
            level (int): The indentation level.

        Returns:
            str: The normalized render text.
        """
        if not text:
            return text
        return "\n".join(
            [
                self.format_graph_line(x.strip(), level, True)
                for x in self.render_start(level).split("\n")
            ]
        )

    def join_render_lines(self, first: str, second: str):
        """
        Joins two lines together, ensuring that there is a newline character between them if
            necessary.

        Args:
            first (str): The first line.
            second (str): The second line.

        Returns:
            str: The joined lines.
        """
        if not first:
            return second

        if not second:
            return first

        return f"{first}{second}" if first.endswith("\n") else f"{first}\n{second}"

    def render(self, level: int = 0) -> str:
        """Renders the node as graph text.

        Returns:
            str: Graph text
        """
        logging.debug("Rendering node %s: %s", self.id, self)
        text = self.render_start(level)

        for child in self.children:
            child_text = child.render(level + 1)
            text = self.join_render_lines(text, child_text)

        text = self.join_render_lines(text, self.render_end(level))
        return text

    def render_start(self, _):
        """
        Called during the start of the node rendering method, before calling render on node's
        children. Subclasses should override this to supply the actual rendered text of the node.

        Parameters:
        - level (int): Indentation

        Returns:
            - Node rendered text
        """
        return ""

    def render_end(self, _):
        """
        Called after rendering the child nodes, for any additional node text that needs to be
        render. Subclasses should override this to supply the actual rendered text of the node.

        Parameters:
        - level (int): Indentation

        Returns:
            - Node render text
        """
        return ""

    def add_child(self, node):
        """Adds child node.

        Args:
            node: Node to add as a child

        Return:
            self
        """
        self.children.append(node)
        node.parent = self
        return self

    def insert_child(self, index: int, node):
        """Adds child node to given index (same use as list.insert)

        Args:
            index: Index to insert the node at
            node: Node to add as a child

        Return:
            self
        """
        self.children.insert(index, node)
        node.parent = self
        return self

    def child_index(self, node_id: str | int):
        """Returns the index of the child with the given id."""
        for index, child in enumerate(self.children):
            if child.id == node_id:
                return index
        return None

    def find_node_by_id(self, node_id: str | int, recurse=False):
        """Find node by given id by searching the current node and its direct children.

        Args:
            id (str | int): Node identifier
            recurse (bool, optional): If recurse then it will also search through all children.
                Defaults to False.

        Returns:
            Node|None: None if not found otherwise the found node.
        """
        node_id = str(node_id)
        return self.find_node(lambda node: node.id == node_id, recurse=recurse)

    def find_node(self, callback: Callable[[Node], bool], recurse=False):
        """Find node by given id by searching the current node and its direct children.

        Args:
            id (str | int): Node identifier
            recurse (bool, optional): If recurse then it will also search through all children.
                Defaults to False.

        Returns:
            Node|None: None if not found otherwise the found node.
        """
        if callback(self):
            return self

        for child in self.children:
            if callback(child):
                return child
            if recurse:
                node = child.find_node(callback, recurse)
                if node:
                    return node
        return None

    def next_sibling(self, node_id: str | int):
        """Find next sibling in the direct children of this node by given id.

        Args:
            id (str|int): Node identifier
            recurse (bool, optional): If recurse then it will also search through all children.
                Defaults to False.

        Returns:
            Node|None: None if not found otherwise the found node.
        """
        node_id = str(node_id)
        found = False
        for node in self.children:
            if found:
                return node
            if node.id == node_id:
                found = True
        return None

    def previous_sibling(self, node_id: str | int):
        """Find previous sibling in the direct children of this node by given id.

        Args:
            id (str|int): Node identifier
            recurse (bool, optional): If recurse then it will also search through all children.
                Defaults to False.

        Returns:
            Node|None: None if not found otherwise the found node.
        """
        node_id = str(node_id)
        previous_node = None
        for node in self.children:
            if node.id == node_id:
                return previous_node
            previous_node = node
        return None

    def walk_tree(self):
        """Walk the tree and yield nodes."""
        yield self
        for child in self.children:
            yield from child.walk_tree()

    def walk_nodes(self, node_type: Type[NodeT]) -> Generator[NodeT, None, None]:
        """Walk the tree and yield nodes of specific type."""
        if isinstance(self, node_type):
            yield self
        for child in self.children:
            yield from child.walk_nodes(node_type)

    def remove_child(self, node_id: str | int):
        """Removes a child from the node.Args:
            id (str|int): Node identifier

        Returns:
            Node|None: None if not found otherwise the remoevd node."""
        node_id = str(node_id)
        for child in self.children:
            if child.id == node_id:
                self.children.remove(child)
                return child

        return None


class GraphRootNode(Node):
    """Root node of the graph."""

    def __init__(self, graph_direction: str = "TB"):
        super().__init__("graphroot")
        self.direction = graph_direction

    def render(self, level: int = 0) -> str:
        """Renders the node as graph text."""
        return super().render(-1)

    def __repr__(self) -> str:
        return "GraphRoot"


class LiteralNode(Node):
    """A literal text node. Node doesn't have children."""

    def __init__(self, text=""):
        super().__init__()
        self.text = text

    def render(self, level=0) -> str:
        return self.format_graph_line(self.text, level, True)

    def __repr__(self) -> str:
        return f"Literal: {self.text}"


class CommentNode(Node):
    """A comment node"""

    def __init__(self, comment: str):
        super().__init__("")
        self.text = comment

    def render(self, level=0) -> str:
        return self.format_graph_line(f"%% {self.text}", 0, True)

    def __repr__(self) -> str:
        return f"Comment: {self.text}"


class RuleNode(Node):
    """A mock rule node withought needint a rule"""

    def __init__(self, rule_id: str, label: str, style: str | None = None):
        super().__init__(rule_id)
        self.label = label
        self.style = style

    def render_start(self, level: int):
        text = self.id
        if self.label:
            text += f"[{self.label}]"
        if self.style:
            text += f":::{self.style}"
        return self.format_graph_line(text, level)

    def __repr__(self) -> str:
        return f"Rule: {self.id}"


class SubgraphNode(Node):
    """Subgraph node - chain or section."""

    def __init__(
        self,
        direction: str,
        node_id: str,
        name: str | None = None,
        render_direction: bool = True,
    ):
        super().__init__(node_id)
        self.name = name
        self.direction = direction
        self.render_direction = render_direction

    def render_start(self, level):
        tagline = f"subgraph {self.id}"
        if self.name:
            tagline += f" [{self.name}]"
        text = self.format_graph_line(tagline, level, True)
        if self.render_direction:
            text += self.format_graph_line(f"direction {self.direction}", level + 1, True)

        return text

    def render_end(self, level):
        return self.format_graph_line("end", level, True)

    def __repr__(self) -> str:
        return f"Subgraph: {self.id}"


class LinkNode(Node):
    """
    Represents a link in the graph.

    Attributes:
        id (str): The unique identifier of the node.
        link (Link): Original link.
        label (str): The label of the link.
        start_id (str): Effective start node id.
        end_id (str): Effective end node id.
    """

    def __init__(
        self,
        link: Link,
        label: str | None = None,
        start_id: str | None = None,
        end_id: str | None = None,
    ):
        super().__init__(link.id)
        self.link = link
        self.label = label
        self._start_id = start_id
        self._end_id = end_id

    @property
    def start_id(self):
        """
        Returns the effective start node id of the link (either the Link.start_id or if setter is
        called then returns that value).

        Returns:
            str: Effective start node id.
        """
        return self._start_id if self._start_id else self.link.start.id

    @start_id.setter
    def start_id(self, value: str | None):
        self._start_id = value

    @property
    def end_id(self):
        """
        Returns the effective end node id of the link (either the Link.end_id or if setter is called
            then returns that value).

        Returns:
            str: Effective end node id.
        """
        return self._end_id if self._end_id else self.link.end.id

    @end_id.setter
    def end_id(self, value: str | None):
        self._end_id = value

    def render_start(self, level):
        if not self.link.render:
            logging.info("Won't render link '%s' -> render property set to False", self.link)
            return ""

        label = self.label or ""
        if not self.label and self.link.style != LINK_STYLE_INVISIBLE:
            label = determine_link_label(self.link)
        if label:
            label = f"|{label}|"

        return self.format_graph_line(
            f"{self.start_id} {get_link_style(self.link)}{label} {self.end_id}", level
        )

    def __repr__(self) -> str:
        return f"Link: from {self.start_id} to {self.end_id}"


class StyleNode(Node):
    """
    Represents a style definition node.

    Attributes:
        id (str): The unique identifier of the node.
        style (str): The style of the node.
    """

    def __init__(self, node_id: str, style: str):
        super().__init__(node_id)
        self.style = style

    def render(self, level=0):
        """
        Renders the node with the specified style at the given indentation level.

        Args:
            level (int): The indentation level for rendering the node.

        Returns:
            str: The rendered graph line for the node.

        """
        return self.format_graph_line(f"classDef {self.id} {self.style}", 0, True)

    def __repr__(self) -> str:
        return f"Style: {self.id}"


class NodeRenderState:
    """Represents the render state of a node."""

    def __init__(self, remove: bool = False, collapse: bool = False, splat: bool = False):
        self.set_state(remove, collapse, splat)

    def set_state(self, remove: bool = False, collapse: bool = False, splat: bool = False):
        """
        Sets the state of the node.

        Args:
            remove (bool): Whether to remove the node.
            collapse (bool): Whether to collapse the node.
            splat (bool): Whether to splat the node.

        Returns:
            None
        """
        self._remove = remove
        if remove:
            self._collapse = False
            self._splat = False
        else:
            self._collapse = collapse
            self._splat = splat

    @property
    def remove(self):
        """
        Returns whether the node should be removed or not.

        Returns:
            bool: True if the node should be removed, False otherwise.
        """
        return self._remove

    @property
    def render(self):
        """
        Returns the node's render state.

        Returns:
            bool: True if the node should be rendered, False otherwise.
        """
        return not self._remove

    @property
    def splat(self):
        """
        Returns the node's splat state.

        Returns:
            bool: True if the node should be splatted, False otherwise.
        """
        return self._splat

    @property
    def collapse(self):
        """
        Returns the node's collapse state.

        Returns:
            bool: True if the node should be collapsed, False otherwise.
        """
        return self._collapse
