"""Common rendering functions."""
import logging
from firewall2mermaid.common import (
    CLASS_DEF,
    LINK_STYLE_BETWEEN_SECTIONS,
    LINK_STYLE_NORMAL,
    LINK_STYLE_NORMAL_LONGER,
    find_jump_target_rule,
)
from firewall2mermaid.model import Chain, Link, Rule, Section


def hash_node(node) -> str:
    """Hash a node to a string.

    Args:
        node (Node): Node

    Returns:
        str: Hashed node
    """
    if isinstance(node, (Section, Chain, Rule)):
        return str(id(node)) if hasattr(node, "name") and node.name == "detached" else node.id
    return "unknown node type"


def is_link_between_sections(link: Link) -> bool:
    """Check if the link is between sections (chains, sections).

    Args:
        link (Link): Link

    Returns:
        bool: True if the link is between sections, False otherwise.
    """
    end_hierarchy = [
        hash_node(node)
        for node in [link.end, *link.end.get_parents()]
        if isinstance(node, (Section, Chain, Rule))
    ]

    return {hash_node(x) for x in [link.start, *link.start.get_parents()]}.intersection(
        end_hierarchy
    ) == set()


def get_link_style(link: Link) -> str:
    """Generate link mermaid code.

    Args:
        link (Link): Link

    Returns:
        str: Mermaid text for given link
    """
    if link.style:
        return link.style

    if is_link_between_sections(link):
        return LINK_STYLE_BETWEEN_SECTIONS

    link_style = LINK_STYLE_NORMAL
    if isinstance(link.start, Rule):
        is_jump_with_matchers = link.start.action == "jump" and link.start.has_rule_matchers
        is_multiple_links = len(link.start.links) > 1

        if is_jump_with_matchers:
            # elongate the otherwise in jump
            is_same_jump_target = find_jump_target_rule(link.start) == link.end
            link_style = LINK_STYLE_NORMAL_LONGER if not is_same_jump_target else LINK_STYLE_NORMAL
        elif is_multiple_links:
            is_same_parent = link.start.parent == link.end.parent
            link_style = LINK_STYLE_NORMAL_LONGER if not is_same_parent else LINK_STYLE_NORMAL

    return link_style


def determine_rule_style(rule: Rule) -> str | None:
    """Determine the graph style to use for given rule.

    Args:
        rule (Rule): Rule

    Returns:
        str | None: Node style or None if no style can be determined
    """
    if rule.disabled == "yes":
        logging.debug("Rule '%s' is disabled", rule)
        return "disabled"
    return rule.action if rule.action in CLASS_DEF else None


def determine_rule_action_sublabel(rule: Rule) -> str:
    """Create label part from rule action parameters.

    Args:
        rule (Rule): Rule

    Returns:
        str: label part
    """
    if rule.action in ["log", "drop", "accept", "return"]:
        return f"{rule.action}"
    if rule.action == "jump":
        return f"jump to {rule.jump_target}"
    if rule.action in ["add-src-to-address-list", "add-dst-to-address-list"]:
        label = f"add ip to '{rule.address_list}'"
        if rule.address_list_timeout:
            label += f" for {rule.address_list_timeout}"
        return label
    return ""


def determine_rule_connection_sublabel(rule: Rule) -> str:
    """Create label part from rule connection parameters.

    Args:
        rule (Rule): Rule

    Returns:
        str: Label part
    """
    return f"connection is {rule.connection_state}" if rule.connection_state else ""


def determine_rule_packet_sublabel(rule: Rule) -> str:
    """Create label part from rule packet parameters

    Args:
        rule (Rule): Rule

    Returns:
        str: Label part
    """
    label = ""
    attrs = []

    if rule.protocol:
        attrs.append(rule.protocol)
    if rule.tcp_flags:
        attrs.append(rule.tcp_flags)
    if rule.dst_limit:
        attrs.append(f"below {rule.dst_limit}")

    if attrs:
        label = f"is {','.join(attrs)}"

    return label


def determine_rule_src_sublabel(rule: Rule) -> str:
    """Create label part from rule source parameters.

    Args:
        rule (Rule): Rule

    Returns:
        str: label part
    """
    label = ""

    if (
        rule.src_address_list
        or rule.src_address
        or rule.src_address_type
        or rule.port
        or rule.src_port
        or rule.in_interface
        or rule.in_bridge_port
    ):
        if rule.src_address_list:
            label = rule.src_address_list
        elif rule.src_address:
            label = rule.src_address
        elif rule.src_address_type:
            label = rule.src_address_type
        else:
            label = "any"

        if rule.src_port or rule.port:
            label += f":{rule.src_port}" if rule.src_port else f":*{rule.port}"

    # on
    if rule.in_interface or rule.in_bridge_port:
        label += " on "
        if rule.in_interface:
            label += rule.in_interface
        if rule.in_bridge_port:
            label += f":{rule.in_bridge_port}"

    return "src is " + label if label else ""


def determine_rule_dst_sublabel(rule: Rule) -> str:
    """Create label part from rule destination parameters.

    Args:
        rule (Rule): Rule

    Returns:
        str: label part
    """
    label = ""

    # to
    if (
        rule.dst_address_list
        or rule.dst_address
        or rule.dst_address_type
        or rule.port
        or rule.dst_port
        or rule.out_interface
        or rule.out_bridge_port
    ):
        if rule.dst_address_list:
            label = rule.dst_address_list
        elif rule.dst_address:
            label = rule.dst_address
        elif rule.dst_address_type:
            label = rule.dst_address_type
        else:
            label = "any"

        if rule.dst_port or rule.port:
            label += f":{rule.dst_port}" if rule.dst_port else f":*{rule.port}"

    # on
    if rule.out_interface or rule.out_bridge_port:
        label += " on "
        if rule.out_interface:
            label += rule.out_interface
        if rule.out_bridge_port:
            label += f":{rule.out_bridge_port}"

    return f"dst is {label}" if label else ""


def determine_rule_label(rule: Rule, comment_handling="auto") -> str:
    """Create rule node label.

    Args:
        rule (Rule): Rule
        comment_handling (str, optional): Whether to use shortened labels. Defaults to auto.
            Can be one of:
                - show: Always shows comments in node labels
                - hide: Never shows comments in node labels
                - auto: Shows comments only when no other label can be generated
                - prefer: Shows comments in node label instead of generated label, if aviable

    Returns:
        str: Label
    """
    logging.debug("Creating label for rule '%s' with mode '%s'", rule, comment_handling)
    if comment_handling == "prefer" and rule.comment:
        # quote the label
        if any(char in rule.comment for char in ["(", ")", "{", "}", "[", "]"]):
            return f'"{rule.comment}"'
        logging.debug("Preferring comment as label '%s'", rule.comment)
        return rule.comment

    label: str = ""
    # label parts
    action_label = determine_rule_action_sublabel(rule)
    packet_label = determine_rule_packet_sublabel(rule)
    src_label = determine_rule_src_sublabel(rule)
    dst_label = determine_rule_dst_sublabel(rule)
    connection_label = determine_rule_connection_sublabel(rule)

    if packet_label or src_label or dst_label or connection_label:
        label_parts = []
        if packet_label:
            label_parts.append(packet_label)
        if connection_label:
            label_parts.append(connection_label)
        if src_label:
            label_parts.append(src_label)
        if dst_label:
            label_parts.append(dst_label)
        label = ", ".join(label_parts)
        logging.debug("Found the following label parts: %s", ", ".join(label_parts))

    # add comment part - if show or auto + nothing generated
    add_comment = comment_handling == "show" or (comment_handling == "auto" and not label)
    if add_comment and rule.comment:
        logging.debug("Adding rule comment: %s", rule.comment)
        label = f"{label} *{rule.comment}" if label else rule.comment

    # fall back when no label can be generated and no comment exists
    if not label:
        label = action_label if action_label else rule.id
        logging.debug("No label generated, falling back to label/rule id")

    # add question mark when links are more than one (a jump most likely)
    if len(rule.links) > 1 and rule.has_rule_matchers:
        logging.debug("Rule splits path")
        label += "?"

    # add notes if logging to someplace
    if rule.log == "yes":
        logging.debug("Rule is logged, adding log part %s", rule.log_prefix)
        label += f" !>{{{rule.log_prefix}}}"

    # quote the label
    if any(char in label for char in ["(", ")", "{", "}", "[", "]"]):
        return f'"{label}"'

    return label


def determine_link_label(link: Link) -> str:
    """Generate label for given link. If link has label (not None) it will return that label.
    Otherwise it will generate a label based on the linking nodes.

    Args:
        link (Link): Link

    Returns:
        str: Formatted link label - e.g. |some text here| or empty string when link is chained or
            link has empty "" as label.
    """
    if link.label is not None:
        logging.debug("Using link label '%s' from '%s'", link.label, link)
        return link.label

    if is_link_between_sections(link):
        # find the right name for the section
        if isinstance(link.end, Rule):
            return f"to {link.end.parent.parent.name}:{link.end.parent.name}"
        if isinstance(link.end, Chain):
            return f"to {link.end.parent.name}:{link.end.name}"
        if isinstance(link.end, Section):
            return f"to {link.end.name}"
        return "to unknown parent"

    return ""
