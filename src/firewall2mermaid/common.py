"""Common/utility functions and definitions used by the entire application."""
from __future__ import annotations

import sys
from typing import Callable
from firewall2mermaid.model import GraphSelector, PacketFlowElement, Rule, TreeNode

# Detached element name
DETACHED = "detached"


GRAPH_SELECTOR_PATTERN = r"^(?P<mod>[+*\-%]+)?(?P<id>[^:\s]*(:[^:\s]*){0,2})$"

# Natural sort order of section
SECTION_SORT_ORDER = ["legend", "raw", "mangle", "nat", "filter"]


def section_sort_key(name: str) -> int:
    """Create section sort key.

    Args:
        name (str): Section name

    Returns:
        int: Sort key
    """
    return SECTION_SORT_ORDER.index(name) if name in SECTION_SORT_ORDER else sys.maxsize


# class/styles definitions
CLASS_DEF = {
    "accept": "fill:#2c8c57",
    "tarpit": "fill:#2c8c57",
    "drop": "fill:#8c2c61",
    "src-nat": "fill:#8c572c",
    "dst-nat": "fill:#8c572c",
    "masquerade": "fill:#8c572c",
    "log": "fill:#686d6d",
    "jump": "fill:#44797f",
    "return": "fill:#44797f",
    "passthrough": "fill:#686d6d",
    "fasttrack-connection": "fill:#686d6d",
    "disabled": "fill:#444444",
    "collapsed": "fill:#47605e",
}


def __generate_flow(order: list[tuple[str, str]]) -> PacketFlowElement:
    """Generate flow for given list of ordered tuples (section, chain)

    Args:
        order (list[tuple[str, str]]): List of ordered flow elements (section, chain)

    Returns:
        PacketFlowElement: Root flow element
    """
    root = PacketFlowElement(order[0][0], chain=order[0][1])
    element = root

    for item in order[1:]:
        flow = PacketFlowElement(item[0], item[1])
        element.next = flow
        element = flow

    return root


TAB_CHARACTER = "\t"
"""Tabular character to use for indentation"""

FIREWALL_LINE_START = "/ip firewall"
"""firewall line start"""

LINK_STYLE_BETWEEN_SECTIONS = "-.->"
"""link style between sections"""

LINK_STYLE_NORMAL = "-->"
"""Default link style"""

LINK_STYLE_NORMAL_LONGER = "--->"
"""Elongated link style"""

LINK_STYLE_INVISIBLE = "~~~"
"""Invisible link style"""

DEFAULT_LINK_LABEL = "otherwise"
"""Default link label"""

PREROUTING_FLOW = __generate_flow(
    [("raw", "prerouting"), ("mangle", "prerouting"), ("nat", "dstnat")]
)
"""Packet flow for prerouting phase"""

INPUT_FLOW = __generate_flow([("mangle", "input"), ("filter", "input")])
"""Packet flow for input phase"""

FORWARD_FLOW = __generate_flow([("mangle", "forward"), ("filter", "forward")])
"""Packet flow for forward phase"""

OUTPUT_FLOW = __generate_flow([("raw", "output"), ("mangle", "output"), ("filter", "output")])
"""Packet flow for output phase"""

POSTROUTING_FLOW = __generate_flow([("mangle", "postrouting"), ("nat", "srcnat")])
"""Packet flow for postrouting phase"""

ALL_FLOWS = [PREROUTING_FLOW, INPUT_FLOW, FORWARD_FLOW, OUTPUT_FLOW, POSTROUTING_FLOW]
"""All known flows"""


def find_jump_target_rule(rule: Rule) -> Rule | None:
    """Get the first rule of the chain corresponding to the rule's jump target parameter.

    Args:
        rule (Rule): Jump rule

    Returns:
        Rule|None: Rule or None
    """
    if not rule.jump_target:
        return None

    jump_chain = rule.jump_target_chain()
    if not jump_chain:
        return None

    return jump_chain.first_rule()


def parse_list(list_arg: list[str], sort_key: Callable | None = None) -> list[str]:
    """Parsing list from so that we can also use ',' as separator.

    Args:
        list_arg (list[str]): Input parameters
        sort_key (Callable, optional): If set sort the list using this as sort key parameter

    Returns:
        list[str]: Parsed and sorted list
    """
    l = []
    if list_arg:
        l = [item for x in list_arg for item in x.split(",")]
    if sort_key:
        l.sort(key=sort_key)
    return l


def dedup(items: list) -> list:
    """Deduplicate items in list using equality.

    Args:
        list (list): List to deduplicate

    Returns:
        list: New list with deduplicated items removed
    """
    deduped = []
    for item in items:
        if item not in deduped:
            deduped.append(item)

    return deduped


def create_adhoc_rule(section: str, chain: str, index: int, **kwargs: str) -> Rule:
    """Create adhock rule from given parameters.

    Args:
        section (str): Firewall section
        chain (str): Parent chain name
        index (int): Rule index
        kwargs (dict): Additional rule attributes

    Returns:
        Rule: Created rule
    """
    attributes: dict[str, str] = {"action": "accept", "chain": chain}
    for k, v in kwargs.items():
        attributes[k.replace("-", "_")] = v

    line_args = " ".join([f"{k}={v}" for k, v in attributes.items()])
    line = f"{FIREWALL_LINE_START} {section} add {line_args}"
    return Rule(line, index, attributes)


def get_effective_selector(
    selectors: list[GraphSelector], element_or_id: str | TreeNode
) -> GraphSelector:
    """
    Finds and returns the most applicable GraphSelector for given element or identifier. Selection
    logic is as fallow:
    1. Fully qualified match
    2. Same level selector (precedence by lowest number of wildcards and right most components as
        wildcards) i.e. section:chain:* > section:*:rule > section:*:* > *:*:rule > *:*:*
    3. Default

    Args:
        selectors (list[GraphSelector]): List of selectors
        element_or_id (str|Chain|Section|Rule): Element or id to match

    Returns:
        GraphSelector: Most applicable selector or default selector if no match found
    """
    element_id = element_or_id.id if isinstance(element_or_id, TreeNode) else element_or_id
    id_components = element_id.split(":")
    depth = len(id_components)

    level_selectors = [x for x in selectors if x.depth == depth]

    closest_match: tuple[int, GraphSelector] | None = None
    for selector in level_selectors:
        match_score = _compute_selector_score(id_components, selector)
        if match_score == 0:
            continue
        if match_score == 1:
            return selector
        if not closest_match or closest_match[0] > match_score:
            closest_match = (match_score, selector)

    if closest_match:
        return closest_match[1]

    return GraphSelector("")  # always true graph selector (no selectors specified)


def _compute_selector_score(id_components: list[str], selector: GraphSelector):
    """
    Computes the score of a given selector based on the provided id components.

    Args:
        id_components (list[str]): The list of ID components.
        selector (GraphSelector): The selector object to compute the score for.

    Returns:
        int: The computed score for the selector. Lower scores means closer match with 1 meaning
        perfect match and 0 means no match. Scoring values higher than 1 are related to the number
        and position of wildcards in the selector. The higher the score the less specific the
        selector. Sample example (* indicates selector wildcard match):
            - <match>:<match>:<match> => score 1
            - <match>:<match>:* => score 2
            - <match>:*:<match> => score 4
            - <match>:*:* => score 6
            - *:<match>:<match> => score 8
            - *:<match>:* => 10
            - *:*:<match> => score 12
    """

    l = len(id_components)
    components = selector.selectors

    if len(components) < l:
        return 0

    score = 1
    for i, component in enumerate(id_components):
        if not components[i]:
            score = score + (l - i) ** 2
        elif component != components[i]:
            return 0
    return score
