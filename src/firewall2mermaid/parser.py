"""Microtik File parser."""

import logging
import re
from firewall2mermaid.common import FIREWALL_LINE_START
from firewall2mermaid.model import ParserRule


def __is_firewall_rule_line(line: str) -> bool:
    """Determine if line is a firewall rule

    Args:
        line (str): Raw line string

    Returns:
        bool: True if line is a firewall rule that we can parse
    """
    return line.startswith(
        (
            f"{FIREWALL_LINE_START} filter",
            f"{FIREWALL_LINE_START} nat",
            f"{FIREWALL_LINE_START} raw",
        )
    )


def parse_rules(file_path: str) -> list[ParserRule]:
    """Parse input file and generate list of applicable rules

    Args:
        file_path (str): Input file path

    Returns:
        list[ParserRule]: List of parsed rules. Note that in all rules the parent (chain) is set to
            None.
    """
    logging.info("Parsing rules from %s", file_path)
    rules: list[ParserRule] = []

    with open(file_path, "r", encoding="utf-8") as file:
        for line in file:
            line = line.strip()
            if __is_firewall_rule_line(line):
                logging.debug("Found firewall rule line: %s", line)
                rules.append(parse_rule(line))

    logging.debug("Parsed total %d rules", len(rules))
    return rules


def parse_rule(line: str) -> ParserRule:
    """Parse rule from given rule line

    Args:
        line (str): Firewall rule line as appearing in dump

    Returns:
        ParserRule: Parsed rule
    """
    return ParserRule(line.strip(), attributes=parse_rule_attributes(line))


def parse_rule_attributes(line: str) -> dict[str, str]:
    """Parse rule attributes for given full line.

    Args:
        line (str): Raw file line.

    Returns:
        dict[str, str]: Parsed attributes
    """
    attributes: dict[str, str] = {}
    tokens = re.split(r'\s+(?=(?:(?:[^"]*"){2})*[^"]*$)', line)
    for token in tokens:
        if "=" in token:
            attr, value = token.split("=")
            attr = attr.replace("-", "_")
            value = value.replace('"', "")
            attributes[attr] = value

    attrs = [f"{k}={v}" for k, v in attributes.items()]
    logging.debug("Parsed rule attributes: %s", " ".join(attrs))

    # case rule is added with no action (defaults to accept)
    if "action" not in attributes:
        logging.debug("No action parsed - assigning 'accept' as default")
        attributes["action"] = "accept"

    return attributes
