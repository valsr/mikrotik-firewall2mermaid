"""Module entry point."""
from firewall2mermaid import app


if __name__ == "__main__":
    app.main()
