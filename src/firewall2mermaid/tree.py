"""Creates AST tree that is used for rendering."""
import logging
from typing import Iterator
from firewall2mermaid.common import (
    ALL_FLOWS,
    FORWARD_FLOW,
    INPUT_FLOW,
    LINK_STYLE_INVISIBLE,
    OUTPUT_FLOW,
    POSTROUTING_FLOW,
    PREROUTING_FLOW,
    create_adhoc_rule,
    find_jump_target_rule,
    get_effective_selector,
)
from firewall2mermaid.model import (
    Chain,
    GraphSelector,
    PacketFlowElement,
    ParserRule,
    RootNode,
    Rule,
    Section,
)


class TreeMaker:
    """Constructs a tree graph from parsed rules. The tree is used for rendering the graph."""

    def __init__(
        self,
        parsed_rules: list[ParserRule],
        elements: list[GraphSelector],
        prune_disabled_rules=False,
        prune_log_rules=False,
        add_flow_elements=False,
    ):
        """Constructor

        Args:
            parsed_rules (list[ParserRule]): List of parsed rules to tree-fy
            elements (list[tuple[str,str]]): List of elements to populate tree with. Given in
                section, chain tuple.
            prune_disabled_rules (bool, optional): Whether to ignore/remove disabled nodes. Defaults
                to False.
            prune_log_rules (bool, optional): Whether to ignore/remove log rule nodes. Defaults to
                False.
            add_flow_elements (bool, optional): Add missing flow elements in the packet flow.
                Defaults to False.
        """
        self.parsed_nodes = list(parsed_rules)
        """List of parsed rules to tree-fy"""
        self.root: RootNode
        """Tree root"""
        self.graph_elements = list(elements)
        """List of elements to graph."""
        self.prune_disabled_rules = prune_disabled_rules
        """Whether to ignore/remove disabled nodes."""
        self.prune_log_rules = prune_log_rules
        """Whether to ignore/remove log rule nodes."""
        self.add_flow_elements = add_flow_elements
        """Add missing flow elements in the packet flow."""

    def make_tree(self) -> RootNode:
        """Construct a tree graph for given list of rules.

        Returns:
            Root: Constructed tree
        """
        logging.info("Constructing graph tree")
        self.root = RootNode()

        self._populate_tree()
        if self.add_flow_elements:
            self._add_missing_flow_elements()
        self._prune_tree()

        self._wire_relationships()

        return self.root

    def _prune_tree(self):
        """Prune generated tree"""
        logging.info("Pruning tree")
        prune_sections = []
        for section in self.root.sections:
            prune_chains = [
                chain for chain in section.chains if not self._should_graph_chain(chain)
            ]
            if len(prune_chains) == len(section.chains):
                prune_sections.append(section)
            else:
                for chain in prune_chains:
                    logging.debug("Pruning chain '%s'", chain)
                    chain.detach()

        for section in prune_sections:
            logging.debug("Pruning section '%s'", section)
            section.detach()

        for chain in self.root.chains:
            self._prune_rules(chain)

    def _prune_rules(self, chain: Chain):
        """Prune rules from chain based on pruning rules

        Args:
            chain (Chain): Chain to prune rules from
        """
        logging.debug("Pruning rules from chain '%s'", chain)
        prune_rules = []

        if self.prune_disabled_rules:
            prune_rules = [rule for rule in chain.rules if rule.disabled]

        if self.prune_log_rules:
            prune_rules.extend(rule for rule in chain.rules if rule.action == "log")

        for rule in prune_rules:
            logging.debug("Pruning rule '%s'", rule)
            rule.detach()

    def _populate_tree(self):
        """Populate tree from parsed rules without filtering any content."""
        for parsed_rule in self.parsed_nodes:
            section = self.root.get_or_add_section(parsed_rule.section)
            chain = section.get_or_add_chain(parsed_rule.chain)

            rule = Rule(
                line=parsed_rule.line,
                index=self.root.next_rule_index(),
                attributes=parsed_rule.attributes,
            )
            attrs = [f"{x}={rule.x}" for x in rule.known_attributes]
            logging.debug(
                "Adding rule '%s' at index %s with attributes: %s",
                rule.id,
                rule.index,
                " ".join(attrs),
            )
            chain.add_rule(rule)

    def _should_graph_chain(self, chain: Chain, chains_in_flow: list[Chain] | None = None) -> bool:
        """Check if chain should be graphed. Chain should be graphed if:
        1. The chain is not empty
        2. The chain is included in the graphing filters
        3. Chain is jumped to from another chain that will be graphed"""
        render = False
        chains_in_flow = chains_in_flow or []
        # use section selector to determine if chain should be graphed at first
        selector = get_effective_selector(self.graph_elements, chain.parent)
        render = selector.render

        # overwrite when chain selector is more specific
        selector = get_effective_selector(self.graph_elements, chain)
        if selector.remove or selector.splat:
            render = selector.render

        # check if chain is jumped by another chain that is to be graphed (this is a slow process
        # and could result in circular flow)
        if chain in chains_in_flow:
            logging.warning("Circular chain flow detected ignoring current check")
            return render

        for jump_chain in [
            x.parent
            for x in self.root.rules
            if x.action == "jump"
            and x.parent.parent == chain.parent
            and x.jump_target == chain.name
            and x.parent not in chains_in_flow
        ]:
            if self._should_graph_chain(jump_chain, chains_in_flow + [chain]):
                render = True

        logging.debug("Should render chain '%s': %s", chain.id, render)
        return render

    def _determine_graph_chains(self, tree: RootNode) -> list[Chain]:
        """Determine the chains that should be graphed in the given tree based on:
        1. Graphing filters
        2. Inclusions due to rule jumps

        Note this is a slow function so call it seldomly
        """
        chains: list[Chain] = []

        for section in tree.sections:
            for chain in section.chains:
                if self._should_graph_chain(chain):
                    logging.debug("Chain '%s' should be graphed", chain)
                    chains.append(chain)

        return chains

    def _list_has_chain(self, chains: list[Chain], section_name: str, chain_name: str) -> bool:
        """Check if section has chain with given name

        Args:
            chains (list[Chain]): List of chains to check if it contains given chain
            section (str): Section name to check
            chain_name (str): Chain name to check

        Returns:
            bool: True if section has chain with given name
        """
        return any(x for x in chains if x.name == chain_name and x.parent.name == section_name)

    def _add_missing_flow_elements(self):
        """Adds the missing packet flow elements (sections and chains) based on the current tree
        sections and chains"""
        graph_chains = self._determine_graph_chains(self.root)

        flows: list[PacketFlowElement] = []
        if (
            self._list_has_chain(graph_chains, "filter", "forward")
            or self._list_has_chain(graph_chains, "filter", "input")
            or self._list_has_chain(graph_chains, "nat", "srcnat")
            or self._list_has_chain(graph_chains, "mangle", "prerouting")
        ):
            flows.append(PREROUTING_FLOW)

        if self._list_has_chain(graph_chains, "filter", "input"):
            flows.append(INPUT_FLOW)

        if self._list_has_chain(graph_chains, "filter", "forward"):
            flows.append(FORWARD_FLOW)

        if self._list_has_chain(graph_chains, "filter", "output"):
            flows.append(OUTPUT_FLOW)

        if (
            self._list_has_chain(graph_chains, "filter", "forward")
            or self._list_has_chain(graph_chains, "filter", "output")
            or self._list_has_chain(graph_chains, "nat", "srcnat")
            or self._list_has_chain(graph_chains, "mangle", "postrouting")
        ):
            flows.append(POSTROUTING_FLOW)

        # add upstream chains based on flow
        for flow in flows:
            for item in iter(flow):
                section = self.root.get_or_add_section(item.section)
                if not section.has_chain(item.chain):
                    chain = section.add_chain(item.chain)
                    # add rule
                    chain.add_rule(
                        create_adhoc_rule(
                            section.name,
                            chain.name,
                            self.root.next_rule_index(),
                            action="accept",
                            comment="rule added for packet flow",
                        )
                    )
                # add if not already in graph_elements
                selector = get_effective_selector(
                    self.graph_elements, f"{item.section}:{item.chain}"
                )
                if not selector.render:
                    logging.debug(
                        "Adding missing flow element (%s, %s) to graph selector",
                        item.section,
                        item.chain,
                    )
                    self.graph_elements.append(GraphSelector(f"{item.section}:{item.chain}"))

    def _wire_relationships(self):
        for sections in self.root.sections:
            self._wire_section_relationships(sections)

        # packets flow from chains between section, there is an order between chains and within
        # chains
        self._add_flow_links_in_flows()
        self._add_links_between_flows()

    def _add_flow_links_in_flows(self):
        """Add packet flow links between items in same flow chain. For example for prerouting
        chain, the packet flow is raw prerouting -> mangle prerouting -> dstnat.
        """
        logging.debug("Adding packet flow links (chain level)")

        for flow in ALL_FLOWS:
            start_rule = None
            for item in flow:
                section = self.root.section(item.section)
                if not section:
                    flow = item.next
                    continue
                chain = section.chain(item.chain)
                if not chain:
                    flow = item.next
                    continue

                chain_first_rule = chain.first_rule()
                if (
                    start_rule
                    and (not start_rule.is_terminal_rule or start_rule.action == "accept")
                    and chain_first_rule
                ):
                    logging.debug(
                        "Adding link from '%s' to chain '%s'", start_rule.id, chain_first_rule.id
                    )
                    start_rule.add_link(chain_first_rule)
                    start_rule.parent.add_link(chain, LINK_STYLE_INVISIBLE)
                start_rule = chain.last_rule()

    def _add_links_between_flows(self):
        """Add links between flows"""
        logging.debug("Adding links between flows")

        # prerouting -> input
        prerouting = self._find_first_chain_in_flow(reversed(PREROUTING_FLOW))
        input_chain = self._find_first_chain_in_flow(INPUT_FLOW)
        if prerouting and input_chain:
            logging.debug("Adding link from '%s' to '%s'", prerouting.id, input_chain.id)
            prerouting.add_link(input_chain, LINK_STYLE_INVISIBLE)
            # add visible link
            last_rule = prerouting.last_rule()
            first_rule = input_chain.first_rule()
            if first_rule and last_rule:
                last_rule.add_link(first_rule)

        # prerouting -> forward
        forward = self._find_first_chain_in_flow(FORWARD_FLOW)
        if prerouting and forward:
            logging.debug("Adding link from '%s' to '%s'", prerouting.id, forward.id)
            prerouting.add_link(forward, LINK_STYLE_INVISIBLE)
            # add visible link
            last_rule = prerouting.last_rule()
            first_rule = forward.first_rule()
            if first_rule and last_rule:
                last_rule.add_link(first_rule)

        # forward -> postrouting
        forward_end = self._find_first_chain_in_flow(reversed(FORWARD_FLOW))
        postrouting = self._find_first_chain_in_flow(POSTROUTING_FLOW)
        if forward_end and postrouting:
            logging.debug("Adding link from '%s' to '%s'", forward_end.id, postrouting.id)
            forward_end.add_link(postrouting, LINK_STYLE_INVISIBLE)
            # add visible link
            last_rule = forward_end.last_rule()
            first_rule = postrouting.first_rule()
            if first_rule and last_rule:
                last_rule.add_link(first_rule)

        # output -> postrouting
        output = self._find_first_chain_in_flow(reversed(OUTPUT_FLOW))
        if output and postrouting:
            logging.debug("Adding link from '%s' to '%s'", output.id, postrouting.id)
            output.add_link(postrouting, LINK_STYLE_INVISIBLE)
            # add visible link
            last_rule = output.last_rule()
            first_rule = postrouting.first_rule()
            if first_rule and last_rule:
                last_rule.add_link(first_rule)

    def _find_first_chain_in_flow(self, flow: PacketFlowElement | Iterator[PacketFlowElement]):
        for item in flow:
            section = self.root.section(item.section)
            if not section:
                continue
            chain = section.chain(item.chain)
            if chain:
                return chain
        return None

    def _wire_section_relationships(self, section: Section):
        """Add relationships for rules in section

        Args:
            section (Section): Section to wire relationships
        """
        logging.debug("Adding links between rules in section %s", section.name)
        for chain in section.chains:
            rules = chain.rules
            start_rule = None
            for rule in sorted(rules, key=lambda x: x.index):
                if start_rule:
                    logging.debug("Adding link from '%s' to '%s'", start_rule.id, rule.id)
                    label = "otherwise" if start_rule.action == "jump" else None
                    start_rule.add_link(rule, label=label)
                start_rule = rule
                if rule.action == "jump":
                    logging.debug("Rule '%s' jumps to '%s'", rule.id, rule.parent)
                    wired = self._wire_jump_rule(rule)
                    # clear from rule as to not add link from the jump rule to the next rule if it
                    # is a blank jump
                    if wired and not rule.has_rule_matchers:
                        start_rule = None

    def _wire_jump_rule(self, rule: Rule) -> bool:
        """Wire relationships for jump rule

        Args:
            rule (Rule): Jump rule

        Returns:
            bool: True if the rule has been wired properly, False if jump target couldn't be found
        """
        section = rule.parent.parent

        jump_target = find_jump_target_rule(rule)
        if not jump_target:
            logging.warning("Can't find chain '%s' in '%s'", rule.jump_target, section)
            return False

        logging.debug("Adding link for jump from '%s' to '%s'", rule.id, jump_target.id)
        label = "if yes" if rule.has_rule_matchers else None
        rule.add_link(jump_target, label=label)

        # find the next rule to return to
        next_rule = rule.next_rule()
        if not next_rule:
            return True

        # add links from explicit returns in the jump chain to the next rule
        return_rules: list[Rule] = [x for x in jump_target.parent.rules if x.action == "return"]
        for return_rule in return_rules:
            logging.debug("Adding returning from '%s' to '%s'", return_rule.id, next_rule.id)
            return_rule.add_link(next_rule, label="return")

        # add link from implicit return (non-terminal last nodes), unless is return
        last_rule = jump_target.parent.last_rule()
        if last_rule and not last_rule.is_terminal_rule and last_rule.action != "return":
            logging.debug("Adding implicit returning from '%s' to '%s'", last_rule.id, next_rule.id)
            last_rule.add_link(next_rule, label="return")
        return True
