"""Application startup logic."""

import logging
import os
import argparse
import re
import sys
import importlib.metadata

from firewall2mermaid.common import GRAPH_SELECTOR_PATTERN, parse_list
from firewall2mermaid.errors import InvalidGraphSelector
from firewall2mermaid.model import GraphSelector, ParserRule
from firewall2mermaid.parser import parse_rules
from firewall2mermaid.render import Renderer
from firewall2mermaid.tree import TreeMaker


class NegateAction(argparse.Action):
    """Utility to construct toggle command line options (i.e. --show --no-show)."""

    def __call__(self, parser, namespace, values, option_string=None):
        if option_string:
            setattr(namespace, self.dest, option_string[2:4] != "no")


def main():
    """CLI main entry point."""

    args = create_parser().parse_args()
    setup_logging(args.verbose)
    logging.debug("Args: %s", args)

    parsed_elements = parse_graph_selector(parse_list(args.graph))
    logging.debug("Parsed graph elements: %s", parsed_elements)
    output_file: str = determine_output_filename(args.input, args.output)

    extra_args = []
    if args.log_command:
        command_arg = "Command: " + " ".join(sys.argv[1:])
        extra_args.append(command_arg)

    execute(
        args.input,
        parsed_elements,
        output_file,
        show_logs=args.show_logs,
        show_disabled=args.show_disabled,
        comments=args.comments[0],
        show_legend=args.show_legend,
        direction=args.direction[0] if args.direction else "TB",
        log_rules=args.log_rules,
        add_flow_elements=args.flow,
        collapse_as_rule=args.collapsed_as_rule,
        show_root=not args.rootless,
        extra_logs=extra_args,
    )


def setup_logging(verbose: bool):
    """Setup logging.

    Args:
        verbose (bool): Whether to use verbose loggin
    """
    level = logging.DEBUG if verbose else logging.INFO
    logging.basicConfig(level=level, format="%(asctime)s - %(levelname)s - %(message)s")


def is_file_valid(parser, arg):
    """
    Check if the given file is valid and readable.

    Args:
        parser (argparse.ArgumentParser): The argument parser object.
        arg (str): The file path to check.

    Returns:
        str: The valid file path. False if not valid
    """
    if not os.path.isfile(arg) or not os.access(arg, os.R_OK):
        parser.error("The file %s does not exist or we can't read it!", arg)
        return False
    return arg


def create_parser() -> argparse.ArgumentParser:
    """Construct argument parser.

    Returns:
        argparse.ArgumentParser: Application parser
    """
    parser = argparse.ArgumentParser(
        description="Graph MikroTik firewall rules to mermaid js",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    version = importlib.metadata.metadata("firewall2mermaid")["Version"]
    parser.add_argument("--version", action="version", version=version)
    parser.add_argument(
        "--input",
        required=True,
        metavar="FILE",
        type=lambda x: is_file_valid(parser, x),
        help="Path to the input file containing firewall rules",
    )
    parser.add_argument(
        "--output",
        help="Path to the output file to store the processed results (default: input.mmd)",
    )
    parser.add_argument(
        "--graph",
        nargs="+",
        help="""Graph selected elements. Specify items using graph selector format -
`[mod]section:chain`. 'mod' are optional characters that modifies how the elements are graphed,
when present they have the meaning:

- +: Render the given section/chain normally
- *: Will splat the given section/chain (i.e. render ungrouped)
- -: Will not render the given section/chain (note takes precedence over other modifiers)
- %%: Will collapse the given section/chain

Either section or chain may be omitted to specify 'all' elements of that type.
For example ':ddos' will graph all chains named 'ddos' of all sections. Similarly 'raw:' or 'raw'
will graph the entire 'raw' section.

You can use ':' as a global selector to catch all-else scenarios. For example '-:' will hide all
element that are not explicitly selected. Similarly '+:' will render all elements that are not""",
    )
    parser.add_argument(
        "--comments",
        "--comment-handling",
        "--node-comments",
        choices=["show", "hide", "auto", "prefer"],
        default="auto",
        nargs=1,
        help="Show/hide comments on graph nodes",
    )
    parser.add_argument(
        "--show-logs",
        "--no-show-logs",
        action=NegateAction,
        dest="show_logs",
        default=False,
        help="Shows log nodes",
        nargs=0,
    )
    parser.add_argument(
        "--show-disabled",
        "--no-show-disabled",
        action=NegateAction,
        dest="show_disabled",
        default=False,
        help="Shows disabled nodes",
        nargs=0,
    )
    parser.add_argument(
        "--show-legend",
        "--no-show-legend",
        action=NegateAction,
        dest="show_legend",
        default=True,
        help="Shows node legend",
        nargs=0,
    )
    parser.add_argument(
        "--direction",
        choices=["TB", "BT", "LR", "RL"],
        help="Graph direction",
        default=["TB"],
        nargs=1,
    )
    parser.add_argument(
        "--log-rules",
        action="store_true",
        default=False,
        help="Add a comment/log for each rule in the graph",
    )
    parser.add_argument(
        "--flow",
        "--add-flow-elements",
        action="store_true",
        default=False,
        help="Add upstream/downstream flow elements (as in packet flow)",
    )
    parser.add_argument(
        "--collapsed-as-rule",
        "--collapse-as-rule",
        action="store_true",
        default=False,
        help="Render collapsed elements as rules in current chain/section",
    )
    parser.add_argument(
        "--rootless",
        "--no-root",
        action="store_true",
        default=False,
        help="Render graph without root node",
    )
    parser.add_argument(
        "--log-command",
        action="store_true",
        default=False,
        help="Log execution command line",
    )
    parser.add_argument("--verbose", "-v", action="store_true", help="Increase verbosity")

    return parser


def determine_output_filename(
    input_file_arg: str,
    output_arg: str | None,
) -> str:
    """Determine output file path for given input parameters.

    Args:
        input_file_arg (str): Input file argument
        output_arg (str | None): Output file argument (if specified)

    Returns:
        str: Output file path.
    """
    if not output_arg:
        output_arg = os.path.splitext(input_file_arg)[0]
        output_arg += ".mmd"

    return output_arg


def execute(
    file_path: str,
    graph: list[GraphSelector],
    output_file: str,
    show_disabled=False,
    show_logs=False,
    comments="auto",
    show_legend=True,
    direction="TB",
    log_rules=False,
    add_flow_elements=False,
    collapse_as_rule=False,
    show_root=True,
    extra_logs: list[str] | None = None,
):
    """Main application execution.

    Args:
        file_path (str): Input file (Mikrotik file)
        graph (list[GraphSelector]): List of elements to graph.
        output_file (str): Output file (where to store graph file)
        show_disabled (bool, optional): Show disabled nodes on graph. Defaults to False.
        show_logs (bool, optional): Show log nodes. Defaults to False.
        comments (str, optional): Node comment handling. Defaults to 'auto'.
        show_legend (bool, optional): Show legend nodes. Defaults to True.
        direction (str, optional): Overall graph direction. Defaults to "TB".
        log_rules (bool, optional): Log all rules as comments at the beginning of the graph.
            Defaults to False.
        add_flow_elements (bool, optional): Add upstream elements (chains, sections, etc).
            Defaults to False.
        collapse_as_rules (bool, optional): Render collapsed chains as rules instead. Defaults to
            False.
        show_root (bool, optional): Render graph with root node. Defaults to True.
    """
    extra_logs = extra_logs or []
    rules: list[ParserRule] = parse_rules(file_path)

    tree_maker = TreeMaker(
        parsed_rules=rules,
        elements=graph,
        prune_disabled_rules=not show_disabled,
        prune_log_rules=not show_logs,
        add_flow_elements=add_flow_elements,
    )
    root = tree_maker.make_tree()

    renderer = Renderer(
        root,
        graph_selector=graph,
        node_render_mode=comments,
        show_legend=show_legend,
        graph_direction=direction,
        log_rules=log_rules,
        collapse_as_rule=collapse_as_rule,
        show_root=show_root,
        extra_logs=extra_logs,
    )
    graph_text = renderer.render_graph()

    with open(output_file, "w", encoding="utf-8") as out_file:
        out_file.write(graph_text)
        logging.info("Graph data save to %s", output_file)


def parse_graph_selector(elements: list[str]) -> list[GraphSelector]:
    """
    Parses a list of graph selector elements and returns a list of GraphSelector objects.

    Args:
        elements (list[str]): The list of graph selector elements to parse.

    Returns:
        list[GraphSelector]: A list of GraphSelector objects.

    Raises:
        InvalidGraphSelector: If an element in the list does not match the expected pattern.
    """

    p = re.compile(GRAPH_SELECTOR_PATTERN)
    t: list[GraphSelector] = []
    for element in elements:
        m = p.match(element)
        if not m:
            raise InvalidGraphSelector(element)
        d = m.groupdict()
        t.append(GraphSelector(d["id"] or "", mod=d["mod"] or ""))
    return t
