import random
from typing import Callable
from unittest.mock import Mock

import pytest
from pytest_mock import mocker
from firewall2mermaid.errors import (
    ChainAlreadyExistsError,
    ElementNotFound,
    SectionAlreadyExistsError,
)
from firewall2mermaid.model import (
    Chain,
    GraphSelector,
    Link,
    LinkableTreeNode,
    PacketFlowElement,
    PacketFlowIterator,
    ParserRule,
    RootNode,
    Rule,
    Section,
    TreeNode,
)
from firewall2mermaid.parser import parse_rule
from assertpy import assert_that

from tests.conftest import (
    _parser_rule_factory,
    _rule_factory,
    known_rule_attributes_for_generation,
    rule_matching_attributes,
    terminal_actions,
)


class DummyLinkNode(LinkableTreeNode):
    def __init__(self, id=None):
        self._id = random.randint(0, 1000000) if id is None else id

    def __eq__(self, __value: object) -> bool:
        if isinstance(__value, DummyLinkNode):
            return self.id == __value.id

        return False

    @property
    def id(self) -> int:
        return self._id


def assert_rules_are_equivalent(rule: Rule | None, other: Rule | None):
    assert rule
    assert other
    assert_that(rule.line).is_equal_to(other.line)
    assert_that(rule.index).is_equal_to(other.index)
    assert_that(rule.loggable).is_equal_to(other.loggable)

    assert_that(rule.known_attributes).is_equal_to(other.known_attributes)
    for attribute in rule.known_attributes:
        assert_that(rule.__getattribute__(attribute)).is_equal_to(other.__getattribute__(attribute))


def assert_chains_are_equivalent(chain: Chain, other: Chain):
    assert_that(chain.render).is_equal_to(other.render)

    assert_that(len(chain.rules)).is_equal_to(len(other.rules))
    for rule in chain.rules:
        r = [x for x in other.rules if x.index == rule.index][0]
        assert_rules_are_equivalent(rule, r)


class TestLink:
    def test_link_has_id(self):
        assert_that(Link(DummyLinkNode(), DummyLinkNode()).id).is_not_none()

    def test_link_equals_to_self(self):
        sut = Link(DummyLinkNode(), DummyLinkNode())

        assert_that(sut).is_equal_to(sut)

    def test_similar_link_equals_to_self(self):
        start = DummyLinkNode()
        end = DummyLinkNode()

        sut = Link(start, end)
        other = Link(start, end)

        assert_that(sut).is_equal_to(other)

    def test_clone_link_equals_to_clone(self):
        sut = Link(DummyLinkNode(), DummyLinkNode())

        assert_that(sut.clone()).is_equal_to(sut)


class TestParserRule:
    def test_when_parsing_rule_extra_spaces_are_trimmed(self):
        line = "a line with space "
        rule = parse_rule(line)
        assert_that(rule.line).is_equal_to(line.strip())


def LinkableTreeNodeCreator(root: RootNode | None = None):
    if not root:
        root = RootNode()
    return LinkableTreeNode(root)


def SectionCreator(root: RootNode | None = None):
    if not root:
        root = RootNode()
    return root.add_section(f"SUT{random.randint(0, 10000)}")


def ChainCreator(root: RootNode | None = None):
    if not root:
        root = RootNode()
    section = root.add_section("SUT") if not root.has_section("SUT") else root.section("SUT")
    assert section
    return section.add_chain(f"SUT{random.randint(0, 10000)}")


def RuleCreator(root: RootNode | None = None):
    if not root:
        root = RootNode()
    section = root.add_section("SUT") if not root.has_section("SUT") else root.section("SUT")
    assert section
    chain = section.add_chain("SUT")
    assert chain
    rule = _rule_factory("SUT", "SUT", random.randint(0, 10000))
    chain.add_rule(rule)
    return rule


def RootNodeCreator(parent: TreeNode | None = None):
    return RootNode()


def TreeNodeCreator(parent: TreeNode | None = None):
    if not parent:
        parent = Mock()
    return TreeNode(parent)


class TestTreeNode:
    @pytest.mark.parametrize("sut_maker", [RootNodeCreator])
    def test_tree_root_when_self_is_root_returns_self(self, sut_maker: Callable[..., RootNode]):
        sut = sut_maker()
        assert_that(sut.tree_root).is_equal_to(sut)

    @pytest.mark.parametrize(
        "sut_maker",
        [TreeNodeCreator, LinkableTreeNodeCreator, SectionCreator, ChainCreator, RuleCreator],
    )
    def test_tree_root_when_self_is_not_root_returns_root_of_tree(
        self, sut_maker: Callable[..., TreeNode] | Callable[..., LinkableTreeNode]
    ):
        root = RootNode()
        sut = sut_maker(root)
        assert sut
        assert_that(sut.tree_root).is_equal_to(root)

    def test_eq_when_same_object_returns_true(self):
        sut = TreeNode(DummyLinkNode())
        assert_that(sut).is_equal_to(sut)

    def test_eq_when_different_objects_returns_false(self):
        assert_that(TreeNode(DummyLinkNode(1))).is_not_equal_to(TreeNode(DummyLinkNode(2)))


@pytest.mark.parametrize(
    "sut_maker", [LinkableTreeNodeCreator, SectionCreator, ChainCreator, RuleCreator]
)
class TestLinkableTreeNode:
    def test_add_link_when_when_target_not_in_tree_raise_elementnotfound(self, sut_maker):
        sut = sut_maker(RootNode())
        target = sut_maker(RootNode())
        assert_that(sut.add_link).raises(ElementNotFound).when_called_with(target)

    def test_add_link_when_params_are_good_link_is_properly_created_and_added(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)
        target = root.add_section("to")
        assert target
        sut.add_link(target, "style", "label")

        assert_that(sut.links).is_length(1)
        assert_that(sut.links[0]).has_start(sut).has_end(target).has_style("style").has_label(
            "label"
        )

    def test_add_existing_link_when_added_removes_links_from_previous_link_owner(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)
        old_node = root.add_section("old")
        target = root.add_section("to")
        assert target
        assert old_node
        old_node.add_link(target)

        sut.add_existing_link(old_node.links[0])

        assert_that(old_node.links).is_empty

    def test_add_existing_link_repoints_start_of_link(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)
        old_node = root.add_section("old")
        target = root.add_section("to")
        assert target
        assert old_node
        old_node.add_link(target)

        sut.add_existing_link(old_node.links[0])

        assert_that(sut.links).is_length(1)
        assert_that(sut.links[0]).has_start(sut)

    def test_add_existing_link_when_target_does_not_exist_in_treeraise_elementnotfound(
        self, sut_maker
    ):
        root = RootNode()
        sut = sut_maker(root)
        target = RootNode().add_section("to")
        assert target

        assert_that(sut.add_existing_link).raises(ElementNotFound).when_called_with(
            Link(sut, target)
        )

    def test_add_existing_link_when_previous_owner_does_not_have_link_does_not_raise_error(
        self, sut_maker
    ):
        root = RootNode()
        sut = sut_maker(root)
        target = root.add_section("to")
        assert target

        try:
            sut.add_existing_link(Link(target, target))
        except BaseException:
            pytest.fail("Should not throw exception when link does not exist in previous owner")

    def test_remove_link_when_link_found_is_removed(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)
        target = root.add_section("to")
        assert target
        sut.links.append(Link(target, target))

        sut.remove_link(sut.links[0])
        assert_that(sut.links).is_length(0)

    def test_remove_link_when_link_not_found_raise_valueerror(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)

        assert_that(sut.remove_link).raises(ValueError).when_called_with(Link(sut, sut))

    def test_remove_link_when_link_not_found_does_not_raise_error(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)
        target = root.add_section("to")
        assert target

        sut.add_link(target)
        try:
            sut.remove_links_to(target)
        except BaseException:
            pytest.fail("Should not throw exception")

    def test_remove_link_when_target_matches_multiple_links_all_are_removed(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)
        target = root.add_section("to")
        assert target
        sut.links.append(Link(sut, target, label="1"))
        sut.links.append(Link(sut, target, label="2"))
        sut.links.append(Link(sut, target, label="3"))

        sut.remove_links_to(target)

        assert_that(sut.links).is_length(0)

    def test_clone_links_links_are_equal_to_clone_links(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)
        target = root.add_section("to")
        assert target
        sut.add_link(target)

        cloned = sut.clone_links()

        assert_that(cloned).is_equal_to(sut.links)

    def test_clone_links_cloned_links_are_different_objects(self, sut_maker):
        root = RootNode()
        sut = sut_maker(root)
        target = root.add_section("to")
        assert target
        sut.add_link(target)

        cloned = sut.clone_links()
        cloned[0].start = target
        cloned.append(Link(target, target))

        assert_that(cloned).is_not_equal_to(sut.links)
        assert_that(sut.links).does_not_contain(Link(target, target))


class TestRule:
    def test_init_sets_defaults(self, parser_rule_factory: Callable[..., ParserRule]):
        parser_rule: ParserRule = parser_rule_factory("filter", "input", comment="test rule")
        sut = Rule(parser_rule.line, 0, parser_rule.attributes)

        assert_that(sut.parent).is_not_none().has_name("detached")
        assert_that(sut.known_attributes).contains("chain")
        assert_that(sut).has_loggable(True)
        assert_that(sut).has_index(0)
        assert_that(sut).has_raw_line(parser_rule.line)

    def test_id_is_set(self, rule_factory: Callable[..., Rule]):
        sut = rule_factory("filter", "input", 0, comment="test rule")

        assert_that(sut.id).is_not_none().contains(":rule0")

    def test_chain_equals_parent_name(self, rule_factory: Callable[..., Rule]):
        sut = rule_factory("filter", "input", 0, comment="test rule")

        assert_that(val=sut).has_chain(sut.parent.name)

    def test_line_equals_raw_line(self, rule_factory: Callable[..., Rule]):
        sut = rule_factory("filter", "input", 0, comment="test rule")

        assert_that(val=sut).has_line(sut.raw_line)

    @pytest.mark.parametrize("attribute", known_rule_attributes_for_generation)
    def test_has_rule_matchers_when_given_attribute_correctly_determines_result(
        self, attribute: str
    ):
        sut = _rule_factory("filter", "input", 0, **{attribute: "test"})

        assert_that(sut).has_has_rule_matchers(attribute in rule_matching_attributes)

    @pytest.mark.parametrize("action", terminal_actions)
    def test_is_terminal_rule_when_rule_is_terminal_is_true(self, action: str):
        sut = _rule_factory("filter", "input", 0, action=action, comment="test")

        assert_that(sut).has_is_terminal_rule(True)

    @pytest.mark.parametrize("action", terminal_actions)
    def test_is_terminal_status_when_action_is_terminal_when_non_terminal_attribute_present_is_false(
        self, action: str
    ):
        parser_rule = _parser_rule_factory("filter", "input", action=action, ttl="test")
        sut = Rule(parser_rule.line, 0, parser_rule.attributes)

        assert_that(sut).has_is_terminal_rule(False)

    @pytest.mark.parametrize("action", known_rule_attributes_for_generation - terminal_actions)
    def test_is_terminal_status_when_action_is_non_terminal_is_false(self, action: str):
        sut = _rule_factory("filter", "input", 0, action=action, ttl="test")

        assert_that(sut).has_is_terminal_rule(False)

    def test_lt_uses_index_for_comparison(self, rule_factory: Callable[..., Rule]):
        rule = rule_factory("filter", "input", 5, comment="test rule")
        equal_rule = rule_factory("filter", "input", 5, comment="test rule")
        higher_rule = rule_factory("filter", "input", 10, comment="test rule")
        lower_rule = rule_factory("filter", "input", 0, comment="test rule")

        assert_that(rule < lower_rule).is_false()
        assert_that(rule < equal_rule).is_false()
        assert_that(rule < higher_rule).is_true()

    def test_le_uses_index_for_comparison(self, rule_factory: Callable[..., Rule]):
        rule = rule_factory("filter", "input", 5, comment="test rule")
        equal_rule = rule_factory("filter", "input", 5, comment="test rule")
        higher_rule = rule_factory("filter", "input", 10, comment="test rule")
        lower_rule = rule_factory("filter", "input", 0, comment="test rule")

        assert_that(rule <= lower_rule).is_false()
        assert_that(rule <= equal_rule).is_true()
        assert_that(rule <= higher_rule).is_true()

    def test_gt_uses_index_for_comparison(self, rule_factory: Callable[..., Rule]):
        rule = rule_factory("filter", "input", 5, comment="test rule")
        equal_rule = rule_factory("filter", "input", 5, comment="test rule")
        higher_rule = rule_factory("filter", "input", 10, comment="test rule")
        lower_rule = rule_factory("filter", "input", 0, comment="test rule")

        assert_that(rule > lower_rule).is_true()
        assert_that(rule > equal_rule).is_false()
        assert_that(rule > higher_rule).is_false()

    def test_ge_uses_index_for_comparison(self, rule_factory: Callable[..., Rule]):
        rule = rule_factory("filter", "input", 5, comment="test rule")
        equal_rule = rule_factory("filter", "input", 5, comment="test rule")
        higher_rule = rule_factory("filter", "input", 10, comment="test rule")
        lower_rule = rule_factory("filter", "input", 0, comment="test rule")

        assert_that(rule >= lower_rule).is_true()
        assert_that(rule >= equal_rule).is_true()
        assert_that(rule >= higher_rule).is_false()

    def test_next_rule_gives_correctly_next_rule_by_index(self, rule_factory: Callable[..., Rule]):
        rule = rule_factory("filter", "input", 0, comment="test rule")
        next_rule = rule_factory("filter", "input", 1, comment="test rule")
        rule.parent.add_rule(next_rule)

        assert_that(rule.next_rule()).is_not_none().is_equal_to(next_rule)

    def test_next_rule_when_rule_last_in_chain_returns_none(
        self, rule_factory: Callable[..., Rule]
    ):
        rule = rule_factory("filter", "input", 0, comment="test rule")
        assert_that(rule.next_rule()).is_none()

    def test_jump_target_chain_when_chain_doesnt_exist_raises_elementnotfound(
        self, rule_factory: Callable[..., Rule]
    ):
        rule = rule_factory("filter", "input", 0, jump_target="test")

        assert_that(rule.jump_target_chain).raises(ElementNotFound)

    def test_jump_target_chain_returns_chain_in_same_section(
        self, rule_factory: Callable[..., Rule]
    ):
        rule = rule_factory("filter", "input", 0, jump_target="test")
        chain = Chain("test")
        rule.parent.parent.add_chain(chain)

        assert_that(rule.jump_target_chain()).is_equal_to(chain)

    def test_jump_target_chain_when_target_not_in_section_raises_elementnotfound(
        self, rule_factory: Callable[..., Rule]
    ):
        rule = rule_factory("filter", "input", 0, jump_target="test")
        rule.tree_root.add_section("foreign").add_chain("test")

        assert_that(rule.jump_target_chain).raises(ElementNotFound)

    def test_clone_returns_rule_equivalent_to_rule(self, rule_factory: Callable[..., Rule]):
        rule = rule_factory("filter", "input", 0, jump_target="test")
        clone = rule.clone()

        assert_rules_are_equivalent(rule, clone)

    def test_clone_returns_new_object(self, rule_factory: Callable[..., Rule]):
        rule = rule_factory("filter", "input", 0, jump_target="test")
        clone = rule.clone()

        assert_that(rule).is_not_same_as(clone)

    def test_eq_when_other_is_self_return_true(self, rule_factory):
        sut = rule_factory("filter", "input", 0, jump_target="test")

        assert_that(sut).is_equal_to(sut)

    def test_eq_when_other_is_equal_return_true(self, rule_factory):
        sut = rule_factory("filter", "input", 0, jump_target="test")
        other = rule_factory("filter", "input", 0, jump_target="test")

        assert_that(sut).is_equal_to(other)

    def test_eq_when_other_has_different_section_return_true(self, rule_factory):
        sut = rule_factory("filter", "input", 0, jump_target="test")
        other = rule_factory("raw", "input", 0, jump_target="test")

        assert_that(sut).is_equal_to(other)

    def test_eq_when_other_has_different_chain_return_true(self, rule_factory):
        sut = rule_factory("filter", "input", 0, jump_target="test")
        other = rule_factory("filter", "output", 0, jump_target="test")

        assert_that(sut).is_equal_to(other)

    def test_eq_when_other_is_different_attributes_return_false(self, rule_factory):
        sut = rule_factory("filter", "input", 0, jump_target="test")
        other = rule_factory("filter", "input", 0, jump_target="test", action="drop")

        assert_that(sut).is_not_equal_to(other)

    def test_eq_rules_differ_only_in_liks_return_true(self, rule_factory):
        sut = rule_factory("filter", "input", 0, jump_target="test")
        sut.links.append(Link(sut, sut))
        other = sut.clone()
        sut.links.append(Link(other, other))

        assert_that(sut).is_equal_to(other)


class TestChain:
    def test_init_sets_defaults_values(self):
        sut = Chain("test")

        assert_that(sut).has_name("test")
        assert_that(sut.parent).has_name("detached")
        assert_that(sut).has_render(True)

    def test_chain_has_id(self):
        sut = Chain("test")

        assert_that(sut.id).is_not_none()

    def test_add_rule_appends_rule_to_rules(self, rule_factory: Callable[..., Rule]):
        sut = Chain("test")
        rule = rule_factory("filter", "input", 1)
        second_rule = rule_factory("filter", "input", 2)
        assert_that(sut.add_rule(rule)).is_equal_to(rule)
        assert_that(sut.rules).is_length(1).contains(rule)

        assert_that(sut.add_rule(second_rule)).is_equal_to(second_rule)
        assert_that(sut.rules).is_length(2).contains(second_rule)

    def test_add_rule_when_adding_same_rule_multiple_times_only_adds_once(
        self, rule_factory: Callable[..., Rule]
    ):
        sut = Chain("test")
        rule = rule_factory("filter", "input", 1)
        sut.add_rule(rule)
        assert_that(sut.rules).is_length(1).contains(rule)

        assert_that(sut.add_rule(rule)).is_none()
        assert_that(sut.rules).is_length(1).contains(rule)

    def test_add_rule_when_rule_by_index_exists_does_not_add_rule(
        self, rule_factory: Callable[..., Rule]
    ):
        sut = Chain("test")
        rule = rule_factory("filter", "input", 1)
        sut.add_rule(rule)
        assert_that(val=sut.rules).is_length(1).contains(rule)

        assert_that(sut.add_rule(rule_factory("raw", "input", 1))).is_none()
        assert_that(sut.rules).is_length(1).contains(rule)

    def test_remove_rule_when_rule_exists_in_list_removes_from_list(
        self, rule_factory: Callable[..., Rule]
    ):
        sut = Chain("test")
        rule = rule_factory("filter", "input", 1)
        second_rule = rule_factory("filter", "input", 2)
        sut.add_rule(rule)
        sut.add_rule(second_rule)

        assert_that(sut.remove_rule(rule)).is_equal_to(rule)
        assert_that(sut.rules).is_length(1).contains(second_rule)

        assert_that(sut.remove_rule(second_rule)).is_equal_to(second_rule)
        assert_that(sut.rules).is_length(0)

    def test_remove_rule_when_rule_exists_unsets_parent(self, rule_factory: Callable[..., Rule]):
        sut = Chain("test")
        rule = rule_factory("filter", "input", 1)
        second_rule = rule_factory("filter", "input", 2)
        sut.add_rule(rule)
        sut.add_rule(second_rule)

        removed = sut.remove_rule(rule)
        assert removed
        assert_that(removed.parent).is_not_equal_to(sut)

    def test_remove_rule_when_rules_does_not_exist_returns_none(
        self, rule_factory: Callable[..., Rule]
    ):
        sut = Chain("test")
        rule = rule_factory("filter", "input", 1)

        assert_that(sut.remove_rule(rule)).is_none()

    def test_first_rule_when_no_rules_exist_returns_none(self):
        sut = Chain("test")

        assert_that(sut.first_rule()).is_none()

    def test_first_rule_returns_first_rule_by_index(self, rule_factory: Callable[..., Rule]):
        sut = Chain("test")
        first_rule = rule_factory("filter", "input", 1)
        second_rule = rule_factory("filter", "input", 2)
        sut.add_rule(second_rule)
        sut.add_rule(first_rule)

        assert_that(sut.first_rule()).is_equal_to(first_rule)

    def test_last_rule_when_no_rules_exist_returns_none(self):
        sut = Chain("test")

        assert_that(sut.last_rule()).is_none()

    def test_last_rule_returns_last_rule_by_index(self, rule_factory: Callable[..., Rule]):
        sut = Chain("test")
        first_rule = rule_factory("filter", "input", 1)
        second_rule = rule_factory("filter", "input", 2)
        sut.add_rule(second_rule)
        sut.add_rule(first_rule)

        assert_that(sut.last_rule()).is_equal_to(second_rule)

    def test_next_rule_when_no_rules_exist_returns_none(self, rule_factory: Callable[..., Rule]):
        rule = rule_factory("filter", "input", 0, comment="test rule")
        sut = Chain("test")

        assert_that(sut.next_rule(rule)).is_none()

    def test_next_rule_when_rule_last_in_chain_returns_none(
        self, rule_factory: Callable[..., Rule]
    ):
        rule = rule_factory("filter", "input", 0, comment="test rule")
        sut = Chain("test")
        sut.add_rule(rule)

        assert_that(sut.next_rule(rule)).is_none()

    def test_next_rule_returns_next_rule_by_index(self, rule_factory: Callable[..., Rule]):
        rule: Rule = rule_factory("filter", "input", 0, comment="test rule")
        next_rule: Rule = rule_factory("filter", "input", 1, comment="test rule")
        sut = Chain("test")
        sut.add_rule(rule)
        sut.add_rule(next_rule)

        assert_that(sut.next_rule(rule)).is_equal_to(next_rule)

    def test_detach_removes_from_parent(self):
        sut = Chain("test")
        parent = sut.parent
        sut.detach()

        assert_that(sut.parent).is_not_none().is_not_equal_to(parent)
        assert_that(sut).is_not_in(parent.chains)

    def test_clone_returns_equivalent_chain(self):
        sut = Chain("test")

        assert_that(sut).is_equal_to(sut.clone())

    def test_clone_clones_rules(self, rule_factory: Callable[..., Rule]):
        sut = Chain("test")
        rule: Rule = rule_factory("filter", "input", 0, comment="test rule")
        second_rule: Rule = rule_factory("filter", "input", 1, comment="test rule")
        sut.add_rule(rule)
        sut.add_rule(second_rule)
        clone = sut.clone()

        assert_that(clone.rules).is_length(2)
        assert_rules_are_equivalent(sut.first_rule(), clone.first_rule())
        assert_rules_are_equivalent(sut.last_rule(), clone.last_rule())

    def test_eq_when_other_is_self_return_true(self):
        sut = Chain("test")

        assert_that(sut).is_equal_to(sut)

    def test_eq_when_other_is_equal_return_true(self):
        assert_that(Chain("test")).is_equal_to(Chain("test"))

    def test_eq_when_other_is_different_return_false(self):
        assert_that(Chain("test")).is_not_equal_to(Chain("Dummy"))

    def test_eq_when_other_has_different_section_return_true(self):
        sut = Chain("test")
        parent = Section("new")
        other = Chain("test")
        parent.add_chain(other)

        assert_that(sut).is_equal_to(other)

    def test_eq_when_links_differ_return_true(self):
        sut = Chain("test")
        sut.links.append(Link(sut, sut))
        other = sut.clone()
        assert_that(sut).is_equal_to(other)

    def test_eq_when_rules_differ_return_false(self, rule_factory):
        sut = Chain("test")
        sut.add_rule(rule_factory("filter", "test", 0))
        other = sut.clone()
        other.add_rule(rule_factory("filter", "test", 1))

        assert_that(sut).is_not_equal_to(other)


class TestSection:
    def test_inits_to_defaults_values(self):
        sut = Section("test")

        assert_that(sut.name).is_equal_to("test")
        assert_that(sut.render).is_true()

    def test_section_has_id(self):
        sut = Section("test")

        assert_that(sut.id).is_not_none()

    def test_has_chain_works(self):
        sut = Section("sut")
        sut.add_chain("test")

        assert_that(sut.has_chain("test")).is_true()
        assert_that(sut.has_chain("other")).is_false()

    def test_add_chain(self):
        sut = Section("test")
        chain = Chain("test")
        sut.add_chain(chain)

        assert_that(sut.chains).is_length(1).contains(chain)

        chain = Chain("test2")
        sut.add_chain(chain)
        assert_that(sut.chains).is_length(2).contains(chain)

    def test_add_chain_multiple_times_raises_chainalreadyexistserror(self):
        sut = Section("test")
        chain = Chain("test")
        sut.add_chain(chain)

        assert_that(sut.chains).is_length(1).contains(chain)

        assert_that(sut.add_chain).raises(ChainAlreadyExistsError).when_called_with(chain)
        assert_that(sut.chains).is_length(1).contains(chain)

    def test_add_chain_with_same_name_raises_chainalreadyexistserror(self):
        sut = Section("test")
        chain = Chain("test")
        sut.add_chain(chain)

        assert_that(sut.chains).is_length(1).contains(chain)
        assert_that(sut.add_chain).raises(ChainAlreadyExistsError).when_called_with("test")
        assert_that(sut.chains).is_length(1).contains(chain)

    def test_add_or_get_chain_adds_chain_when_not_found(self):
        sut = Section("test")

        assert_that(sut.get_or_add_chain("test")).is_not_none().has_name("test")
        assert_that(sut.has_chain("test"))

    def test_add_or_get_chain_gets_chain_when_found(self):
        sut = Section("test")
        chain = Chain("test")
        sut.add_chain(chain)

        assert_that(sut.get_or_add_chain("test")).is_not_none().is_same_as(chain)

    def test_chain_returns_correct_chain(self):
        sut = Section("test")
        chain = Chain("test")
        sut.add_chain(chain)

        assert_that(sut.chain("test")).is_equal_to(chain)

    def test_chain_returns_none_if_not_found(self):
        sut = Section("test")
        sut.add_chain("test")

        assert_that(sut.chain("nonexistant")).is_none()

    def test_remove_chain(self):
        sut = Section("test")
        chain = Chain("input")
        second_chain = Chain("output")
        sut.add_chain(chain)
        sut.add_chain(second_chain)

        assert_that(sut.remove_chain(chain)).is_equal_to(chain)
        assert_that(sut.chains).is_length(1).contains(second_chain)

        assert_that(sut.remove_chain(second_chain)).is_equal_to(second_chain)
        assert_that(sut.chains).is_length(0)

    def test_remove_chain_resets_parent(self):
        sut = Section("test")
        chain = Chain("input")
        sut.add_chain(chain)

        removed = sut.remove_chain(chain=chain)
        assert removed
        assert_that(removed.parent).is_not_equal_to(sut)

    def test_remove_chain_with_non_existing_chain_returns_none(
        self, rule_factory: Callable[..., Rule]
    ):
        sut = Section("test")
        chain = Chain("test")

        assert_that(sut.remove_chain(chain)).is_none()

    def test_detach(self):
        sut = Section("test")
        parent = sut.parent
        sut.detach()

        assert_that(sut.parent).is_not_none().is_not_equal_to(parent)
        assert_that(parent.sections).does_not_contain(sut)

    def test_clone_returns_equivalent_chain(self):
        sut = Section("test")
        clone = sut.clone()

        assert_that(sut.render).is_equal_to(clone.render)

    def test_clone_clones_chains(self):
        sut = Section("test")
        sut.add_chain("first")
        sut.add_chain("second")
        clone = sut.clone()

        assert_that(clone.chains).is_length(2)
        assert_chains_are_equivalent(clone.chains[0], sut.chains[0])
        assert_chains_are_equivalent(clone.chains[1], sut.chains[1])

    def test_next_section_returns_none_if_no_next_section(self):
        sut = Section("test")

        assert_that(sut.next_section()).is_none()

    @pytest.mark.parametrize("execution_number", range(10))
    def test_next_section_returns_next_section(self, execution_number: int):
        root = RootNode()
        section_legend = Section("legend")
        section_raw = Section("raw")
        section_mangle = Section("mangle")
        section_nat = Section("nat")
        section_filter = Section("filter")
        section_other = Section("other")

        l = [
            section_legend,
            section_raw,
            section_mangle,
            section_nat,
            section_filter,
            section_other,
        ]
        random.shuffle(l)
        for section in l:
            root.add_section(section)

        assert_that(section_legend.next_section()).is_not_none().is_equal_to(section_raw)
        assert_that(section_raw.next_section()).is_not_none().is_equal_to(section_mangle)
        assert_that(section_mangle.next_section()).is_not_none().is_equal_to(section_nat)
        assert_that(section_nat.next_section()).is_not_none().is_equal_to(section_filter)
        assert_that(section_filter.next_section()).is_not_none().is_equal_to(section_other)

    def test_eq_when_other_is_self_return_true(self):
        sut = Section("test")

        assert_that(sut).is_equal_to(sut)

    def test_eq_when_other_is_equal_return_true(self):
        assert_that(Section("test")).is_equal_to(Section("test"))

    def test_eq_when_other_different_root_return_true(self):
        sut = Section("test")
        other = Section("test")
        RootNode().add_section(other)

        assert_that(sut).is_equal_to(other)

    def test_eq_when_other_is_different_return_false(self):
        assert_that(Section("test")).is_not_equal_to(Section("Dummy"))

    def test_eq_when_links_differ_return_true(self):
        sut = Section("test")
        sut.links.append(Link(sut, sut))
        other = sut.clone()
        assert_that(sut).is_equal_to(other)

    def test_eq_when_chains_differ_return_false(self):
        sut = Section("test")
        sut.add_chain(Chain("first"))
        other = sut.clone()
        other.add_chain(Chain("second"))
        assert_that(sut).is_not_equal_to(other)


class TestRootNode:
    def test_inits_to_defaults_values(self):
        sut = RootNode()

        assert_that(sut.sections).is_empty()

    def test_add_or_get_section_adds_section_when_not_found(self):
        sut = RootNode()

        assert_that(sut.get_or_add_section("test")).is_not_none().has_name("test")
        assert_that(sut.has_section("test"))

    def test_add_or_get_section_gets_section_when_found(self):
        sut = RootNode()

        section = sut.add_section("test")

        assert_that(sut.get_or_add_section("test")).is_not_none().is_same_as(section)

    def test_has_section_works(self):
        sut = RootNode()
        sut.add_section("test")

        assert_that(sut.has_section("test")).is_true()
        assert_that(sut.has_section("other")).is_false()

    def test_add_section_works(self):
        sut = RootNode()
        section = Section("test")

        sut.add_section(section)

        assert_that(sut.sections).is_length(1).contains(section)
        assert_that(section.parent).is_equal_to(sut)

    def test_add_section_multiple_times_raises_sectionalreadyexists_error(self):
        sut = RootNode()
        section = Section("test")

        sut.add_section(section)

        assert_that(sut.add_section).raises(SectionAlreadyExistsError).when_called_with(section)
        assert_that(sut.sections).is_length(1).contains(section)

    def test_add_section_with_same_name_raises_sectionalreadyexistserror(self):
        sut = RootNode()
        section = Section("test")
        sut.add_section(section)

        assert_that(sut.sections).is_length(1).contains(section)

        assert_that(sut.add_section).raises(SectionAlreadyExistsError).when_called_with("test")
        assert_that(sut.sections).is_length(1).contains(section)

    def test_remove_section_returns_none_if_section_not_found(self):
        assert_that(RootNode().remove_section(Section("test"))).is_none()

    def test_next_section_returns_none_if_section_not_in_list(self):
        assert_that(RootNode().next_section(Section("test"))).is_none()

    def test_next_section_returns_none_if_no_next_section(self):
        sut = RootNode()
        section = Section("test")
        sut.add_section(section)

        assert_that(sut.next_section(section)).is_none()

    @pytest.mark.parametrize("execution_number", range(10))
    def test_next_section_returns_next_section_by_name(self, execution_number: int):
        sut = RootNode()
        section_legend = Section("legend")
        section_raw = Section("raw")
        section_mangle = Section("mangle")
        section_nat = Section("nat")
        section_filter = Section("filter")
        section_other = Section("other")

        l = [
            section_legend,
            section_raw,
            section_mangle,
            section_nat,
            section_filter,
            section_other,
        ]
        random.shuffle(l)
        for section in l:
            sut.add_section(section)

        assert_that(sut.next_section(section_legend)).is_not_none().is_equal_to(section_raw)
        assert_that(sut.next_section(section_raw)).is_not_none().is_equal_to(section_mangle)
        assert_that(sut.next_section(section_mangle)).is_not_none().is_equal_to(section_nat)
        assert_that(sut.next_section(section_nat)).is_not_none().is_equal_to(section_filter)
        assert_that(sut.next_section(section_filter)).is_not_none().is_equal_to(section_other)

    def test_first_section_returns_none_if_no_sections(self):
        assert_that(RootNode().first_section()).is_none()

    def test_first_section_returns_the_first_available_section(self):
        sut = RootNode()
        section_legend = Section("legend")
        section_mangle = Section("mangle")
        section_nat = Section("nat")
        section_filter = Section("filter")
        section_other = Section("other")

        sut.add_section(section_other)
        assert_that(sut.first_section()).is_equal_to(section_other)

        sut.add_section(section_filter)
        assert_that(sut.first_section()).is_equal_to(section_filter)

        sut.add_section(section_mangle)
        assert_that(sut.first_section()).is_equal_to(section_mangle)
        sut.add_section(section_nat)
        assert_that(sut.first_section()).is_equal_to(section_mangle)
        sut.add_section(section_legend)
        assert_that(sut.first_section()).is_equal_to(section_legend)

    def test_section_returns_none_if_not_found(self):
        sut = RootNode()
        sut.add_section("filter")

        assert_that(sut.section("test")).is_none()

    def test_section_returns_correct_section(self):
        sut = RootNode()
        section = Section("filter")
        sut.add_section(section)

        assert_that(sut.section("filter")).is_not_none().is_equal_to(section)

    def test_next_rule_index_returns_the_next_available_id(self):
        sut = RootNode()
        sut.add_section("test").add_chain("test").add_rule(_rule_factory("test", "test", 50))
        sut.add_section("test2").add_chain("test2").add_rule(_rule_factory("test2", "test", 10))

        assert_that(sut.next_rule_index()).is_equal_to(51)

    def test_eq_when_other_is_self_return_true(self):
        sut = RootNode()

        assert_that(sut).is_equal_to(sut)

    def test_eq_when_other_is_equal_return_true(self):
        assert_that(RootNode()).is_equal_to(RootNode())

    def test_eq_when_sections_differ_return_false(self):
        sut = RootNode()
        sut.add_section("first")
        other = sut.clone()
        other.add_section("second")
        assert_that(sut).is_not_equal_to(other)

    def test_clone_returns_root_node_with_sections(self):
        sut = RootNode()
        sut.add_section("test")
        other = sut.clone()

        assert_that(sut.sections).is_equal_to(other.sections)

    def test_clone_returns_different_object_for_sections(self):
        sut = RootNode()
        sut.add_section("test")
        other = sut.clone()

        assert_that(id(sut.sections)).is_not_equal_to(id(other.sections))
        assert_that(id(sut.sections[0])).is_not_equal_to(id(other.sections[0]))


class TestPacketFlowElement:
    def test_last_node_returns_last_node_in_sequence(self):
        first = PacketFlowElement("1", "1")
        second = PacketFlowElement("2", "2")
        third = PacketFlowElement("3", "3")
        first.next = second
        second.next = third

        assert_that(first.last_node()).is_equal_to(third)
        assert_that(second.last_node()).is_equal_to(third)
        assert_that(third.last_node()).is_equal_to(third)


class TestPacketFlowIterator:
    def test_iterator_follows_order(self):
        first = PacketFlowElement("1", "1")
        second = PacketFlowElement("2", "2")
        third = PacketFlowElement("3", "3")
        first.next = second
        second.next = third

        iter = PacketFlowIterator(first)

        assert_that(next(iter)).is_equal_to(first)
        assert_that(next(iter)).is_equal_to(second)
        assert_that(next(iter)).is_equal_to(third)


class TestGraphSelector:
    def test_graph_selector_is_default(self):
        selector = GraphSelector("")
        assert_that(selector.default).is_true()

        selector = GraphSelector("", mod="+")
        assert_that(selector.default).is_true()

        selector = GraphSelector("filter")
        assert_that(selector.default).is_false()

    def test_graph_selector_is_splat(self):
        selector = GraphSelector("", mod="+")
        assert_that(selector.splat).is_false()

        selector = GraphSelector("", mod="*+")
        assert_that(selector.splat).is_true()

    def test_graph_selector_is_normal_render(self):
        selector = GraphSelector("")
        assert_that(selector.show).is_true()

        selector = GraphSelector("", mod="+")
        assert_that(selector.show).is_true()

        selector = GraphSelector("", mod="*")
        assert_that(selector.show).is_false()

    def test_graph_selector_is_remove(self):
        # Test when mod does not contain "-"
        selector = GraphSelector("", mod="+")
        assert_that(selector.remove).is_false()

        # Test when mod contains "-"
        selector = GraphSelector("", mod="-+")
        assert_that(selector.remove).is_true()

    def test_graph_selector_is_collapse(self):
        # Test when mod does not contain "%"
        selector = GraphSelector("", mod="+")
        assert_that(selector.collapse).is_false()

        # Test when mod contains "%"
        selector = GraphSelector("", mod="%+")
        assert_that(selector.collapse).is_true()

    def test_graph_selector_is_render(self):
        # Test when mod contains "-"
        selector = GraphSelector("", mod="-+")
        assert_that(selector.render).is_false()

        # Test when mod does not contain "-"
        selector = GraphSelector("", mod="+")
        assert_that(selector.render).is_true()
