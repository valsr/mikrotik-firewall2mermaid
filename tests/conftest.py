from typing import Any, Callable
import pytest

from firewall2mermaid.common import FIREWALL_LINE_START, create_adhoc_rule

from firewall2mermaid.model import ParserRule, RootNode, Rule
from firewall2mermaid.parser import parse_rule
from firewall2mermaid.tree import TreeMaker


def _parser_rule_line_factory(section: str, chain: str, **kwargs: str) -> str:
    attrs_line = ""
    if kwargs:
        for key in kwargs.keys():
            if " " in kwargs[key] and not str(kwargs[key]).startswith('"'):
                kwargs[key] = f'"{kwargs[key]}"'
        attrs_line = " ".join([f"{key}={kwargs[key]}" for key in kwargs])
    return f"{FIREWALL_LINE_START} {section} add chain={chain} {attrs_line}"


def _parser_rule_factory(section: str, chain: str, **kwargs: str) -> ParserRule:
    return parse_rule(_parser_rule_line_factory(section, chain, **kwargs))


def _rule_factory(section: str, chain: str, index: int, **kwargs: str) -> Rule:
    return create_adhoc_rule(section, chain, index, **kwargs)


@pytest.fixture
def parser_rule_line_factory() -> Callable[[str, str], str]:
    return _parser_rule_line_factory


@pytest.fixture
def parser_rule_factory() -> Callable[[str, str], ParserRule]:
    return _parser_rule_factory


@pytest.fixture
def rule_factory() -> Callable[[str, str, int], Rule]:
    return _rule_factory


@pytest.fixture
def single_filter_input_rule(parser_rule_factory: Callable[..., ParserRule]) -> ParserRule:
    return parser_rule_factory("filter", "input")


@pytest.fixture
def default_rule_list(parser_rule_factory: Callable[..., ParserRule]) -> list[ParserRule]:
    return [
        # prerouting
        parser_rule_factory(
            "raw", "prerouting", action="fasttrack-connection", comment="fasttrack"
        ),
        parser_rule_factory(
            "mangle", "prerouting", action="fasttrack-connection", comment="fasttrack"
        ),
        parser_rule_factory("nat", "dstnat", action="fasttrack-connection", comment="fasttrack"),
        # input
        parser_rule_factory(
            "mangle", "input", action="fasttrack-connection", disabled="no", comment="fasttrack"
        ),
        parser_rule_factory(
            "mangle", "input", action="accept", comment="disabled rule", disabled="yes"
        ),
        parser_rule_factory("filter", "input", action="fasttrack-connection", comment="fasttrack"),
        parser_rule_factory(
            "filter", "input", action="log", comment="log rule", log_prefix="filter"
        ),
        parser_rule_factory(
            "filter",
            "input",
            action="jump",
            comment="jump",
            jump_target="terminal-chain",
            src_address="192.168.0.0",
        ),
        parser_rule_factory("filter", "input", action="drop", comment="drop all"),
        # forward
        parser_rule_factory(
            "mangle", "forward", action="fasttrack-connection", comment="fasttrack"
        ),
        parser_rule_factory(
            "mangle", "forward", action="accept", comment="disabled rule", disabled="yes"
        ),
        parser_rule_factory(
            "filter", "forward", action="fasttrack-connection", comment="fasttrack"
        ),
        parser_rule_factory(
            "filter", "forward", action="log", comment="log rule", log_prefix="filter"
        ),
        parser_rule_factory(
            "filter", "forward", action="jump", comment="jump", jump_target="terminal-chain"
        ),
        parser_rule_factory(
            "filter", "forward", action="jump", comment="jump", jump_target="jump-chain"
        ),
        parser_rule_factory("filter", "forward", action="drop", comment="drop all"),
        # output
        parser_rule_factory("raw", "output", action="fasttrack-connection", comment="fasttrack"),
        parser_rule_factory("mangle", "output", action="fasttrack-connection", comment="fasttrack"),
        parser_rule_factory("filter", "output", action="fasttrack-connection", comment="fasttrack"),
        # postrouting
        parser_rule_factory(
            "mangle", "postrouting", action="fasttrack-connection", comment="fasttrack"
        ),
        parser_rule_factory("nat", "srcnat", action="fasttrack-connection", comment="fasttrack"),
        # terminal chain
        parser_rule_factory(
            "filter",
            "terminal-chain",
            action="drop",
            src_address="192.168.0.0",
            comment="drop if src matches",
        ),
        # jump chains
        parser_rule_factory("filter", "jump-chain", action="return", src_address="10.0.0.0"),
        parser_rule_factory(
            "filter",
            "jump-chain",
            action="accept",
            comment="jump target",
            src_address="192.168.0.0",
        ),
        parser_rule_factory("raw", "jump-chain", action="accept", comment="jump target"),
        parser_rule_factory("filter", "terminal-chain", action="drop", comment="drop all"),
        # disabled chain
        parser_rule_factory("filter", "disabled-chain", action="drop", disabled="yes"),
        # log chain
        parser_rule_factory("filter", "log-chain", action="log"),
        # rule order chain
        parser_rule_factory("filter", "rule-order", comment="1"),
        parser_rule_factory("filter", "rule-order", comment="2"),
        parser_rule_factory("filter", "rule-order", comment="3"),
        parser_rule_factory("filter", "rule-order", comment="4"),
    ]


@pytest.fixture
def add_flow_rule_list(parser_rule_factory: Callable[..., ParserRule]) -> list[ParserRule]:
    return [
        parser_rule_factory("filter", "input", action="fasttrack-connection", comment="fasttrack"),
        parser_rule_factory("nat", "srcnat", action="masquerade", comment="masquerade"),
        parser_rule_factory("nat", "dstnat", action="dst-nat", comment="dstnat"),
        parser_rule_factory("raw", "prerouting", action="accept", comment="raw"),
    ]


@pytest.fixture
def rule_list_ordered_tests() -> list[ParserRule]:
    return [
        parse_rule("/ip firewall filter add chain=input comment=0"),
        parse_rule("/ip firewall filter add chain=input comment=1"),
        parse_rule("/ip firewall filter add chain=output comment=2"),
        parse_rule("/ip firewall raw add chain=input comment=3"),
        parse_rule("/ip firewall raw add chain=input comment=4"),
        parse_rule("/ip firewall raw add chain=output comment=5"),
    ]


@pytest.fixture
def default_tree_root(default_rule_list: list[ParserRule]) -> RootNode:
    return TreeMaker(default_rule_list, []).make_tree()


known_rule_attributes = set(
    [
        "action",
        "address-list-timeout",
        "chain",
        "comment",
        "connection-bytes",
        "connection-limit",
        "connection-mark",
        "connectio-rate",
        "connection-state",
        "connection-type",
        "content",
        "dscp",
        "dst-address",
        "dst-address-list",
        "dst-address-type",
        "dst-limit",
        "dst-port",
        "fragment",
        "hotspot",
        "icmp-options",
        "in-bridge-port",
        "in-bridge-port-list",
        "in-interface",
        "in-interface-list",
        "ingress-priority",
        "ipsec-policy",
        "ipv4-options",
        "jump-target",
        "layer7-protocol",
        "log-prefix",
        "nth",
        "new-connection-mark",
        "new-dscp",
        "new-mss",
        "new-packet-mark",
        "new-priority",
        "new-routing-mark",
        "new-ttl",
        "out-bridge-port",
        "out-bridge-port-list",
        "out-interface",
        "out-interface-list",
        "packet-mark",
        "packet-size",
        "per-connection-classifier",
        "port",
        "protocol",
        "psd",
        "random",
        "reject-with",
        "routing-table",
        "routing-mark",
        "src-address",
        "src-address-list",
        "src-address-type",
        "src-port",
        "src-mac-address",
        "tcp-flags",
        "tcp-mss",
        "time",
        "tls-host",
        "ttl",
    ]
)

known_rule_attributes_for_generation = known_rule_attributes - set(["chain"])

terminal_actions = set(["drop", "accept", "reject", "tarpit"])
rule_matching_attributes = set(known_rule_attributes).difference(
    {"action", "comment", "chain", "log-prefix", "log", "jump-target"}
)


# asserter tests
@pytest.fixture
def sample_graph_text():
    return """
%%{init: {'flowchart': {'htmlLabels': false}, 'theme': 'dark'}}%%
flowchart TB

classDef fasttrack-connection fill:#686d6d
classDef log fill:#686d6d
classDef drop fill:#8c2c61

subgraph root
	direction TB
	subgraph legend
		direction TB
		subgraph legend-legend
			direction TB
			rule7[*fasttrack-connection]:::fasttrack-connection
			rule8[*log]:::log
			rule9[*drop]:::drop
		end
	end
	subgraph filter [filter]
		direction TB
		subgraph filter-input [input]
			direction TB
			rule1[*fasttrack]:::fasttrack-connection
			rule2[*log rule]:::log
			rule3[*drop all]:::drop
		end
		subgraph filter-forward [forward]
			direction TB
			rule4[*fasttrack]:::fasttrack-connection
			rule5[*log rule]:::log
			rule6[*drop all]:::drop
		end
	end
end

%% Relationships
filter:input:1 --> filter:input:2
filter:input:2 --> filter:input:3
filter:forward:4 --> filter:forward:5
filter:forward:5 --> filter:forward:6
"""
