import re
import pytest
from firewall2mermaid.common import (
    GRAPH_SELECTOR_PATTERN,
    create_adhoc_rule,
    get_effective_selector,
    GraphSelector,
)
from assertpy import assert_that


class TestCreateAdhocRule:
    def test_comment_with_spaces_is_not_quoted(self):
        rule = create_adhoc_rule(
            "dummy",
            "test",
            1,
            action="accept",
            comment="rule added for packet flow",
        )
        assert_that(rule.comment).is_equal_to("rule added for packet flow")


class TestGetEffectiveSelector:
    def test_always_return_fully_qualified_match(self):
        selectors = [
            GraphSelector("section:chain:rule"),
            GraphSelector("section:chain"),
            GraphSelector("section"),
        ]
        result = get_effective_selector(selectors, "section:chain:rule")
        assert_that(result).is_equal_to(GraphSelector("section:chain:rule"))

        result = get_effective_selector(selectors, "section:chain")
        assert_that(result).is_equal_to(GraphSelector("section:chain"))

    @pytest.mark.parametrize(
        "id, expected",
        [
            ("section:chain:rule", GraphSelector("section:chain:")),
            ("section:other:rule", GraphSelector("section::rule")),
            ("section:other:other", GraphSelector("section::")),
            ("other:other:other", GraphSelector("::")),
        ],
    )
    def test_pick_same_level_selector_based_on_score(self, id, expected):
        selectors = [
            GraphSelector("section:chain:"),
            GraphSelector("section::rule"),
            GraphSelector("section::"),
            GraphSelector("::rule"),
            GraphSelector("::"),
        ]
        result = get_effective_selector(selectors, id)
        assert_that(result).is_equal_to(expected)

    def test_no_match_found_when_selectors_given_returns_default_show_selector(self):
        selectors = [
            GraphSelector("section:chain:rule"),
            GraphSelector("section:chain"),
            GraphSelector("section"),
        ]
        result = get_effective_selector(selectors, "other")
        assert_that(result).is_equal_to(GraphSelector("", mod=""))

    def test_no_match_found_when_no_selectors_given_returns_default_include_selector(self):
        result = get_effective_selector([], "other")
        assert_that(result).is_equal_to(GraphSelector("", mod=""))


class TestGraphSelectorPattern:
    @pytest.mark.parametrize(
        "selector",
        [
            "section:chain:rule",
            "section:chain",
            "section",
            "*section",
            "-section",
            "+section",
            "%%section",
            "%%*section",
            "section-with-dash",
            ":",
            "::",
        ],
    )
    def test_pattern_matching(self, selector):
        assert_that(re.match(GRAPH_SELECTOR_PATTERN, selector)).is_not_none()
