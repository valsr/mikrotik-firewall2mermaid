from typing import Callable
from firewall2mermaid.parser import parse_rule
from assertpy import assert_that


class TestParser:
    def test_when_parsing_rule_line_if_no_action_is_present_default_to_accept(
        self, parser_rule_line_factory: Callable[..., str]
    ):
        line = parser_rule_line_factory("section", "chain")
        rule = parse_rule(line)
        assert_that(rule.attributes).contains_entry({"action": "accept"})

    def test_when_parsing_rule_line_action_is_present(
        self, parser_rule_line_factory: Callable[..., str]
    ):
        line = parser_rule_line_factory("section", "chain", action="action")
        rule = parse_rule(line)
        assert_that(rule.attributes).contains_entry({"action": "action"})

    def test_when_parsing_rule_line_section_is_present(
        self, parser_rule_line_factory: Callable[..., str]
    ):
        line = parser_rule_line_factory("section", "chain")
        rule = parse_rule(line)
        assert_that(rule.section).is_equal_to("section")

    def test_when_parsing_rule_line_chain_is_present(
        self, parser_rule_line_factory: Callable[..., str]
    ):
        line = parser_rule_line_factory("section", "chain")
        rule = parse_rule(line)
        assert_that(rule.chain).is_equal_to("chain")
        assert_that(rule.attributes).contains_entry({"chain": "chain"})

    def test_when_parsing_rule_with_extra_attribute_attribute_is_present(
        self, parser_rule_line_factory: Callable[..., str]
    ):
        line = parser_rule_line_factory("section", "chain", dummy="dummy")
        rule = parse_rule(line)
        assert_that(rule.attributes).contains_entry({"dummy": "dummy"})
