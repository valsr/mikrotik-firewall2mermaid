from __future__ import annotations
import re

from tests.assertions import AsserterContext, AugmentBase


def extract_label(line: str, open: str = "[", close: str = "]") -> str:
    if open == close:
        return line.split(open)[1]
    return line.split(open)[1].split(close)[0]


class WithLabel(AugmentBase):
    """With asserter for simple label matching."""

    def __init__(self, pattern: str, open="[", close="]", reg_ex=False):
        self.label: str = pattern
        self.open = open
        self.close = close
        self.reg_ex = reg_ex
        self.compiled_pattern = re.compile(pattern) if reg_ex else None

    def match(self, context: AsserterContext) -> bool:
        line = context.current_line() or ""
        if self.open not in line or self.close not in line:
            return False

        label = extract_label(line, self.open, self.close)

        if not self.compiled_pattern:
            return self.label in label

        return self.compiled_pattern.search(label) is not None

    def message(self) -> str:
        if self.reg_ex:
            return f"label matching '{self.label}'"
        return f"label '{self.label}'"


class WithStyle(AugmentBase):
    """With asserter for style matching"""

    def __init__(self, style: str):
        self.style: str = style

    def match(self, context: AsserterContext) -> bool:
        line = context.current_line() or ""
        if ":::" not in line:
            return False

        style = line.split(":::")[1]
        return self.style == style

    def message(self) -> str:
        return f"style '{self.style}'"


class WithoutStyle(AugmentBase):
    """With asserter for negative style matching."""

    def __init__(self, style: str | None = None):
        self.style: str | None = style

    def match(self, context: AsserterContext) -> bool:
        line = context.current_line() or ""

        if not self.style:
            return ":::" not in line

        # Without specific style
        style = line.split(":::")[1]
        return self.style != style

    def message(self) -> str:
        return f"no style matching '{self.style}'" if self.style else "no styles"


class WithId(AugmentBase):
    """With asserter for matching node id"""

    def __init__(self, id: str, use_regex=False):
        self.id: str = id
        self.use_regex = use_regex
        if use_regex:
            self.pattern = re.compile(id)

    def match(self, context: AsserterContext) -> bool:
        line: str = context.current_line() or ""
        if "[" in line:
            line = line.split("[")[0].rstrip()

        line = line.split(" ")[-1].strip()

        if self.use_regex:
            return self.pattern.match(line) is not None
        return line == self.id

    def message(self) -> str:
        return f"id '{self.id}'"


class WithDirection(AugmentBase):
    """With asserter for matching subgraph direction"""

    def __init__(self, direction: str):
        self.direction: str = direction

    def match(self, context: AsserterContext) -> bool:
        while context.has_line():
            line: str = context.current_line() or ""
            if line.strip() == f"direction {self.direction}":
                return True
            context.next_line()

        return False

    def message(self) -> str:
        return f"direction '{self.direction}'"


class AtDepth(AugmentBase):
    """With asserter for matching exact node depth"""

    def __init__(self, level: int):
        self.level: int = level

    def match(self, context) -> bool:
        return context.depth == self.level

    def message(self) -> str:
        return f"at depth of {self.level}"


class AtMinDepth(AugmentBase):
    """With asserter for matching exact node depth"""

    def __init__(self, level: int):
        self.level: int = level

    def match(self, context) -> bool:
        return context.depth >= self.level

    def message(self) -> str:
        return f"at minimum depth of {self.level}"


class AtMaxDepth(AugmentBase):
    """With asserter for matching exact node depth"""

    def __init__(self, level: int):
        self.level: int = level

    def match(self, context) -> bool:
        return context.depth <= self.level

    def message(self) -> str:
        return f"at maximum depth of {self.level}"


class WithLinkStyle(AugmentBase):
    """With asserter for matching node in link start"""

    def __init__(self, style: str):
        self.style: str = style

    def match(self, context: AsserterContext) -> bool:
        from tests.assertions.asserters import LINK_REGEX

        m = re.match(LINK_REGEX, context.current_line() or "")
        return self.style in m.group("link") if m else False

    def message(self) -> str:
        return f"link style '{self.style}'"


class WithLinkStart(AugmentBase):
    """With asserter for matching node in link start"""

    def __init__(self, pattern: str, use_regex=False):
        self.pattern: str = pattern
        self.use_regex = use_regex
        if self.use_regex:
            self.compiled_pattern = re.compile(self.pattern)

    def match(self, context: AsserterContext) -> bool:
        from tests.assertions.asserters import LINK_REGEX

        m = re.match(LINK_REGEX, context.current_line() or "")
        if not m:
            return False

        start = m.group("start")
        return (
            self.compiled_pattern.match(start) is not None
            if self.use_regex
            else self.pattern == start
        )

    def message(self) -> str:
        return f"from matching '{self.pattern}'" if self.use_regex else f"from '{self.pattern}'"


class WithLinkEnd(AugmentBase):
    """With asserter for matching node in link start"""

    def __init__(self, pattern: str, use_regex=False):
        self.pattern: str = pattern
        self.use_regex: bool = use_regex
        if self.use_regex:
            self.compiled_pattern = re.compile(self.pattern)

    def match(self, context: AsserterContext) -> bool:
        from tests.assertions.asserters import LINK_REGEX

        m = re.match(LINK_REGEX, context.current_line() or "")
        if not m:
            return False

        end = m.group("end")
        return (
            self.compiled_pattern.match(end) is not None if self.use_regex else self.pattern == end
        )

    def message(self) -> str:
        return f"to matching '{self.pattern}'" if self.use_regex else f"to '{self.pattern}'"


class IsCollapsed(AugmentBase):
    """Assert graph node is collapsed"""

    def match(self, context: AsserterContext) -> bool:
        # context should be two lines subgraph X/end
        return len(context) == 2

    def message(self) -> str:
        return f"that is collapsed"
