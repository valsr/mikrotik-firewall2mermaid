import unittest
from unittest.mock import MagicMock, patch
from assertpy import assert_that
from tests.assertions import (
    AsserterBase,
    AsserterContext,
    AssertionFailure,
    AugmentBase,
)
from tests.assertions.asserters import RootAsserter


class DummyAsserter(AsserterBase):
    def __init__(self, parent: AsserterBase):
        super().__init__(parent)

    def _next_match(self, context: AsserterContext) -> bool:
        return True

    def extract_context(self, context: AsserterContext) -> AsserterContext:
        return context

    def message(self):
        return "Dummy message"


class DummyAugment(AugmentBase):
    def match(self, context: AsserterContext):
        return True

    def message(self):
        return "Dummy augment message"


class TestAsserterContext:
    def test_eq_returns_true_for_equal_objects(self):
        # Arrange
        context1 = AsserterContext(["line 1", "line 2", "line 3"])
        context1.index = 1
        context1.depth = 2
        context2 = AsserterContext(["line 1", "line 2", "line 3"])
        context2.index = 1
        context2.depth = 2

        # Assert
        assert_that(context1).is_equal_to(context2)

    def test_eq_returns_false_for_unequal_objects(self):
        # Arrange
        context1 = AsserterContext(["line 1", "line 2", "line 3"])
        context1.index = 1
        context1.depth = 2
        context2 = AsserterContext(["line 1", "line 2", "line 3"])
        context2.index = 0
        context2.depth = 2

        # Assert
        assert_that(context1).is_not_equal_to(context2)

    def test_eq_returns_false_for_objects_of_different_types(self):
        # Arrange
        context1 = AsserterContext(["line 1", "line 2", "line 3"])
        context1.index = 1
        context1.depth = 2
        other = "not a context object"

        # Assert
        assert_that(context1).is_not_equal_to(other)

    def test_current_line_returns_current_line(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 1

        # Assert
        assert_that(context.current_line()).is_equal_to("line 2")

    def test_current_line_returns_none_when_buffer_end_is_reached(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 3

        # Assert
        assert_that(context.current_line()).is_none()

    def test_next_line_moves_to_next_line(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 1

        # Assert
        assert_that(context.next_line()).is_equal_to("line 3")
        assert_that(context.index).is_equal_to(2)

    def test_next_line_returns_none_at_buffer_end(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2"])
        context.index = 1

        # Assert
        assert_that(context.next_line()).is_none()
        assert_that(context.index).is_equal_to(2)

    def test_peek_next_line_returns_next_line(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 1

        # Assert
        assert_that(context.peek_next_line()).is_equal_to("line 3")
        assert_that(context.index).is_equal_to(1)

    def test_peek_next_line_returns_none_at_buffer_end(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 2

        # Assert
        assert_that(context.peek_next_line()).is_none()

    def test_has_line_returns_true_when_index_less_than_buffer_length(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 2

        # Assert
        assert_that(context.has_line()).is_true()

    def test_has_line_returns_false_when_index_equal_to_buffer_length(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 3

        # Assert
        assert_that(context.has_line()).is_false()

    def test_has_line_returns_false_when_index_greater_than_buffer_length(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 4

        # Assert
        assert_that(context.has_line()).is_false()

    def test_has_next_line_returns_true_when_buffer_has_next_line(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 1

        # Assert
        assert_that(context.has_next_line()).is_true()

    def test_has_next_line_returns_false_when_buffer_end_is_reached(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 3

        # Assert
        assert_that(context.has_next_line()).is_false()

    def test_copy_method_returns_new_context_with_same_properties(self):
        # Arrange
        context = AsserterContext(["line 1", "line 2", "line 3"])
        context.index = 1

        # Act
        copied_context = context.copy()

        # Assert
        assert_that(copied_context).is_not_same_as(context)
        assert_that(copied_context.buffer).is_equal_to(context.buffer)
        assert_that(copied_context.index).is_equal_to(context.index)


class TestAsserterBase(unittest.TestCase):
    def test_fail_raises_assertion_failure_with_message_from_asserter(self):
        # Arrange
        asserter = DummyAsserter(RootAsserter(""))

        # Act
        assert_that(asserter.fail).raises(AssertionFailure).when_called_with().contains(
            "AssertionFailure: Unable to find Dummy message"
        )

    def test_fail_raises_assertion_failure_with_message_from_augment_when_augments_present(self):
        # Arrange
        asserter = DummyAsserter(RootAsserter(""))
        asserter.add_augment(DummyAugment(), match=False)

        # Act
        assert_that(asserter.fail).raises(AssertionFailure).when_called_with().contains(
            "Dummy augment message"
        )

    def test_when_augment_fails_continue_searching_for_next_node(self):
        # Arrange
        asserter = DummyAsserter(RootAsserter(""))
        augment = DummyAugment()
        asserter.add_augment(augment, match=False)

        # Act
        with patch.object(augment, "match", side_effect=[False, True]):
            found = asserter._find_next_match(AsserterContext([]))

        # Assert
        assert_that(found).is_true()

    def test_when_augment_fails_after_being_added_it_should_force_parent_to_rematch(self):
        # Arrange
        asserter = DummyAsserter(RootAsserter(""))
        asserter.match = MagicMock(name="match")  # type: ignore
        asserter.last_match = MagicMock(name="last_match", return_value=1)  # type: ignore
        asserter.context = MagicMock(name="context")  # type: ignore
        augment = DummyAugment()

        # Act
        with patch.object(augment, "match", side_effect=[False, True]):
            asserter.add_augment(augment)

        # Assert
        asserter.match.assert_called_once()
