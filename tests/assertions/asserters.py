from __future__ import annotations
import re
from typing import Callable, Generic, Optional
from typing_extensions import Self

from tests.assertions import (
    AsserterBase,
    AsserterContext,
    AsserterRootContext,
    TAsserter,
    TParent,
    line_depth,
)
from tests.assertions.augments import (
    IsCollapsed,
    WithDirection,
    WithId,
    WithLabel,
    WithLinkEnd,
    WithLinkStart,
    WithLinkStyle,
    WithStyle,
    WithoutStyle,
)

LINK_REGEX = r"(?P<start>\S+) (?P<link>[~\-.>]+)(?P<label>\|[^\|]+\|)? (?P<end>\S+)"


class ContinuationAsserter(Generic[TParent, TAsserter], AsserterBase[TParent]):
    """A mocking asserter used for creating a placeholder asserter that mimics the behavior
    of another asserter.

    Args:
        target (TAsserter): The target asserter to mimic.
    """

    def __init__(
        self, hierarchical_parent: TParent, continue_asserter: TAsserter, message="followed by"
    ) -> None:
        super().__init__(hierarchical_parent)
        self.target = continue_asserter
        self._message = message

    def match(self, soft_fail: bool = False, rematch_parent=True) -> bool:
        return self.parent.match(soft_fail, rematch_parent)

    def _next_match(self, context: AsserterContext) -> bool:
        """
        Internal method for matching, always returns False.

        Returns:
            bool: Always returns False.
        """
        return False

    def message(self) -> str:
        return self._message

    def join_message(self) -> str:
        return ""

    def extract_context(self, context: AsserterContext):
        return self.target.context

    def __getattr__(self, name: str):
        attr = getattr(self.target, name)
        if callable(attr):
            # wrapped calls to bound method as they use their own self (target here) as parents
            # and we will need to replace the first parent created by the method call to ensure the
            # parent chain is correct
            attr = wrap_mock_call(attr, self, self.target)
        return attr

    @property
    def last_match(self):
        return self.parent.last_match

    @last_match.setter
    def last_match(self, value):
        pass


def wrap_mock_call(
    function: Callable[..., AsserterBase[TParent]], mock: AsserterBase, mock_target: AsserterBase
):
    def wrapper(*args, **kwargs):
        mock_target._creation_asserter = mock
        asserter = function(*args, **kwargs)
        mock_target._creation_asserter = None
        return asserter

    return wrapper


class FollowableAsserter(AsserterBase[TParent]):
    @property
    def followed_by(self) -> TParent:
        """Create a followed by asserter for the current asserter."""
        target = self.parent
        if not target:
            raise ValueError("Unable to obtain a proper parent")
        return ContinuationAsserter[Self, TParent](self, target)  # type: ignore

    @property
    def and_then(self) -> TParent:
        """Create a followed by asserter for the current asserter."""
        return self.followed_by


class CountableAsserter(AsserterBase[TParent]):
    """Asserters that can be counted - i.e. LineAsserter, GraphAsserter"""

    def first(self) -> FirstAsserter[Self]:
        a = FirstAsserter(self._get_creation_asserter())
        a.match()
        return a

    def last(self) -> LastAsserter[Self]:
        a = LastAsserter(self._get_creation_asserter())
        a.match()
        return a

    def times(self, count: int) -> ExactCountAsserter[Self]:
        a = ExactCountAsserter(self._get_creation_asserter(), count)
        a.match()
        return a

    def exactly(self, count: int) -> ExactCountAsserter[Self]:
        return self.times(count)

    def once(self) -> ExactCountAsserter[Self]:
        return self.times(1)

    def count(self, count: int) -> ExactCountAsserter[Self]:
        return self.times(count)

    def at_least(self, count: int) -> MinCountAsserter[Self]:
        a = MinCountAsserter(self._get_creation_asserter(), count)
        a.match()
        return a

    def min(self, count: int) -> MinCountAsserter[Self]:
        return self.at_least(count)

    def at_most(self, count: int) -> MaxCountAsserter[Self]:
        a = MaxCountAsserter(self._get_creation_asserter(), count)
        a.match()
        return a

    def max(self, count: int) -> MaxCountAsserter[Self]:
        return self.at_most(count)


class GraphAsserterBase(AsserterBase[TParent]):
    """Base class for graph-based asserters."""

    def __init__(self, parent: TParent):
        super().__init__(parent)

    @property
    def advance_on_match(self):
        return False

    def has_line(self, line: str):
        asserter = LineAsserter(self._get_creation_asserter(), line)
        asserter.match()
        return asserter

    def matches_line(self, pattern: str):
        asserter = RegexLineAsserter(self._get_creation_asserter(), pattern)
        asserter.match()
        return asserter

    def has_graph(
        self, graph: str | None = None, direct_descendants_only=True
    ) -> GraphAsserter[Self]:
        asserter = GraphAsserter[Self](
            self._get_creation_asserter(), direct_descendants_only=direct_descendants_only
        )
        asserter.match()

        return asserter.with_id(graph) if graph else asserter

    def has_rule(self, id: int | str | None = None) -> RuleAsserter[Self]:
        asserter = RuleAsserter(self._get_creation_asserter())
        asserter.match()

        return asserter.with_id(id) if id else asserter

    def has_link(self, start: str | None = None) -> LinkAsserter[Self]:
        asserter = LinkAsserter(self._get_creation_asserter())
        asserter.match()

        return asserter.with_from(start) if start else asserter

    def with_label(self, label: str) -> Self:
        self.add_augment(WithLabel(label))
        return self

    def is_collapsed(self) -> Self:
        self.add_augment(IsCollapsed())
        return self


class RootAsserter(AsserterBase):
    """Top-most asserter"""

    def __init__(self, graph: str = "") -> None:
        self.context = AsserterRootContext(graph)
        self.last_match: int | None = 0
        """Last match index in the parent context (heystack). Used for re-matching."""
        self.parent = None

    def message(self) -> str:
        return ""

    def match(self, soft_fail: bool = False, rematch_parent=True) -> bool:
        return False

    def _next_match(self, context: AsserterContext) -> bool:
        return False

    def extract_context(self, context: AsserterContext):
        return self.context


class RootGraphAsserter(
    RootAsserter,
    GraphAsserterBase[RootAsserter],
):
    """Root-level asserter for graph-based assertions."""

    def __init__(self, graph: str) -> None:
        RootAsserter.__init__(self, graph)
        self.graph = graph
        self._creation_asserter = self

    def message(self) -> str:
        return f"in graph:\n{self.graph}"

    def has_style(self, style) -> StyleAsserter[Self]:
        asserter = StyleAsserter(self._get_creation_asserter(), style)
        asserter.match()
        return asserter


class GraphAsserter(
    GraphAsserterBase[TParent],
    CountableAsserter[TParent],
    FollowableAsserter[TParent],
):
    """Asserter for matching a graph/subgraph in the context."""

    def __init__(self, parent: TParent, direct_descendants_only=False):
        super().__init__(parent)
        self.direct_descendants = direct_descendants_only

    def _next_match(self, context: AsserterContext) -> bool:
        while context.has_line():
            # ignore the first line of the parent graph asseter otherwise we will be matching the same graph
            if isinstance(self.parent, GraphAsserterBase) and context.index == 0:
                context.next_line()
                continue

            line = context.current_line() or ""
            if f"subgraph " in line:
                if not self.direct_descendants or line_depth(line) == self.parent.context.depth + 1:
                    return True
            context.next_line()
        return False

    def message(self) -> str:
        return f"subgraph"

    def extract_context(self, context: AsserterContext):
        graph_level = 0
        start = context.index
        while context.has_line():
            line: str = context.current_line() or ""
            if line.strip().startswith("subgraph "):
                graph_level += 1
            elif line.strip() == "end":
                graph_level -= 1
            if graph_level == 0:
                break
            context.next_line()

        return AsserterContext(context.buffer[start : context.index + 1])

    def with_matching_label(self, label: str) -> Self:
        self.add_augment(WithLabel(label, reg_ex=True))
        return self

    def with_direction(self, direction: str) -> Self:
        self.add_augment(WithDirection(direction))
        return self

    def with_id(self, id: str) -> Self:
        self.add_augment(WithId(id))
        return self


class CountAsserterBase(AsserterBase[TParent]):
    """Asserters that do counting operations"""

    def __init__(self, parent: TParent):
        super().__init__(parent)
        self.tally: int = 0

    def extract_context(self, context: AsserterContext):
        return AsserterContext([])

    def _get_follow_parent_asserter(self) -> AsserterBase:
        """Get the follow parent asserter."""
        return self.parent.parent

    def _next_match(self, context: AsserterContext) -> bool:
        return False

    @property
    def context(self):
        return self.parent.context

    def join_message(self) -> str:
        return ""


class ExactCountAsserter(CountAsserterBase[TParent]):
    def __init__(self, parent: TParent, count: int) -> None:
        super().__init__(parent)
        self._count = count

    def match(self, soft_fail: bool = False, rematch_parent=True) -> bool:
        count = 1
        while self.parent.match(True, False):
            count += 1

        self.tally = count
        success = count == self._count

        if not success and not soft_fail:
            self.fail()
        return success

    def message(self) -> str:
        return f"exactly {self._count} (found {self.tally} times)"


class MinCountAsserter(CountAsserterBase[TParent]):
    def __init__(self, parent: TParent, min: int) -> None:
        super().__init__(parent)
        self._min = min

    def match(self, soft_fail: bool = False, rematch_parent=True) -> bool:
        count = 1
        while self.parent.match(True, False):
            count += 1

        self.tally = count
        success = count >= self._min

        if not success and not soft_fail:
            self.fail()
        return success

    def message(self) -> str:
        return f"at least {self._min} times(found {self.tally} times)"


class MaxCountAsserter(CountAsserterBase[TParent]):
    def __init__(self, parent: TParent, min: int) -> None:
        super().__init__(parent)
        self._max = min

    def match(self, soft_fail: bool = False, rematch_parent=True) -> bool:
        count = 1
        while self.parent.match(True, False):
            count += 1

        self.tally = count
        success = count <= self._max

        if not success and not soft_fail:
            self.fail()
        return success

    def message(self) -> str:
        return f"at most {self._max} times (found {self.tally} times)"


class LineAsserter(
    CountableAsserter[TParent],
    FollowableAsserter[TParent],
):
    """Asserter for matching a line in the context."""

    def __init__(self, parent: TParent, pattern: str):
        super().__init__(parent)
        self.pattern: str = pattern

    def _next_match(self, context: AsserterContext) -> bool:
        while context.has_line():
            line: str = context.current_line() or ""
            if self._compare_line(line):
                return True
            context.next_line()
        return False

    def message(self) -> str:
        return f"line that matches '{self.pattern}'"

    def _compare_line(self, line: str):
        """
        Compare the given line with the pattern.

        Args:
            line (str): The line to compare.

        Returns:
            bool: True if the pattern is found in the line, False otherwise.
        """
        return self.pattern in line

    def extract_context(self, context: AsserterContext):
        return AsserterContext([context.buffer[context.index]])


class FirstAsserter(CountAsserterBase[TParent]):
    def __init__(self, parent: TParent) -> None:
        super().__init__(parent)

    def match(self, soft_fail: bool = False, rematch_parent=True) -> bool:
        # TODO: not happy with this implementation, it works for now but detecting when being called
        # form sub-asserter needs to be improved
        if soft_fail and rematch_parent:
            # when called to rematch parent, our strategy is to call next until the parent's contex changes
            if isinstance(self.parent, RootAsserter) or isinstance(
                self.parent.parent, RootAsserter
            ):
                return False if soft_fail else self.fail()

            while self.parent.match(True, False):
                pass

            return self.parent.match(True, True)
        else:
            self.parent.last_match = -1
            return self.parent.match()

    def message(self) -> str:
        return "[first element]"


class LastAsserter(CountAsserterBase[TParent]):
    def __init__(self, parent: TParent) -> None:
        super().__init__(parent)

    def match(self, soft_fail: bool = False, rematch_parent=True) -> bool:
        # TODO: not happy with this implementation, it works for now but detecting when being called
        # form sub-asserter needs to be improved
        if soft_fail and rematch_parent:
            if not self.parent.match(soft_fail, rematch_parent):
                return False

        # match until there are no more parents
        while self.parent.match(True, False):
            pass

        return True

    def message(self) -> str:
        return "[last element]"


class RegexLineAsserter(LineAsserter[TParent]):
    """Asserter for matching a line with a regular expression pattern."""

    def __init__(self, parent: TParent, pattern: str):
        super().__init__(parent, "")
        self.pattern: str = pattern
        self.compiled_pattern = re.compile(self.pattern)
        self.matcher: Optional[re.Match[str]] = None

    def _compare_line(self, line: str) -> bool:
        self.matcher = self.compiled_pattern.search(line)
        return self.matcher is not None


class StyleAsserter(LineAsserter[TParent]):
    def __init__(self, parent: TParent, style_id: str):
        super().__init__(parent, f"classDef {style_id}")
        self.style = style_id

    def message(self) -> str:
        return f"style '{self.style}'"


RULE_REGEX = r"^\s*([\w\-]+:){0,2}(collapsed)?rule"


class RuleAsserter(
    RegexLineAsserter[TParent],
):
    """Asserter for matching a graph/subgraph in the context."""

    def __init__(self, parent: TParent):
        super().__init__(parent, RULE_REGEX)

    def message(self) -> str:
        return "rule"

    def with_label(self, label: str) -> Self:
        self.add_augment(WithLabel(label))
        return self

    def matching_label(self, pattern: str) -> Self:
        self.add_augment(WithLabel(pattern, reg_ex=True))
        return self

    def with_id(self, id: int | str) -> Self:
        if isinstance(id, str) and re.match(RULE_REGEX, id):
            self.add_augment(WithId(id, use_regex=False))
        else:
            self.add_augment(WithId(rf"{RULE_REGEX}{id}", use_regex=True))
        return self

    def with_style(self, style: str) -> Self:
        self.add_augment(WithStyle(style))
        return self

    def without_style(self, style: str) -> Self:
        self.add_augment(WithoutStyle(style))
        return self

    def without_any_style(self) -> Self:
        self.add_augment(WithoutStyle())
        return self


class LinkAsserter(
    RegexLineAsserter[TParent],
):
    """Asserter for matching a graph/subgraph in the context."""

    def __init__(self, parent: TParent):
        super().__init__(parent, LINK_REGEX)

    def message(self) -> str:
        return "link"

    def with_label(self, label: str) -> Self:
        self.add_augment(WithLabel(label, "|", "|"))
        return self

    def matching_label(self, pattern: str) -> Self:
        self.add_augment(WithLabel(pattern, "|", "|", reg_ex=True))
        return self

    def with_from(self, id: str) -> Self:
        self.add_augment(WithLinkStart(id))
        return self

    def matching_from(self, pattern: str) -> Self:
        self.add_augment(WithLinkStart(pattern, use_regex=True))
        return self

    def with_to(self, id: str) -> Self:
        self.add_augment(WithLinkEnd(id))
        return self

    def matching_to(self, pattern: str) -> Self:
        self.add_augment(WithLinkEnd(pattern, use_regex=True))
        return self

    def with_style(self, style: str) -> Self:
        self.add_augment(WithLinkStyle(style))
        return self
