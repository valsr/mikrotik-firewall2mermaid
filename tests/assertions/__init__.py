from __future__ import annotations
from abc import abstractmethod
from typing import Any, Generic, TypeVar
from typing_extensions import Self
from typing import TypeVar, Generic
import re
from typing import Optional


TParent = TypeVar("TParent", bound="AsserterBase")
TAsserter = TypeVar("TAsserter", bound="AsserterBase")
TMock = TypeVar("TMock", bound="AsserterBase")


SHAPE_STYLES = {
    "default": ("[", "]"),
    "round": ("(", ")"),
    "stadium": ("([", "])"),
    "subroutine": ("[[", "]]"),
    "cylindrical": ("[(", ")]"),
    "circle": ("((", "))"),
    "asymetric": (">", "]"),
    "node": ("{", "}"),
    "hexagon": ("{{", "}}"),
    "parallelogram": ("[/", "/]"),
    "parallelogram_alt": ("[\\", "\\]"),
    "trapezoid": ("[/", "\\]"),
    "trapezoid_alt": ("[\\", "/]"),
    "doublecircle": ("(((", ")))"),
}


def line_depth(line: str) -> int:
    return len(line) - len(line.lstrip("\t"))


class AssertionFailure(BaseException):
    """Exception for assertion failures."""

    def __init__(self, message: str, prefix: str = "AssertionFailure: Unable to find "):
        self.message = message
        self.prefix = prefix

    def __str__(self):
        return f"{self.prefix}{self.message}"


class AssertionError(BaseException):
    """Exception for generic assertion errors."""

    def __init__(self, message: str, prefix: str = "AssertionError: "):
        self.message = message
        self.prefix = prefix

    def __str__(self):
        return f"{self.prefix}{self.message}"


class AsserterContext:
    """Context for asserter operations."""

    def __init__(self, buffer: list[str]) -> None:
        self.buffer = buffer
        """The buffer to search in."""
        self.index = 0
        """Current line index in the buffer."""
        self.depth = line_depth(buffer[0]) if buffer else 0
        """Depth of the current context"""

    def reconstruct_buffer(self) -> str:
        return "\n".join(self.buffer)

    def current_line(self) -> str | None:
        """Get the current line from the context.

        Returns:
            str|None: The current line or None if buffer end is reached.
        """
        return self.buffer[self.index] if self.index < len(self.buffer) else None

    def next_line(self) -> str | None:
        """Move to the next line in the context.

        Returns:
            str|None: The next line or None if buffer end is reached.
        """
        if self.has_next_line():
            self.index += 1
        else:
            self.index = len(self.buffer)
        return self.current_line()

    def peek_next_line(self) -> str | None:
        """Obtains the next line without advancing the index.

        Returns:
            str|None: Next line in buffer or None if the buffer end is reached.
        """
        return self.buffer[self.index + 1] if self.has_next_line() else None

    def has_line(self) -> bool:
        """Checks if the buffer has a current line.

        Returns:
            bool: True if the current line exists in the buffer (might be still None), False otherwise
        """
        return self.index < len(self.buffer)

    def has_next_line(self) -> bool:
        """Checks if the buffer has a next line.

        Returns:
            bool: True if at least one more line exists in the buffer. False, if the buffer end
            is/will be reached during the next call to next_line.
        """
        return self.index < len(self.buffer) - 1

    def copy(self) -> AsserterContext:
        context = AsserterContext(self.buffer)
        context.index = self.index
        return context

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, AsserterContext):
            return False
        return (
            self.buffer == other.buffer and self.index == other.index and self.depth == other.depth
        )

    def __len__(self):
        return len(self.buffer)


class AsserterRootContext(AsserterContext):
    """Context for the root asserter."""

    def __init__(self, buffer: str):
        super().__init__(buffer.splitlines())
        self.graph = buffer
        self.depth = -1

    def next_line(self):
        return self.current_line()

    def peek_next_line(self):
        return self.current_line()

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, AsserterRootContext):
            return False
        return super().__eq__(other)


class AsserterBase(Generic[TParent]):
    """Base class for asserters."""

    def __init__(self, parent: TParent):
        self._creation_asserter: TParent | None = None
        self.parent: TParent = parent
        self.context: AsserterContext
        self.last_match: int | None = None
        """Last match index in the parent context (heystack). Used for re-matching."""
        self.augments: list[AugmentBase] = []

    def _get_creation_asserter(self) -> Self:
        if self._creation_asserter:
            return self._creation_asserter  # type: ignore
        return self

    def _init_matching_context(self) -> AsserterContext:
        from tests.assertions.asserters import ContinuationAsserter

        context = self.parent.context.copy()

        # if parent is ContinuationAsserter asserter we need to calculate the first index past the first match
        if self.last_match is not None:
            context.index = self.last_match + 1
        elif isinstance(self.parent, ContinuationAsserter):
            # need to adjust the start index to be after the last match of the parent + its context size
            context.index = (self.parent.last_match or 0) + len(self.parent.parent.context)

        return context

    def match(self, soft_fail: bool = False, propagate=True) -> bool:
        """Finds the next match in the context for the current asserter. If a match isn't found in
        the current context it will call the parent asserter to advance its context and will try
        again.

        Args:
            soft_fail (bool, optional): When set to True will call fail method which will raise
                AssertionFailure error. If set to false it will return False instead. Defaults to False.
            rematch_parent (bool, optional): When set to True will attempt to return the matcher in
            the the parent asserter and try again
                asserter. Defaults to True.

        Returns:
            bool: True if a match is found, False otherwise
        """

        # match parent if needed
        from tests.assertions.asserters import RootAsserter

        if not isinstance(self.parent, RootAsserter) and self.parent.last_match is None:
            self.parent.match()

        # loop instead of calling match again to keep
        heystack: Optional[AsserterContext] = None
        while True:
            if not heystack:
                heystack = self._init_matching_context()

            if not heystack.has_line():
                break

            if self._find_next_match(heystack):
                self.last_match = heystack.index
                self.context = self.extract_context(heystack)
                return True

            if propagate:
                # parent found a new match, re-match
                if self.parent.match(True, propagate):
                    heystack = None
                    self.last_match = None
                    continue

            # exit after matching failing ot match and/or propagate
            break

        return False if soft_fail else self.fail()

    def _find_next_match(self, context: AsserterContext) -> bool:
        """Find the next match in the context for the current parent asserter.

        Args:
            context (AsserterContext): The context to search in.

        Returns:
            bool: True if a match is found, False otherwise.
        """
        while True:
            if not self._next_match(context):
                return False

            augment_context = self.extract_context(context.copy())
            if self._match_augments(augment_context):
                return True
            context.next_line()

    def _match_augments(self, context: AsserterContext) -> bool:
        for augment in self.augments:
            if not augment.match(context.copy()):
                return False
        return True

    @abstractmethod
    def _next_match(self, context: AsserterContext) -> bool:
        """Implement the logic to check for the next match in the context.

        Returns:
            bool: True if a match is found, False otherwise.
        """
        raise NotImplementedError()

    def fail(self, message: str = ""):
        """Raise an AssertionFailure with a formatted error message.

        Args:
            message (str): The error message. Defaults to an empty string.
        """
        chain = self._parents()
        chain.append(self)

        message = ""
        root = chain[0]
        for asserter in chain[1:]:
            message_part = self._get_asserter_message(asserter)
            if (
                message
                and message_part
                and asserter.join_message()
                and not isinstance(asserter.parent, ContinuationAsserter)
            ):
                message_part = f"{asserter.join_message()} {message_part}"
            if message_part:
                message += f" {message_part}"

        raise AssertionFailure(message.lstrip() + " " + root.message())

    def _get_asserter_message(self, asserter: AsserterBase) -> str:
        message = asserter.message()
        if len(asserter.augments) > 0:
            message += " with " + ", ".join([augment.message() for augment in asserter.augments])
        return message

    @abstractmethod
    def extract_context(self, parent_context: AsserterContext) -> AsserterContext:
        """Sets the context after a successful rematch from the parent asserter (to propagate down)."""
        raise NotImplementedError()

    def _parents(self) -> list[AsserterBase[Any]]:
        """Retrieve a list of parent nodes in declared order.

        Returns:
            list[BaseAsserter[Any]]: A list containing parent nodes in declared order
        """
        parents: list[AsserterBase] = []
        p = self.parent
        while True:
            parents.append(p)
            p = p.parent
            if not p:
                break
        parents.reverse()
        return parents

    @abstractmethod
    def message(self) -> str:
        """Define the template for the error message.

        Returns:
            str: The error message template.
        """
        raise NotImplementedError()

    def join_message(self) -> str:
        return "that contains"

    def add_augment(self, augment: AugmentBase, match: bool = True) -> AugmentBase:
        self.augments.append(augment)

        if match:
            if self.last_match is None or not augment.match(self.context):
                self.match()
        return augment

    def at_depth(self, level: int) -> Self:
        from tests.assertions.augments import AtDepth

        self.add_augment(AtDepth(level))
        return self

    def at_min_depth(self, level: int) -> Self:
        from tests.assertions.augments import AtMinDepth

        self.add_augment(AtMinDepth(level))
        return self

    def at_max_depth(self, level: int) -> Self:
        from tests.assertions.augments import AtMinDepth

        self.add_augment(AtMinDepth(level))
        return self


class AugmentBase:
    @abstractmethod
    def match(self, context: AsserterContext) -> bool:
        raise NotImplementedError()

    @abstractmethod
    def message(self) -> str:
        raise NotImplementedError()


from tests.assertions.asserters import ContinuationAsserter, RootGraphAsserter


def assert_graph(graph: str) -> RootGraphAsserter:
    """Factory function to create a RootGraphAsserter instance."""
    return RootGraphAsserter(graph)
