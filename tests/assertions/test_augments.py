from unittest.mock import MagicMock
from assertpy import assert_that
import pytest
from firewall2mermaid.common import LINK_STYLE_BETWEEN_SECTIONS, LINK_STYLE_INVISIBLE
from tests.assertions.augments import (
    AtDepth,
    AtMaxDepth,
    AtMinDepth,
    IsCollapsed,
    WithDirection,
    WithId,
    WithLabel,
    AsserterContext,
    WithLinkEnd,
    WithLinkStart,
    WithLinkStyle,
    WithStyle,
    WithoutStyle,
)


class TestWithLabel:
    def test_match_returns_true_when_label_matches(self):
        # Arrange
        context = AsserterContext(["[test]"])
        augment = WithLabel("test")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_label_does_not_match(self):
        # Arrange
        context = AsserterContext(["[other]"])
        augment = WithLabel("test")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_match_returns_false_when_label_missing(self):
        # Arrange
        context = AsserterContext(["node"])
        augment = WithLabel("test")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = WithLabel("test")

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("label 'test'")

    def test_match_returns_true_when_label_matches_when_using_same_close_open_character(self):
        # Arrange
        context = AsserterContext(["|test|"])
        augment = WithLabel("test", "|", "|")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_true_when_label_matches_when_regex(self):
        # Arrange
        context = AsserterContext(["[a test123]"])
        augment = WithLabel(r"test\d", reg_ex=True)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_label_does_not_match_when_regex(self):
        # Arrange
        context = AsserterContext(["[other]"])
        augment = WithLabel(r"test\d+", reg_ex=True)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message_when_regex(self):
        # Arrange
        augment = WithLabel(r"test\d+", reg_ex=True)

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("label matching 'test\\d+'")

    def test_match_returns_true_when_label_matches_when_using_same_close_open_character_when_regex(
        self,
    ):
        # Arrange
        context = AsserterContext(["|test123|"])
        augment = WithLabel(r"test\d+", "|", "|", reg_ex=True)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()


class TestWithStyle:
    def test_match_returns_true_when_style_matches(self):
        # Arrange
        context = AsserterContext([":::bold"])
        augment = WithStyle("bold")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_style_does_not_match(self):
        # Arrange
        context = AsserterContext([":::italic"])
        augment = WithStyle("bold")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message_with_style(self):
        # Arrange
        augment = WithStyle("italic")

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("style 'italic'")


class TestWithoutStyle:
    def test_match_returns_true_when_no_style_present(self):
        # Arrange
        context = AsserterContext(["Some line without style"])
        augment = WithoutStyle()

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_true_when_style_does_not_match(self):
        # Arrange
        context = AsserterContext(["Some line with style:::bold"])
        augment = WithoutStyle("italic")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_style_matches(self):
        # Arrange
        context = AsserterContext([":::bold"])
        augment = WithoutStyle("bold")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message_with_style(self):
        # Arrange
        augment = WithoutStyle("italic")

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("no style matching 'italic'")

    def test_message_returns_expected_message_without_style(self):
        # Arrange
        augment = WithoutStyle()

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("no styles")


class TestWithId:
    @pytest.mark.parametrize(
        "case", ["test_id[label]", "node test_id[label]", "node test_id [label]"]
    )
    def test_match_returns_true_when_id_matches(self, case):
        # Arrange
        context = AsserterContext([case])
        augment = WithId("test_id")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_when_have_node_type_returns_true_when_id_matches(self):
        # Arrange
        context = AsserterContext(["nodetype nodeid"])
        augment = WithId("nodeid")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    @pytest.mark.parametrize(
        "case", ["test_id[label]", "node test_id[label]", "node test_id [label]"]
    )
    def test_match_returns_true_when_id_matches_when_using_regex(self, case):
        # Arrange
        context = AsserterContext([case])
        augment = WithId(".*id", use_regex=True)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_when_have_node_type_returns_true_when_id_matches_when_using_Regex(self):
        # Arrange
        context = AsserterContext(["nodetype nodeid"])
        augment = WithId(".*id", use_regex=True)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_id_does_not_match(self):
        # Arrange
        context = AsserterContext(["node"])
        augment = WithId("test_id")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = WithId("test_id")

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("id 'test_id'")


class TestWithDirection:
    def test_match_returns_true_when_direction_matches(self):
        # Arrange
        context = AsserterContext(["direction right"])
        augment = WithDirection("right")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_direction_does_not_match(self):
        # Arrange
        context = AsserterContext(["direction left"])
        augment = WithDirection("right")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_match_returns_false_when_direction_not_found(self):
        # Arrange
        context = AsserterContext(["a sample line. right"])
        augment = WithDirection("right")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = WithDirection("right")

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("direction 'right'")


class TestAtDepth:
    def test_match_returns_true_when_depth_matches(self):
        # Arrange
        context = AsserterContext(["\t" * 3 + "Some line"])
        augment = AtDepth(3)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_depth_does_not_match(self):
        # Arrange
        context = AsserterContext(["\t" * 2 + "Some line"])
        augment = AtDepth(3)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = AtDepth(3)

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("at depth of 3")


class TestAtMinDepth:
    def test_match_returns_true_when_depth_is_greater_than_or_equal_to_level(self):
        # Arrange
        context = AsserterContext(["\t" * 3 + "Some line"])
        augment = AtMinDepth(3)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_depth_is_less_than_level(self):
        # Arrange
        context = AsserterContext(["\t" * 2 + "Some line"])
        augment = AtMinDepth(3)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = AtMinDepth(3)

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("at minimum depth of 3")


class TestAtMaxDepth:
    def test_match_returns_true_when_depth_is_less_than_or_equal_to_level(self):
        # Arrange
        context = AsserterContext(["\t" * 3 + "Some line"])
        augment = AtMaxDepth(3)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_depth_is_greater_than_level(self):
        # Arrange
        context = AsserterContext(["\t" * 4 + "Some line"])
        augment = AtMaxDepth(3)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = AtMaxDepth(3)

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("at maximum depth of 3")


class TestWithLinkStyle:
    def test_match_returns_true_when_style_matches(self):
        # Arrange
        context = AsserterContext([f"from {LINK_STYLE_BETWEEN_SECTIONS} to"])
        augment = WithLinkStyle(LINK_STYLE_BETWEEN_SECTIONS)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_style_does_not_match(self):
        # Arrange
        context = AsserterContext([f"from {LINK_STYLE_INVISIBLE} to"])
        augment = WithLinkStyle(LINK_STYLE_BETWEEN_SECTIONS)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_match_returns_false_when_link_not_found(self):
        # Arrange
        context = AsserterContext([f"rule1 [label]"])
        augment = WithLinkStyle(LINK_STYLE_BETWEEN_SECTIONS)

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = WithLinkStyle(LINK_STYLE_BETWEEN_SECTIONS)

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to(f"link style '{LINK_STYLE_BETWEEN_SECTIONS}'")


class TestWithLinkStart:
    def test_match_returns_true_when_start_matches_pattern(self):
        # Arrange
        context = AsserterContext(["start --> end"])
        augment = WithLinkStart("start")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_start_does_not_match_pattern(self):
        # Arrange
        context = AsserterContext(["other --> end"])
        augment = WithLinkStart("start")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = WithLinkStart("start")

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("from 'start'")

    def test_message_returns_expected_message_when_using_regex(self):
        # Arrange
        context = AsserterContext(["start --> end"])
        augment = WithLinkStart("start", use_regex=True)

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("from matching 'start'")


class TestWithLinkEnd:
    def test_match_returns_true_when_end_matches_pattern(self):
        # Arrange
        context = AsserterContext(["start --> test"])
        augment = WithLinkEnd("test")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    def test_match_returns_false_when_end_does_not_match_pattern(self):
        # Arrange
        context = AsserterContext(["start --> other"])
        augment = WithLinkEnd("test")

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = WithLinkEnd("test")

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("to 'test'")

    def test_message_returns_expected_message_when_using_regex(self):
        # Arrange
        augment = WithLinkEnd("test", use_regex=True)

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("to matching 'test'")


class TestIsCollapsed:
    def test_match_returns_true_when_context_is_collapsed(self):
        # Arrange
        context = AsserterContext(["subgraph X", "end"])
        augment = IsCollapsed()

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_true()

    @pytest.mark.parametrize("context", [["subgraph X"], ["subgraph X", "end", "extra line"]])
    def test_match_returns_false_when_context_is_not_collapsed(self, context):
        # Arrange
        context = AsserterContext(context)
        augment = IsCollapsed()

        # Act
        result = augment.match(context)

        # Assert
        assert_that(result).is_false()

    def test_message_returns_expected_message(self):
        # Arrange
        augment = IsCollapsed()

        # Act
        result = augment.message()

        # Assert
        assert_that(result).is_equal_to("that is collapsed")
