from assertpy import assert_that
from unittest.mock import patch
from unittest.mock import patch

import pytest
from firewall2mermaid.common import LINK_STYLE_NORMAL
from tests.assertions import (
    AsserterContext,
    AsserterRootContext,
    AssertionFailure,
    RootGraphAsserter,
)
from tests.assertions.asserters import (
    ContinuationAsserter,
    ExactCountAsserter,
    FirstAsserter,
    GraphAsserter,
    LastAsserter,
    LineAsserter,
    LinkAsserter,
    MaxCountAsserter,
    MinCountAsserter,
    ContinuationAsserter,
    RegexLineAsserter,
    RootAsserter,
    RuleAsserter,
    StyleAsserter,
)
from tests.assertions.augments import (
    WithId,
    WithLabel,
    WithLinkEnd,
    WithLinkStart,
    WithLinkStyle,
    WithStyle,
    WithoutStyle,
)

sample_graph = """subgraph A
\tsubgraph A1
\t\t\tA1 -> A2
\t\t\tA1 -> A2
\tend
end
%% comment line
subgraph B
\tsubgraph B1
\t\tB1 -> B2
\t\tB1 -> B2
\tend
\tsubgraph B2
\t\tB2 -> B3
\tend
end
subgraph C
end
"""

multi_line_text = "\n".join(
    [
        "This is line 1",
        "This is line 2",
        "This is line 3",
        "This is duplicated line",
        "This is duplicated line",
        "This is line 6",
    ]
)


class TestRootAsserter:
    def test_instantiation_without_arguments(self):
        asserter = RootAsserter()
        assert_that(asserter).is_not_none()

    def test_context_instance_of_AsserterRootContext(self):
        asserter = RootAsserter()
        assert_that(asserter.context).is_instance_of(AsserterRootContext)

    def test_extract_context_does_nothing(self):
        asserter = RootAsserter()
        context = AsserterContext(["line 1", "line 2", "line 3"])
        asserter.extract_context(context)
        assert_that(context.index).is_equal_to(0)

    def test_message_method_returns_empty_string(self):
        asserter = RootAsserter()
        assert_that(asserter.message()).is_empty()

    def test_match_method_returns_false_by_default(self):
        asserter = RootAsserter()
        assert_that(asserter.match()).is_false()

    def test_last_match_returns_zero(self):
        asserter = RootAsserter()

        assert_that(asserter.last_match).is_equal_to(0)


class TestRootGraphAsserter:
    def test_instantiation_with_valid_graph(self, sample_graph_text):
        asserter = RootGraphAsserter(sample_graph_text)
        assert_that(asserter).is_not_none()

    def test_context_instance_of_AsserterRootContext(self, sample_graph_text):
        asserter = RootGraphAsserter(sample_graph_text)
        assert_that(asserter.context).is_instance_of(AsserterRootContext)

    def test_extract_returns_current_context(self, sample_graph_text):
        asserter = RootGraphAsserter(sample_graph_text)
        context = AsserterContext(["line 1", "line 2", "line 3"])
        assert_that(asserter.extract_context(context)).is_equal_to(asserter.context)

    def test_message_method_returns_graph_message(self, sample_graph_text):
        asserter = RootGraphAsserter(sample_graph_text)
        assert_that(asserter.message()).is_equal_to(f"in graph:\n{sample_graph_text}")

    def test_match_method_returns_false_by_default(self, sample_graph_text):
        asserter = RootGraphAsserter(sample_graph_text)
        assert_that(asserter.match()).is_false()

    def text_has_style_with_valid_style(self, sample_graph_text):
        asserter = RootGraphAsserter(sample_graph_text)
        assert_that(asserter.has_style("log")).is_instance_of(StyleAsserter)

    def test_has_style_continued_uses_mock_as_self(self):
        # Arrange
        parent = RootGraphAsserter("Line 1\nclassDef dummy fill:#123456")
        asserter = ContinuationAsserter(LineAsserter(parent, "Line"), parent)

        # Act
        sut = asserter.has_style("dummy")

        # Assert
        assert_that(sut.parent).is_not_none().is_instance_of(ContinuationAsserter)


class TestLineAsserter:
    def test_instantiation_with_valid_arguments(self):
        parent = RootAsserter("line")
        pattern = "test"
        line_asserter = LineAsserter(parent, pattern)
        assert_that(line_asserter).is_not_none()
        assert_that(line_asserter.parent).is_equal_to(parent)
        assert_that(line_asserter.pattern).is_equal_to(pattern)

    def test_message_method_returns_expected_message(self):
        parent = RootAsserter("line")
        pattern = "line 1"
        line_asserter = LineAsserter(parent, pattern)
        assert_that(line_asserter.message()).is_equal_to(f"line that matches '{pattern}'")

    def test_match_line_with_pattern(self):
        # Arrange
        parent = RootAsserter("line 1\nline 2\nline 3")
        pattern = "line 2"
        line_asserter = LineAsserter(parent, pattern)

        # Act
        result = line_asserter.match()

        # Assert
        assert_that(result).is_true()

    def test_fail_to_match_when_pattern_not_found(self):
        # Arrange
        parent = RootAsserter("line 1\nline 2\nline 3")
        pattern = "line 4"
        line_asserter = LineAsserter(parent, pattern)

        # Act
        result = line_asserter.match(soft_fail=True)

        # Assert
        assert_that(result).is_false()

    def test_matches_first_line_when_multiple_lines_with_pattern_present(self):
        # Set up
        buffer = "\n".join(
            [
                "This is line 1",
                "This is line 2 with the pattern",
                "This is line 3",
                "This is line 4 with the pattern",
                "This is line 5",
            ]
        )
        parent = RootAsserter(buffer)
        pattern = "pattern"
        line_asserter = LineAsserter(parent, pattern)

        # Execute
        result = line_asserter.match()

        # Assert
        assert_that(result).is_true()
        assert_that(line_asserter.context.buffer).contains_only("This is line 2 with the pattern")


class TestRegexLineAsserter:
    def test_instantiation_with_valid_arguments(self):
        parent = RootAsserter("line")
        pattern = "test"
        regex_line_asserter = RegexLineAsserter(parent, pattern)
        assert_that(regex_line_asserter).is_not_none()
        assert_that(regex_line_asserter.parent).is_equal_to(parent)
        assert_that(regex_line_asserter.pattern).is_equal_to(pattern)

    def test_message_method_returns_expected_message(self):
        parent = RootAsserter("line")
        pattern = "line 1"
        regex_line_asserter = RegexLineAsserter(parent, pattern)
        assert_that(regex_line_asserter.message()).is_equal_to(f"line that matches '{pattern}'")

    def test_match_line_with_pattern(self):
        # Arrange
        parent = RootAsserter("line 1\nline 2\nline 3")
        pattern = "line 2"
        regex_line_asserter = RegexLineAsserter(parent, pattern)

        # Act
        result = regex_line_asserter.match()

        # Assert
        assert_that(result).is_true()

    def test_fail_to_match_when_pattern_not_found(self):
        # Arrange
        parent = RootAsserter("line 1\nline 2\nline 3")
        pattern = "line 4"
        regex_line_asserter = RegexLineAsserter(parent, pattern)

        # Act
        result = regex_line_asserter.match(soft_fail=True)

        # Assert
        assert_that(result).is_false()

    def test_matches_first_line_when_multiple_lines_with_pattern_present(self):
        # Set up
        buffer = "\n".join(
            [
                "This is line 1",
                "This is line 2 with the pattern",
            ]
        )
        parent = RootAsserter(buffer)
        pattern = r"pat+ern"
        regex_line_asserter = RegexLineAsserter(parent, pattern)

        # Execute
        result = regex_line_asserter.match()

        # Assert
        assert_that(result).is_true()
        assert_that(regex_line_asserter.context.buffer).contains_only(
            "This is line 2 with the pattern"
        )


class TestGraphAsserter:
    def test_matched_context_includes_full_graph_definition(self):
        # Arrange
        root = RootAsserter("subgraph A\nend")
        sut = GraphAsserter(root)

        # Act
        sut.match()

        # Assert
        assert_that(sut.context.index).is_equal_to(0)
        assert_that(sut.context.buffer).contains("subgraph A", "end").is_length(2)

    # Sub-subgraph should ignore the first line of its parent subgraph matcher
    def test_child_graph_asserter_ignores_first_line_of_parent_graph_asseter(self):
        # Arrange
        root = RootAsserter(sample_graph)
        parent = GraphAsserter(root)
        sut = GraphAsserter(parent)

        # Act
        parent.match()
        sut.match()

        # Assert
        assert_that(sut.context.index).is_equal_to(0)
        # parent matches subgraph A, so the first line of subgraph A1 is ignored
        assert_that(sut.context.reconstruct_buffer().lstrip()).starts_with("subgraph A1")

    def test_has_line_continued_uses_mock_as_self(self):
        # Arrange
        continue_from = GraphAsserter(RootAsserter("subgraph A\nsubgraph B\nend\nA1end\n"))
        parent = GraphAsserter(continue_from)
        asserter = ContinuationAsserter(parent, continue_from)

        # Act
        parent.match()
        sut = asserter.has_line("A1")

        # Assert
        assert_that(sut.parent).is_not_none().is_instance_of(ContinuationAsserter)

    def test_matches_line_continued_uses_mock_as_self(self):
        # Arrange
        continue_from = GraphAsserter(RootAsserter("subgraph A\nsubgraph B\nend\nA1end\n"))
        parent = GraphAsserter(continue_from)
        asserter = ContinuationAsserter(parent, continue_from)

        # Act
        parent.match()
        sut = asserter.matches_line("A.*")

        # Assert
        assert_that(sut.parent).is_not_none().is_instance_of(ContinuationAsserter)

    def test_has_graph_continued_uses_mock_as_self(self):
        # Arrange
        continue_from = GraphAsserter(
            RootAsserter("subgraph A\nsubgraph B\nend\nsubgraph C\nend\nend\n")
        )
        parent = GraphAsserter(continue_from)
        asserter = ContinuationAsserter(parent, continue_from)

        # Act
        parent.match()
        sut = asserter.has_graph(direct_descendants_only=False)

        # Assert
        assert_that(sut.parent).is_not_none().is_instance_of(ContinuationAsserter)

    def test_has_rule_continued_uses_mock_as_self(self):
        # Arrange
        continue_from = GraphAsserter(
            RootAsserter("subgraph A\nsubgraph B\nend\nrule[rule]\nend\n")
        )
        parent = GraphAsserter(continue_from)
        asserter = ContinuationAsserter(parent, continue_from)

        # Act
        parent.match()
        sut = asserter.has_rule()

        # Assert
        assert_that(sut.parent).is_not_none().is_instance_of(ContinuationAsserter)

    def test_has_link_continued_uses_mock_as_self(self):
        # Arrange
        continue_from = GraphAsserter(
            RootAsserter("subgraph A\nsubgraph B\nend\nrule --> rule\nend\n")
        )
        parent = GraphAsserter(continue_from)
        asserter = ContinuationAsserter(parent, continue_from)

        # Act
        parent.match()
        sut = asserter.has_link()

        # Assert
        assert_that(sut.parent).is_not_none().is_instance_of(ContinuationAsserter)


class TestStyleAsserter:
    def test_instantiation_with_valid_arguments(self):
        parent = RootAsserter("line")
        style_id = "test"
        style_asserter = StyleAsserter(parent, style_id)
        assert_that(style_asserter).is_not_none()
        assert_that(style_asserter.parent).is_equal_to(parent)
        assert_that(style_asserter.style).is_equal_to(style_id)

    def test_message_method_returns_expected_message(self):
        parent = RootAsserter("line")
        style_id = "style1"
        style_asserter = StyleAsserter(parent, style_id)
        assert_that(style_asserter.message()).is_equal_to(f"style '{style_id}'")

    def test_match_line_with_style(self):
        # Arrange
        parent = RootAsserter("line 1\nclassDef style rgbabel")
        style_id = "style"
        style_asserter = StyleAsserter(parent, style_id)

        # Act
        result = style_asserter.match()

        # Assert
        assert_that(result).is_true()


class TestRuleAsserter:
    def test_instantiation_with_valid_arguments(self):
        parent = RootAsserter('first line\nsection:chain:rule1["with comment"]:::drop')
        rule_asserter = RuleAsserter(parent)
        assert_that(rule_asserter).is_not_none()
        assert_that(rule_asserter.parent).is_equal_to(parent)

    def test_message_method_returns_expected_message(self):
        parent = RootAsserter('first line\nsection:chain:rule1["with comment"]:::drop')
        rule_asserter = RuleAsserter(parent)
        assert_that(rule_asserter.message()).is_equal_to("rule")

    @pytest.mark.parametrize(
        "rule",
        [
            "rule1",
            "section:collapsedrule",
            "section:chain:collapsedrule",
            "section:chain:rule1",
            "section:rule1",
            'section:chain:rule1["with comment"]:::drop',
            "section:chain-special-chars:rule1",
        ],
    )
    def test_match_when_is_rule_returns_true(self, rule):
        # Arrange
        rule_asserter = RuleAsserter(RootAsserter(rule))

        # Act
        result = rule_asserter.match(soft_fail=True)

        # Assert
        assert_that(result).is_true()

    @pytest.mark.parametrize(
        "rule",
        ["notarule", "subgraph rule", "section: not a rule"],
    )
    def test_match_when_not_valid_rule_line_returns_false(self, rule):
        # Arrange
        rule_asserter = RuleAsserter(RootAsserter(rule))

        # Act
        result = rule_asserter.match(soft_fail=True)

        # Assert
        assert_that(result).is_false()

    def test_with_label_adds_WithLabel_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter('first line\nsection:chain:rule1["with comment"]:::drop')
        rule_asserter = RuleAsserter(parent)
        label = "comment"

        # Act
        result = rule_asserter.with_label(label)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLabel)
        assert_that(result.context.reconstruct_buffer()).is_equal_to(
            'section:chain:rule1["with comment"]:::drop'
        )

    def test_matching_label_adds_WithMatchingLabel_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter('first line\nsection:chain:rule1["with comment"]:::drop')
        rule_asserter = RuleAsserter(parent)
        pattern = "com+ent"

        # Act
        result = rule_asserter.matching_label(pattern)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLabel)
        assert_that(result.context.reconstruct_buffer()).is_equal_to(
            'section:chain:rule1["with comment"]:::drop'
        )

    def test_with_id_adds_WithId_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter('first line\nsection:chain:rule1["with comment"]:::drop')
        rule_asserter = RuleAsserter(parent)
        id = 1

        # Act
        result = rule_asserter.with_id(id)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithId)
        assert_that(result.context.reconstruct_buffer()).is_equal_to(
            'section:chain:rule1["with comment"]:::drop'
        )

    def test_with_style_adds_WithStyle_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter('first line\nsection:chain:rule1["with comment"]:::drop')
        rule_asserter = RuleAsserter(parent)
        style = "drop"

        # Act
        result = rule_asserter.with_style(style)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithStyle)
        assert_that(result.context.reconstruct_buffer()).is_equal_to(
            'section:chain:rule1["with comment"]:::drop'
        )

    def test_without_style_adds_WithoutStyle_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter('first line\nsection:chain:rule1["with comment"]:::drop')
        rule_asserter = RuleAsserter(parent)
        style = "style"

        # Act
        result = rule_asserter.without_style(style)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithoutStyle)
        assert_that(result.context.reconstruct_buffer()).is_equal_to(
            'section:chain:rule1["with comment"]:::drop'
        )

    def test_with_id_when_given_string_id_match_without_prefixing(self):
        # Arrange
        parent = RootAsserter("section:ruleid[my rule]")
        rule_asserter = RuleAsserter(parent)

        # Act
        result = rule_asserter.with_id("section:ruleid")

        # Assert
        assert_that(result.augments).is_not_empty()
        augment = result.augments[0]
        assert_that(augment).is_type_of(WithId)
        assert isinstance(augment, WithId)
        assert_that(augment.id).is_equal_to("section:ruleid")
        assert_that(augment.use_regex).is_false()

    def test_without_any_style_adds_WithoutStyle_augment_without_style_argument_and_runs_match(
        self,
    ):
        # Arrange
        parent = RootAsserter('first line\nsection:chain:rule1["with comment"]')
        rule_asserter = RuleAsserter(parent)

        # Act
        result = rule_asserter.without_any_style()

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithoutStyle)
        assert_that(result.context.reconstruct_buffer()).is_equal_to(
            'section:chain:rule1["with comment"]'
        )


class TestLinkAsserter:
    def test_instantiation_with_valid_arguments(self):
        parent = RootAsserter("first line\nrule1 --> rule2")
        link_asserter = LinkAsserter(parent)
        assert_that(link_asserter).is_not_none()
        assert_that(link_asserter.parent).is_equal_to(parent)

    def test_message_method_returns_expected_message(self):
        parent = RootAsserter("first line\nrule1 --> rule2")
        link_asserter = LinkAsserter(parent)
        assert_that(link_asserter.message()).is_equal_to("link")

    def test_with_label_adds_WithLabel_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter("first line\nrule1 -->|link label| rule2")
        link_asserter = LinkAsserter(parent)
        label = "label"

        # Act
        result = link_asserter.with_label(label)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLabel)
        assert_that(result.context.reconstruct_buffer()).is_equal_to("rule1 -->|link label| rule2")

    def test_matching_label_adds_WithMatchingLabel_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter("first line\nrule1 -->|link label| rule2")
        link_asserter = LinkAsserter(parent)
        pattern = "l.*l"

        # Act
        result = link_asserter.matching_label(pattern)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLabel)
        assert_that(result.context.reconstruct_buffer()).is_equal_to("rule1 -->|link label| rule2")

    def test_with_from_adds_WithLinkStart_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter("first line\nrule1 --> rule2")
        link_asserter = LinkAsserter(parent)
        id = "rule1"

        # Act
        result = link_asserter.with_from(id)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLinkStart)
        assert_that(result.context.reconstruct_buffer()).is_equal_to("rule1 --> rule2")

    def test_matching_from_adds_WithLinkStart_augment_with_regex_and_runs_match(self):
        # Arrange
        parent = RootAsserter("first line\nrule1 --> rule2")
        link_asserter = LinkAsserter(parent)
        pattern = "r.*1"

        # Act
        result = link_asserter.matching_from(pattern)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLinkStart)
        assert_that(result.context.reconstruct_buffer()).is_equal_to("rule1 --> rule2")

    def test_with_to_adds_WithLinkEnd_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter("first line\nrule1 --> rule2")
        link_asserter = LinkAsserter(parent)
        id = "rule2"

        # Act
        result = link_asserter.with_to(id)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLinkEnd)
        assert_that(result.context.reconstruct_buffer()).is_equal_to("rule1 --> rule2")

    def test_matching_to_adds_WithLinkEnd_augment_with_regex_and_runs_match(self):
        # Arrange
        parent = RootAsserter("first line\nrule1 --> rule2")
        link_asserter = LinkAsserter(parent)
        pattern = "r.*2"

        # Act
        result = link_asserter.matching_to(pattern)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLinkEnd)
        assert_that(result.context.reconstruct_buffer()).is_equal_to("rule1 --> rule2")

    def test_with_style_adds_WithLinkStyle_augment_and_runs_match(self):
        # Arrange
        parent = RootAsserter("first line\nrule1 --> rule2")
        link_asserter = LinkAsserter(parent)
        style = LINK_STYLE_NORMAL

        # Act
        result = link_asserter.with_style(style)

        # Assert
        assert_that(result.augments).is_not_empty()
        assert_that(result.augments[0]).is_type_of(WithLinkStyle)
        assert_that(result.context.reconstruct_buffer()).is_equal_to("rule1 --> rule2")


class TestMockAsserter:
    def test_match_always_returns_false(self):
        # Arrange
        mock_asserter = ContinuationAsserter(RootAsserter(), RootAsserter())

        # Act
        result = mock_asserter.match()

        # Assert
        assert_that(result).is_false()

    def test_message_returns_custom_message(self):
        # Arrange
        message = "Custom message"
        mock_asserter = mock_asserter = ContinuationAsserter(
            RootAsserter(), RootAsserter(), message
        )

        # Act
        result = mock_asserter.message()

        # Assert
        assert_that(result).is_equal_to(message)

    def test_extract_context_returns_target_context(self):
        # Arrange
        target = RootAsserter("line 1\nline 2\nline 3")
        mock_asserter = ContinuationAsserter(RootAsserter(), target)

        # Act
        extracted_context = mock_asserter.extract_context(target.context)

        # Assert
        assert_that(extracted_context).is_equal_to(target.context)

    def test_getattr_returns_wrapped_method_that_mocks_as_target_type(self):
        # Arrange
        target = LineAsserter(RootAsserter("\n\n"), "")
        target.match()
        mock_asserter = ContinuationAsserter(RootAsserter(), target)

        # Act
        result = mock_asserter.first

        # Assert
        assert callable(result)
        asserter = result()
        assert_that(asserter).is_instance_of(FirstAsserter)
        assert_that(asserter.parent).is_equal_to(mock_asserter)


class TestContinuationAsserter:
    # When using and_then asserter (A -> B -> and_then -> C), asserter C should use context of A
    # starting after B
    def test_subasserter_resumes_matching_aster_parent_match(self):
        # Arrange
        root = RootAsserter(sample_graph)
        asserter = GraphAsserter(root, direct_descendants_only=True)
        sut = GraphAsserter(asserter.and_then, direct_descendants_only=True)

        # Act
        asserter.match()
        sut.match()

        # Assert
        assert_that(asserter.context.reconstruct_buffer()).starts_with("subgraph A")
        assert_that(sut.context.reconstruct_buffer()).starts_with("subgraph B")

    # When using and_then asserter (A -> B -> and_then -> C) and C fails, then it should force to
    # reevaluate B and continue matching from next line (after current B)
    def test_when_subasserter_fails_it_should_force_asserter_to_reevaluate_from_its_current_position_and_continue_matching(
        self,
    ):
        # Arrange
        root = RootAsserter(sample_graph)
        asserter = GraphAsserter(root, direct_descendants_only=True)
        sut = GraphAsserter(asserter.and_then, direct_descendants_only=True)

        # Act
        asserter.match()
        with patch.object(sut, "_next_match", side_effect=[False, True]):
            sut.match()

        # Assert
        assert_that(asserter.context.reconstruct_buffer()).starts_with("subgraph B")
        assert_that(sut.context.reconstruct_buffer()).starts_with("subgraph C")

    def test_when_subasserter_fails_message_should_be_joined_correctly(
        self,
    ):
        # Arrange
        root = RootAsserter(sample_graph)
        asserter = GraphAsserter(root, direct_descendants_only=True)
        sut = LineAsserter(asserter.and_then, "B4")

        # Act
        assert_that(sut.match).raises(AssertionFailure).when_called_with().contains(
            "subgraph followed by line that matches"
        )


class TestFirstAsserter:
    def test_instantiation_with_valid_arguments(self):
        # Arrange
        parent = RootAsserter("line")

        # Act
        first_asserter = FirstAsserter(parent)

        # Assert
        assert_that(first_asserter).is_not_none()
        assert_that(first_asserter.parent).is_equal_to(parent)

    def test_message_method_returns_expected_message(self):
        # Arrange
        parent = RootAsserter("line")

        # Act
        first_asserter = FirstAsserter(parent)

        # Assert
        assert_that(first_asserter.message()).is_equal_to("[first element]")

    def test_match_when_match_exists_always_returns_first_match_as_context(self):
        # Arrange
        root = RootAsserter("line 1\nline 2\nline 3")
        parent = LineAsserter(root, "line")
        sut = FirstAsserter(parent)

        # Act
        sut.match()
        result = sut.match()

        # Assert
        assert_that(result).is_true()
        assert_that(sut.context.reconstruct_buffer()).is_equal_to("line 1")

    # With A -> B -> first -> C, when C fails (rematch parent is called) then B should force to re-evaluate A for next section
    def test_match_when_subasserter_fails_it_resets_parent_to_next_location(self):
        # Arrange
        root = GraphAsserter(RootAsserter(sample_graph), direct_descendants_only=True)
        parent = GraphAsserter(root, direct_descendants_only=True)
        sut = parent.first()
        sub = LineAsserter(sut, "B1")

        # Act
        sub.match()
        result = sut.match()

        # Assert
        assert_that(result).is_true()
        assert_that(sut.context.reconstruct_buffer().lstrip()).starts_with("subgraph B1")


class TestLastAsserter:
    def test_instantiation_with_valid_arguments(self):
        # Arrange
        parent = RootAsserter("line")

        # Act
        sut = LastAsserter(parent)

        # Assert
        assert_that(sut).is_not_none()
        assert_that(sut.parent).is_equal_to(parent)

    def test_message_method_returns_expected_message(self):
        # Arrange
        parent = RootAsserter("line")

        # Act
        sut = LastAsserter(parent)

        # Assert
        assert_that(sut.message()).is_equal_to("[last element]")

    def test_match_when_match_exists_always_returns_first_match_as_context(self):
        # Arrange
        root = RootAsserter("line 1\nline 2\nline 3")
        parent = LineAsserter(root, "line")
        sut = LastAsserter(parent)

        # Act
        sut.match()
        result = sut.match()

        # Assert
        assert_that(result).is_true()
        assert_that(sut.context.reconstruct_buffer()).is_equal_to("line 3")

    # With A -> B -> first -> C, when C fails (rematch parent is called) then B should force to re-evaluate A for next section
    def test_match_when_subasserter_fails_it_resets_parent_to_next_location(self):
        # Arrange
        root = GraphAsserter(RootAsserter(sample_graph), direct_descendants_only=True)
        parent = GraphAsserter(root, direct_descendants_only=True)
        sut = parent.last()
        sub = LineAsserter(sut, "B2")

        # Act
        sub.match()
        result = sut.match()

        # Assert
        assert_that(result).is_true()
        assert_that(sut.context.reconstruct_buffer().lstrip()).starts_with("subgraph B2")


class TestExactCountAsserter:
    def test_matches_exactly_expected_count(self):
        # Arrange
        parent = LineAsserter(RootAsserter(multi_line_text), "This is")
        parent.match()
        sut = ExactCountAsserter(parent, 6)

        # Assert
        assert_that(sut.match()).is_true()

    def test_matches_when_count_is_1(self):
        # Arrange
        parent = LineAsserter(RootAsserter(multi_line_text), "This is line 1")
        parent.match()
        sut = ExactCountAsserter(parent, 1)

        # Assert
        assert_that(sut.match()).is_true()

    @pytest.mark.parametrize("count", [-1, -2, 0, 1, 2, 5, 7])
    def test_fails_when_count_not_equal_to_number_of_matches(self, count):
        # Arrange
        parent = LineAsserter(RootAsserter(multi_line_text), "This is")
        parent.match()
        exact_count_asserter = ExactCountAsserter(parent, count)

        # Assert
        assert_that(exact_count_asserter.match(soft_fail=True)).is_false()

    def test_when_combined_with_augments_expect_correct_count(self):
        # Arrange
        parent = GraphAsserter(RootAsserter(sample_graph))

        # Act
        sut = parent.with_id("B").count(1)

        # Assert
        assert_that(sut.match()).is_true()


class TestMinCountAsserter:
    def test_match_with_min_count(self):
        # Arrange
        parent = LineAsserter(RootAsserter(multi_line_text), "line")
        sut = MinCountAsserter(parent, 2)

        # Act
        result = sut.match()

        # Assert
        assert_that(result).is_true()

    def test_match_with_less_than_min_count(self):
        # Arrange
        parent = LineAsserter(RootAsserter(multi_line_text), "line")
        sut = MinCountAsserter(parent, 10)

        # Act
        result = sut.match(soft_fail=True)

        # Assert
        assert_that(result).is_false()


class TestMaxCountAsserter:
    def test_match_with_min_count(self):
        # Arrange
        parent = LineAsserter(RootAsserter(multi_line_text), "line")
        sut = MaxCountAsserter(parent, 2)

        # Act
        result = sut.match(soft_fail=True)

        # Assert
        assert_that(result).is_false()

    def test_match_with_less_than_min_count(self):
        # Arrange
        parent = LineAsserter(RootAsserter(multi_line_text), "line")
        sut = MaxCountAsserter(parent, 10)

        # Act
        result = sut.match(soft_fail=True)

        # Assert
        assert_that(result).is_true()

    def test_parent_exists_and_is_None(self):
        # Arrange
        sut = RootAsserter("line")

        # Act
        result = hasattr(sut, "parent")

        # Assert
        assert_that(result).is_true()
        assert_that(sut.parent).is_none()


class TestCountableAsserter:
    @pytest.mark.parametrize("method", ["first", "last", "once"])
    def test_when_continued_uses_mock_as_self(self, method: str):
        # Arrange
        parent = RootAsserter("line")
        asserter = ContinuationAsserter(parent, LineAsserter(parent, "line"))

        # Act
        sut = asserter.__getattr__(method)()

        # Assert
        assert_that(sut.parent).is_not_none().is_instance_of(ContinuationAsserter)

    @pytest.mark.parametrize(
        "method", ["times", "count", "exactly", "min", "at_least", "max", "at_most"]
    )
    def test_parameterized_continuation_when_continued_uses_mock_as_self(self, method: str):
        # Arrange
        parent = RootAsserter("line")
        asserter = ContinuationAsserter(parent, LineAsserter(parent, "line"))

        # Act
        sut = asserter.__getattr__(method)(1)

        # Assert
        assert_that(sut.parent).is_not_none().is_instance_of(ContinuationAsserter)
