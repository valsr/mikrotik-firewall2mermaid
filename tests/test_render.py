import pytest
from firewall2mermaid.common import (
    LINK_STYLE_BETWEEN_SECTIONS,
    LINK_STYLE_INVISIBLE,
    LINK_STYLE_NORMAL,
)

from firewall2mermaid.model import Chain, GraphSelector, Link, RootNode, Rule, Section
from firewall2mermaid.render import Renderer
from firewall2mermaid.render.common import determine_link_label, determine_rule_label
from firewall2mermaid.render.model import LinkNode, RuleNode
from firewall2mermaid.tree import TreeMaker
from tests.assertions import AssertionFailure, assert_graph
from assertpy import assert_that


class TestBasicRendering:
    def test_does_not_include_extra_newlines(self):
        sut = Renderer(RootNode(), [])

        graph = sut.render_graph()

        assert_that(graph).starts_with("%%{init: ")
        assert_that(graph).does_not_contain("\n\n\n\n")

    def test_renders_graph_header(self):
        sut = Renderer(RootNode(), [])

        graph = sut.render_graph()

        assert_graph(graph).has_line(
            "%%{init: {'flowchart': {'htmlLabels': false}, 'theme': 'dark'}}%%"
        ).and_then.has_line("flowchart TB")

    def test_renders_rules_as_logs(self, default_tree_root):
        sut = Renderer(default_tree_root, [], log_rules=True)

        graph = sut.render_graph()

        assert_graph(graph).has_line("%% /ip firewall").exactly(32)

    def test_renders_root_graph_section(self, default_tree_root):
        sut = Renderer(default_tree_root, [])

        graph = sut.render_graph()

        assert_graph(graph).has_graph("root").with_direction("TB").at_depth(0)

    @pytest.mark.parametrize(
        "style", ["fasttrack-connection", "accept", "disabled", "log", "jump", "drop"]
    )
    def test_renders_styles(self, default_tree_root, style):
        sut = Renderer(default_tree_root, [])

        graph = sut.render_graph()

        assert_graph(graph).has_style(style)

    def test_renders_relationships_at_end_of_graph(self, default_tree_root):
        sut = Renderer(default_tree_root, [])

        graph = sut.render_graph()

        assert_graph(graph).has_graph("root").followed_by.has_line("%% Relationships")

    def test_dont_render_root_node_when_show_root_is_false(self, default_tree_root):
        sut = Renderer(default_tree_root, [], show_root=False)

        graph = sut.render_graph()

        assert_that(assert_graph(graph).has_graph).raises(AssertionFailure).when_called_with("root")

    def test_extra_logs_added_to_header(self, default_tree_root):
        sut = Renderer(default_tree_root, [], extra_logs=["log 1", "log 2"])

        graph = sut.render_graph()

        assert_that(graph).starts_with("%% log 1\n%% log 2")


class TestSplat:
    def test_splat_chains_rendered_on_their_own(self, default_tree_root):
        sut = Renderer(default_tree_root, [GraphSelector("filter:input", "*")], show_root=False)

        graph = sut.render_graph()

        assert_graph(graph).has_graph("filter:input")

    def test_splatted_chain_rendered_id_for_label(self, default_tree_root):
        sut = Renderer(default_tree_root, [GraphSelector("filter:input", "*")], show_root=False)

        graph = sut.render_graph()

        assert_graph(graph).has_graph("filter:input").with_label("filter:input")

    def test_splat_section_renders_each_chain_on_its_own(self, default_tree_root):
        sut = Renderer(default_tree_root, [GraphSelector("filter", "*")], show_root=False)

        graph = sut.render_graph()

        assert_graph(graph).has_graph("filter:input")
        assert_graph(graph).has_graph("filter:forward")
        assert_graph(graph).has_graph("filter:output")

    def test_splat_chains_add_legend_links(self, default_tree_root):
        sut = Renderer(default_tree_root, [GraphSelector("nat", "*")], show_root=False)

        graph = sut.render_graph()

        assert_graph(graph).has_link("legend").with_to("nat:dstnat").with_style(
            LINK_STYLE_INVISIBLE
        )

    def test_emplty_parent_link_removed_from_legend(self, default_tree_root):
        sut = Renderer(default_tree_root, [GraphSelector("nat", "*")], show_root=False)

        graph = sut.render_graph()

        assert_that(
            assert_graph(graph).has_link("legend").with_style(LINK_STYLE_INVISIBLE).with_to
        ).raises(AssertionFailure).when_called_with("nat")


class TestSelectors:
    def test_multiple_mods_per_selector(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat:dstnat", mod="*%+")])

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph("root").has_rule().with_label(
            "nat:dstnat *collapsed"
        ).with_style("collapsed")

    def test_multiple_selector_picks_first_one(self, default_tree_root):
        # Arrange
        sut = Renderer(
            default_tree_root,
            [GraphSelector("nat:dstnat", "+"), GraphSelector("nat:dstnat", "-")],
        )

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph("root").has_graph("nat").has_graph("nat:dstnat")

    def test_global_selector_does_not_impact_others(self, default_tree_root):
        # Arrange
        sut = Renderer(
            default_tree_root,
            [
                GraphSelector("nat:dstnat", "+"),
                GraphSelector("nat", "+"),
                GraphSelector("", "%"),
            ],
        )

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph("root").has_graph("nat").has_graph("nat:dstnat")


class TestLegendRendering:
    def test_adds_legend_to_root(self, default_tree_root):
        sut = Renderer(default_tree_root, [], show_legend=True)

        graph = sut.render_graph()

        assert_that(graph).starts_with("%%{init: ")
        assert_graph(graph).has_graph("root").has_graph(
            "legend", direct_descendants_only=False
        ).with_label("Legend")

    def test_when_legend_is_false_do_not_render_legend(self, default_tree_root):
        sut = Renderer(default_tree_root, [], show_legend=False)

        graph = sut.render_graph()

        assert_that(graph).does_not_contain("subgraph legend-container")
        assert_that(graph).does_not_contain("subgraph legend")

    @pytest.mark.parametrize(
        "style", ["fasttrack-connection", "accept", "disabled", "log", "jump", "drop"]
    )
    def test_legend_has_rules_for_each_style(self, default_tree_root, style):
        sut = Renderer(default_tree_root, [], show_legend=True)

        graph = sut.render_graph()

        assert_graph(graph).has_graph("root").has_graph("legend").has_rule().with_label(
            style
        ).with_style(style).at_depth(2)

    def test_when_rootless_when_no_subgraphs_add_legend_before_first_rule_graph(
        self, default_tree_root
    ):
        sut = Renderer(
            default_tree_root, [GraphSelector(":", "*%")], show_legend=True, show_root=False
        )

        graph = sut.render_graph()

        assert_that(graph).starts_with("%%{init: ")
        assert_graph(graph).has_graph("legend").with_label("Legend").has_rule()


class TestSectionRendering:
    @pytest.mark.parametrize("section", ["filter", "nat", "raw", "mangle"])
    def test_renders_all_sections(self, default_tree_root, section):
        sut = Renderer(default_tree_root, [])

        graph = sut.render_graph()

        assert_graph(graph).has_graph("root").has_graph(section).with_label(section).with_direction(
            "TB"
        ).at_depth(1)


class TestChainRendering:
    @pytest.mark.parametrize(
        "section, chain, label",
        [
            ("raw", "raw:prerouting", "prerouting"),
            ("raw", "raw:output", "output"),
            ("raw", "raw:jump-chain", "jump-chain"),
            ("mangle", "mangle:prerouting", "prerouting"),
            ("mangle", "mangle:input", "input"),
            ("mangle", "mangle:forward", "forward"),
            ("mangle", "mangle:output", "output"),
            ("mangle", "mangle:postrouting", "postrouting"),
            ("nat", "nat:dstnat", "dstnat"),
            ("nat", "nat:srcnat", "srcnat"),
            ("filter", "filter:input", "input"),
            ("filter", "filter:output", "output"),
            ("filter", "filter:forward", "forward"),
            ("filter", "filter:terminal-chain", "terminal-chain"),
            ("filter", "filter:jump-chain", "jump-chain"),
            ("filter", "filter:disabled-chain", "disabled-chain"),
            ("filter", "filter:log-chain", "log-chain"),
            ("filter", "filter:rule-order", "rule-order"),
        ],
    )
    def test_renders_all_graph_chains(self, default_tree_root, section, chain, label):
        sut = Renderer(default_tree_root, [])

        graph = sut.render_graph()

        assert_graph(graph).has_graph("root").has_graph(section).has_graph(chain).with_label(
            label
        ).with_direction("TB").at_depth(2)


class TestRuleStyleRendering:
    def test_render_stylized_rule(self):
        sut = RuleNode("id", "a label", "style")

        result = sut.render()
        assert_that(result).contains(":::style")

    def test_render_non_stylized_rule(self):
        sut = RuleNode("id", "a label")

        result = sut.render()
        assert_that(result).does_not_contain(":::")


class TestRuleLabelRendering:
    @pytest.mark.parametrize("action", ["unknown", "", None])
    def test_return_id_as_label_for_unrecognized_action_rule(self, action):
        rule = Rule("rule line", 1, {"action": action})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("detached:detached:rule1")

    @pytest.mark.parametrize("action", ["log", "drop", "accept", "return"])
    def test_return_action_as_label_for_recognized_action_rule(self, action):
        rule = Rule("rule line", 1, {"action": action})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to(action)

    def test_return_label_for_jump_action_rule(self):
        rule = Rule("rule line", 1, {"action": "jump", "jump_target": "jump-target"})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("jump to jump-target")

    @pytest.mark.parametrize("action", ["add-src-to-address-list", "add-dst-to-address-list"])
    def test_return_label_for_add_to_list_rule(self, action):
        rule = Rule("rule line", 1, {"action": action, "address_list": "address-list"})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("add ip to 'address-list'")

    @pytest.mark.parametrize("action", ["add-src-to-address-list", "add-dst-to-address-list"])
    def test_return_label_for_add_to_list_rule_with_timeout(self, action):
        rule = Rule(
            "rule line",
            1,
            {"action": action, "address_list": "address-list", "address_list_timeout": "1d"},
        )
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("add ip to 'address-list' for 1d")

    def test_return_comment_for_comment_only_rule(self):
        rule = Rule("rule line", 1, {"comment": "comment"})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("comment")

    def test_return_log_when_rule_has_log_append(self):
        rule = Rule("rule line", 1, {"action": "accept", "log": "yes", "log_prefix": "log-prefix"})
        label = determine_rule_label(rule)
        assert_that(label).ends_with('!>{log-prefix}"')

    def test_return_question_mark_when_rule_has_multiple_links_and_property_matchers(
        self,
    ):
        rule = Rule("rule line", 1, {"protocol": "tcp", "dst_port": "80", "src_port": "80"})
        rule.add_link(rule)
        rule.add_link(rule)
        label = determine_rule_label(rule)
        assert_that(label).ends_with("?")

    def test_return_packet_information_when_rule_has_packet_information(self):
        rule = Rule("rule line", 1, {"protocol": "tcp", "tcp_flags": "syn", "dst_limit": 10})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("is tcp,syn,below 10")

    def test_return_connection_information_when_rule_has_connection_information(self):
        rule = Rule("rule line", 1, {"connection_state": "new"})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("connection is new")

    def test_return_src_information_when_rule_has_src_information(self):
        rule = Rule("rule line", 1, {"src_address": "192.168.0.0", "src_port": "80"})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("src is 192.168.0.0:80")

    def test_return_src_information_when_rule_has_src_information_with_port(self):
        rule = Rule("rule line", 1, {"src_address": "192.168.0.0", "port": "80"})
        label = determine_rule_label(rule)
        assert_that(label).contains("src is 192.168.0.0:*80")

    def test_return_src_information_when_rule_has_src_information_with_in_interface(self):
        rule = Rule(
            "rule line", 1, {"src_address": "192.168.0.0", "src_port": "80", "in_interface": "eth1"}
        )
        label = determine_rule_label(rule)
        assert_that(label).contains("src is 192.168.0.0:80 on eth1")

    def test_return_src_information_when_rule_has_src_information_with_in_bridge_port(
        self,
    ):
        rule = Rule(
            "rule line", 1, {"src_address": "192.168.0.0", "src_port": "80", "in_bridge_port": 80}
        )
        label = determine_rule_label(rule)
        assert_that(label).contains("src is 192.168.0.0:80 on :80")

    def test_return_dst_information_when_rule_has_dst_information(self):
        rule = Rule("rule line", 1, {"dst_address": "192.168.0.0", "dst_port": "80"})
        label = determine_rule_label(rule)
        assert_that(label).is_equal_to("dst is 192.168.0.0:80")

    def test_return_dst_information_when_rule_has_dst_information_with_dst_port(self):
        rule = Rule("rule line", 1, {"dst_address": "192.168.0.0", "port": "80"})
        label = determine_rule_label(rule)
        assert_that(label).contains("dst is 192.168.0.0:*80")

    def test_return_dst_information_when_rule_has_dst_information_with_out_interface(self):
        rule = Rule(
            "rule line",
            1,
            {"dst_address": "192.168.0.0", "dst_port": "80", "out_interface": "eth1"},
        )
        label = determine_rule_label(rule)
        assert_that(label).contains("dst is 192.168.0.0:80 on eth1")

    def test_return_dst_information_when_rule_has_dst_information_with_out_bridge_port(self):
        rule = Rule(
            "rule line", 1, {"dst_address": "192.168.0.0", "dst_port": "80", "out_bridge_port": 80}
        )
        label = determine_rule_label(rule)
        assert_that(label).contains("dst is 192.168.0.0:80 on :80")

    @pytest.mark.parametrize("comment", ["[", "]", "{", "}", "(", ")"])
    def test_quote_label_when_label_includes_special_characters(self, comment):
        rule = Rule("rule line", 1, {"action": "accept", "comment": comment})
        label = determine_rule_label(rule)
        assert_that(label).starts_with('"').ends_with('"')

    @pytest.mark.parametrize(
        "mode,expected_value",
        [
            ("show", "this is a comment"),
            ("hide", "accept"),
            ("auto", "this is a comment"),
            ("prefer", "this is a comment"),
        ],
    )
    def test_comment_handling_for_fallback_label(self, mode, expected_value):
        # Arrange
        rule = Rule("rule line", 1, {"action": "accept", "comment": "this is a comment"})

        # Act
        label = determine_rule_label(rule, mode)

        # Assert
        assert_that(label).is_equal_to(expected_value)

    @pytest.mark.parametrize(
        "mode,expected_value",
        [
            ("show", "is tcp *this is a comment"),
            ("hide", "is tcp"),
            ("auto", "is tcp"),
            ("prefer", "this is a comment"),
        ],
    )
    def test_comment_handling_for_expected_label(self, mode, expected_value):
        # Arrange
        rule = Rule(
            "rule line", 1, {"action": "accept", "protocol": "tcp", "comment": "this is a comment"}
        )

        # Act
        label = determine_rule_label(rule, mode)

        # Assert
        assert_that(label).is_equal_to(expected_value)


class TestRuleRendering:
    def test_rules_rendered_in_graph(self, rule_list_ordered_tests):
        tree = TreeMaker(rule_list_ordered_tests, []).make_tree()

        graph = Renderer(tree, [], show_legend=False).render_graph()

        assert_graph(graph).has_graph("root").has_rule().exactly(6)


class TestLinkRendering:
    def test_renders_link_with_label(self):
        parent = Chain("chain")
        from_rule = Rule("", 1, attributes={"action": "accept"})
        to_rule = Rule("", 2, attributes={"action": "accept"})
        parent.add_rule(from_rule)
        parent.add_rule(to_rule)
        link = Link(from_rule, to_rule)
        link_node = LinkNode(link)

        link.label = "label"

        result = link_node.render(0)

        assert_that(result).is_equal_to(
            f"detached:chain:rule1 {LINK_STYLE_NORMAL}|label| detached:chain:rule2"
        )

    def test_renders_link_without_label(self):
        parent = Chain("chain")
        from_rule = Rule("", 1, attributes={"action": "accept"})
        to_rule = Rule("", 2, attributes={"action": "accept"})
        parent.add_rule(from_rule)
        parent.add_rule(to_rule)
        link = Link(from_rule, to_rule)
        link_node = LinkNode(link)

        result = link_node.render(0)

        assert_that(result).is_equal_to(
            f"detached:chain:rule1 {LINK_STYLE_NORMAL} detached:chain:rule2"
        )

    def test_render_empty_string_when_render_is_false(self):
        parent = Chain("chain")
        from_rule = Rule("", 1, attributes={"action": "accept"})
        to_rule = Rule("", 2, attributes={"action": "accept"})
        parent.add_rule(from_rule)
        parent.add_rule(to_rule)
        link = Link(from_rule, to_rule)
        link.render = False
        link_node = LinkNode(link)

        result = link_node.render(0)

        assert_that(result).is_empty()

    def test_render_link_with_specified_style(self):
        parent = Chain("chain")
        from_rule = Rule("", 1, attributes={"action": "accept"})
        to_rule = Rule("", 2, attributes={"action": "accept"})
        parent.add_rule(from_rule)
        parent.add_rule(to_rule)
        link = Link(from_rule, to_rule)
        link_node = LinkNode(link)

        link.style = "style"

        result = link_node.render(0)

        assert_that(result).is_equal_to("detached:chain:rule1 style detached:chain:rule2")

    def test_render_intersectional_links_with_invisible_style(self):
        from_rule = Rule("", 1, attributes={"action": "accept"})
        to_rule = Rule("", 2, attributes={"action": "accept"})

        link = Link(from_rule, to_rule)
        link_node = LinkNode(link)

        result = link_node.render(0)

        assert_that(result).is_equal_to(
            f"detached:detached:rule1 {LINK_STYLE_BETWEEN_SECTIONS}|to detached:detached| detached:detached:rule2"
        )

    @pytest.mark.parametrize(
        "match, count", [("raw:.*", 4), ("mangle:.*", 12), ("nat:.*", 4), ("filter:.*", 19)]
    )
    def test_ensure_all_links_are_rendered(self, default_tree_root, match, count):
        sut = Renderer(default_tree_root, [])

        graph = sut.render_graph()

        assert_graph(graph).has_link().matching_from(match).exactly(count)

    def test_renders_legend_links(self, default_tree_root):
        sut = Renderer(default_tree_root, [])

        graph = sut.render_graph()

        assert_graph(graph).has_link("legend").with_to("raw").with_style(LINK_STYLE_INVISIBLE)
        assert_graph(graph).has_link("legend").with_to("mangle").with_style(LINK_STYLE_INVISIBLE)
        assert_graph(graph).has_link("legend").with_to("nat").with_style(LINK_STYLE_INVISIBLE)
        assert_graph(graph).has_link("legend").with_to("filter").with_style(LINK_STYLE_INVISIBLE)


class TestCollapse:
    def test_adds_collapse_style(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat", mod="%")])

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_style("collapsed").and_then.has_graph()
        assert_graph(graph).has_graph(
            "legend", direct_descendants_only=False
        ).has_rule().with_label("*collapsed").at_depth(2)

    def test_replaces_collapsed_section_with_rule(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat", mod="%")])

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph("root").has_rule().with_label("nat *collapsed").with_style(
            "collapsed"
        )

    def test_repoints_links_involving_collapsed_section(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat", mod="%")])

        # Act
        graph = sut.render_graph()

        # Assert
        # mangle:prerouting:rule1 -.->|to dstnat| nat:dstnat:rule2
        assert_graph(graph).has_link("mangle:prerouting:rule1").with_label("to nat:dstnat").with_to(
            "nat:collapsedrule33"
        )
        # mangle:postrouting:rule19 -.->|to srcnat| nat:srcnat:rule20
        assert_graph(graph).has_link("mangle:postrouting:rule19").with_label(
            "to nat:srcnat"
        ).with_to("nat:collapsedrule33")
        # nat:dstnat:rule2 -.->|to input| filter:input:rule5
        assert_graph(graph).has_link("nat:collapsedrule33").with_label("to mangle:input").with_to(
            "mangle:input:rule3"
        )
        # nat:dstnat:rule2 -.->|to forward| filter:forward:rule11
        assert_graph(graph).has_link("nat:collapsedrule33").with_label("to mangle:forward").with_to(
            "mangle:forward:rule9"
        )

    def test_replaced_collapsed_chain_with_rule(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat:dstnat", mod="%")])

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_style("collapsed")
        assert_graph(graph).has_graph("root").has_graph("nat").has_rule().with_label(
            "dstnat *collapsed"
        ).with_style("collapsed")

    def test_splat_and_collapsed_chain_should_work_together(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat:dstnat", mod="*%")])

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph("root").has_rule().with_label(
            "nat:dstnat *collapsed"
        ).with_style("collapsed")

    def test_repoints_links_involving_collapsed_chain(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat:dstnat", mod="%")])

        # Act
        graph = sut.render_graph()

        # Assert
        # mangle:prerouting:rule1 -.->|to dstnat| nat:dstnat:rule2
        assert_graph(graph).has_link("mangle:prerouting:rule1").with_label("to nat:dstnat").with_to(
            "nat:dstnat:collapsedrule33"
        )
        # nat:dstnat:rule2 -.->|to input| filter:input:rule5
        assert_graph(graph).has_link("nat:dstnat:collapsedrule33").with_label(
            "to mangle:input"
        ).with_to("mangle:input:rule3")
        # nat:dstnat:rule2 -.->|to forward| filter:forward:rule11
        assert_graph(graph).has_link("nat:dstnat:collapsedrule33").with_label(
            "to mangle:forward"
        ).with_to("mangle:forward:rule9")

    def test_repoints_links_when_when_both_from_and_to_are_collapsed(self, default_rule_list):
        # Arrange
        selector = [
            GraphSelector("mangle:prerouting", mod="*%"),
            GraphSelector("nat:dstnat", mod="*%"),
            GraphSelector(":", mod="-"),
        ]

        tree = TreeMaker(default_rule_list, selector).make_tree()
        sut = Renderer(tree, selector)

        # Act
        graph = sut.render_graph()

        # Assert
        # mangle:prerouting -.->|to dstnat| nat:dstnat
        assert_graph(graph).has_link().matching_from("mangle:prerouting").matching_to("nat:dstnat")

    def test_links_to_non_collapsed_sister_chain_not_modified(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat:dstnat", mod="%")])

        # Act
        graph = sut.render_graph()

        # Assert
        # mangle:postrouting:rule19 -.->|to srcnat| nat:srcnat:rule20
        assert_graph(graph).has_link("mangle:postrouting:rule19").with_label(
            "to nat:srcnat"
        ).with_to("nat:srcnat:rule20")

    def test_drop_links_from_and_to_collapsed_node(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("filter:forward", mod="%")])

        # Act
        graph = sut.render_graph()

        # Assert
        with pytest.raises(AssertionFailure):
            assert_graph(graph).has_link("filter:forward:collapsedrule").with_to(
                "filter:forward:collapsedrule"
            )


class TestCollapseAsRule:
    def test_adds_collapse_style(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("nat", mod="%")])

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_style("collapsed").and_then.has_graph()
        assert_graph(graph).has_graph(
            "legend", direct_descendants_only=False
        ).has_rule().with_label("*collapsed").at_depth(2)

    def test_replaces_collapsed_section_with_rule(self, default_tree_root):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("filter", mod="%")], collapse_as_rule=True)

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph("root").has_graph("mangle").has_rule().matching_label(
            r"filter.*collapsed"
        ).with_style("collapsed").count(5)

    def test_ensure_collapse_rule_inserted_before_link_end_target_when_it_is_start_of_link(
        self, default_tree_root
    ):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("filter", mod="%")], collapse_as_rule=True)

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph("mangle:postrouting", direct_descendants_only=False).has_rule(
            "filter:collapsedrule36"
        ).and_then.has_rule("mangle:postrouting:rule19")

    def test_ensure_collapse_rule_inserted_after_link_start_target_when_it_is_end_of_link(
        self, default_tree_root
    ):
        # Arrange
        sut = Renderer(default_tree_root, [GraphSelector("filter", mod="%")], collapse_as_rule=True)

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph("root").has_graph(
            "mangle:input", direct_descendants_only=False
        ).has_rule("mangle:input:rule3").followed_by.has_rule("filter:collapsedrule33")

    def test_rule_label_shows_chain_name_when_collapsed(self, default_tree_root):
        # Arrange
        sut = Renderer(
            default_tree_root, [GraphSelector("filter:", mod="%")], collapse_as_rule=True
        )

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph(direct_descendants_only=False).has_rule().with_label(
            "filter:forward *collapsed"
        )
        assert_graph(graph).has_graph(direct_descendants_only=False).has_rule().with_label(
            "filter:input *collapsed"
        )
        assert_graph(graph).has_graph(direct_descendants_only=False).has_rule().with_label(
            "filter:output *collapsed"
        )

    def test_replace_collapsed_chain_with_rules(self, default_tree_root):
        # Arrange
        sut = Renderer(
            default_tree_root,
            [GraphSelector("filter:forward", mod="%")],
            collapse_as_rule=True,
        )

        # Act
        graph = sut.render_graph()

        # Assert
        assert_graph(graph).has_graph(
            "mangle:postrouting", direct_descendants_only=False
        ).has_rule().with_label("filter:forward *collapsed").with_style(
            "collapsed"
        ).followed_by.has_rule().with_label(
            "fasttrack"
        )

        assert_graph(graph).has_graph(
            "mangle:forward", direct_descendants_only=False
        ).has_rule().with_label("disabled rule").followed_by.has_rule().with_label(
            "filter:forward *collapsed"
        ).with_style(
            "collapsed"
        )

        assert_graph(graph).has_graph(
            "filter:jump-chain", direct_descendants_only=False
        ).has_rule().with_label("filter:forward *collapsed").with_style(
            "collapsed"
        ).followed_by.has_rule().followed_by.has_rule().with_label(
            "filter:forward *collapsed"
        ).with_style(
            "collapsed"
        )

        assert_graph(graph).has_graph(
            "filter:terminal-chain", direct_descendants_only=False
        ).has_rule().with_label("filter:forward *collapsed").with_style(
            "collapsed"
        ).followed_by.has_rule()


class TestLinkLabel:
    def test_label_uses_combined_parent_names_when_jumps_to_chain_in_different_section(self):
        # Arrange
        section = Section("section")
        chain = section.get_or_add_chain("chain")
        rule = Rule("", 0, {})

        # Act
        label = determine_link_label(Link(rule, chain))

        # Assert
        assert_that(label).is_equal_to("to section:chain")

    def test_label_uses_combined_parent_names_when_jumps_to_rule_of_different_section(self):
        # Arrange
        section = Section("section")
        chain = section.get_or_add_chain("chain")
        rule = Rule("", 0, {})
        end_rule = Rule("", 1, {})
        chain.add_rule(end_rule)

        # Act
        label = determine_link_label(Link(rule, end_rule))

        # Assert
        assert_that(label).is_equal_to("to section:chain")
