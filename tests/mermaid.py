import logging
import re
from pytest import Parser

CHART_TYPES = ["flowchart"]


class AstParsingException(BaseException):
    def __init__(self, message: str):
        self.message = f"Parsing exception: {message}"
        super().__init__(self.message)


class Node:
    def __init__(self):
        self.type = type(self)
        self.children: list[Node] = []

    def __str__(self):
        return f"[{self.type}]"

    def add_child(self, node):
        self.children.append(node)
        return node


class CommentNode(Node):
    def __init__(self, comment: str, multiline=False):
        self.comment = comment
        self.multiline = multiline

    def __str__(self):
        text = self.comment.replace("\n", "")
        return f"[{self.type}=>multiline: {str(self.multiline)}, comment: {text}]"


class RootNode(Node):
    pass


class EmptyNode(Node):
    pass


class ChartTypeNode(Node):
    def __init__(self, type: str = "flowchart", direction: str | None = None):
        self.direction = direction
        self.chart_type = type

    def __str__(self):
        text = f"[{self.type}=>type: {self.chart_type}"
        if self.direction:
            text += f", direction: {self.direction}"
        return text + "]"


class GraphNode(Node):
    def __init__(self, id: str, label: str | None = None):
        self.id = id
        self.label = label

    def __str__(self):
        text = f"[{self.type}=>type: {self.id}"
        if self.label:
            text += f", label: {self.label}"
        return text + "]"


class ClassDefNode(Node):
    def __init__(self, name: str, styles: dict[str, str]):
        self.name = name
        self.styles = styles

    def __str__(self):
        styles = ", ".join([f"{key}:{value}" for key, value in self.styles.items()])
        if styles:
            styles = ", " + styles
        return f"[{self.type}=>{self.name}{styles}]"


class DirectionNode(Node):
    def __init__(self, direction: str):
        self.direction = direction

    def __str__(self):
        return f"[{self.type}=>direction: {self.direction}]"


class EndNode(Node):
    pass


class RuleNode(Node):
    def __init__(self, id: str, label: str | None, classname: str | None):
        self.id = id
        self.label = label
        self.classname = classname

    def __str__(self):
        text = f"[{self.type}=>id: {self.id}"
        if self.label:
            text += f", label: {self.label}"
        if self.classname:
            text += f", classname: {self.classname}"
        return text + "]"


class RelationshipNode(Node):
    def __init__(self, start: str, end: str, label: str | None, style: str):
        self.start = start
        self.label = label
        self.style = style
        self.end = end

    def __str__(self):
        text = f"[{self.type}=>form: {self.start}, to: {self.end}, style: {self.style}"
        if self.label:
            text += f", label: {self.label}"
        return text + "]"


class ParserContext:
    def __init__(self, text):
        self.text = text
        self.index = 0
        self.lines = text.splitlines()

    def current(self):
        return self.lines[self.index]

    def next(self):
        if not self.has_next():
            return None
        self.index += 1
        return self.current()

    def peek(self):
        if not self.has_next():
            return None
        return self.lines[self.index + 1]

    def has_next(self):
        return self.index < len(self.lines)


class MermaidAstParser:
    def parse(self, text: str):
        context = ParserContext(text)
        root = RootNode()
        for node in self.parse_node(context):
            root.add_child(node)
        return root

    def parse_node(self, context: ParserContext):
        node = None
        if context.current():
            line = context.current().lstrip()
            if line.startswith("%%"):
                node = self.parse_comment(context)
            elif line.startswith(CHART_TYPES):
                node = self.parse_chart_type_node(context)
            elif line.startswith("subgraph"):
                node = self.parse_graph_node(context)
            elif line == "end":
                node = EndNode()
            elif line.startswith("direction"):
                node = self.parse_direction_node(context)
            elif line.startswith("rule"):
                node = self.parse_rule_node(context)
            elif not line:
                node = EmptyNode()

        logging.debug(f"Parsed node: {node}")
        # advance the context
        context.next()
        yield node

    def parse_comment(self, context: ParserContext):
        is_multi = context.current().lstrip().startswith("%%{")
        text = context.current().lstrip()
        text = text[(2 + int(is_multi)) :]
        if is_multi:
            while not context.current().rstrip().endswith("}%%"):
                text += f"\n{context.next()}"
        return CommentNode(text, is_multi)

    def parse_chart_type_node(self, context: ParserContext) -> ChartTypeNode:
        line = context.current().lstrip()
        if line.contains(" "):
            type, direction = line.split(" ")
        else:
            type = line
            direction = None
        return ChartTypeNode(type, direction)

    def parse_class_def_node(self, context: ParserContext) -> ClassDefNode:
        m = re.match(r"classDef\s(?P<name>[\S]+)\s(?P<styles>.*);?", context.current())
        if not m:
            raise AstParsingException(f"Invalid class definition in{context.current()}")
        name = m.group("name")
        styles = dict(x.split(":") for x in m.group("styles").split(","))
        return ClassDefNode(name, styles)

    def parse_graph_node(self, context: ParserContext):
        text = context.current()
        while str.count('"', text) % 2 == 1 and context.has_next():
            text += f"\n{context.next()}"

        m = re.match(
            r"subgraph\s(?P<id>[^\[]+)(\[\"?)(?P<name>[^\"\]]*)(\"?\]+)",
            text,
            re.MULTILINE | re.IGNORECASE,
        )
        if not m:
            raise AstParsingException(f"Invalid class definition in{text}")
        return GraphNode(m.group("id"), m.group("name"))

    def parse_direction_node(self, context: ParserContext):
        _, direction = context.current().strip().split(" ")
        return DirectionNode(direction)

    def parse_rule_node(self, context: ParserContext):
        text = context.current()
        while str.count('"', text) % 2 == 1 and context.has_next():
            text += f"\n{context.next()}"

        m = re.match(
            r"rule(?P<id>\d+)(\[\"?(?P<label>[^\"\]]+)\"?])?(:::(?P<class>.*))?",
            context.current(),
            re.MULTILINE | re.IGNORECASE,
        )
        if not m:
            raise AstParsingException(f"Invalid rule in {context.current()}")
        return RuleNode(m.group("id"), m.group("label"), m.group("class"))

    # (?P<start>[^-=~\s<]+)\s*((?P<astart>[xo<])?(--|==|-.)(\s*\|?(?P<inlabel>[^-=.|>ox]+)?\|?)?(-*|=*|.+-)(?P<aend>[xo>])|~~~+)(\s*\|(?P<label>[^|]+)?\|)?\s*(?P<end>.+)$
