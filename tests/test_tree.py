from firewall2mermaid.common import LINK_STYLE_INVISIBLE
from firewall2mermaid.model import GraphSelector, ParserRule, RootNode, Rule
from firewall2mermaid.tree import TreeMaker
from assertpy import assert_that


def assert_links_to(tree: RootNode, from_id: str, to_node_id: str, style: str | None = None):
    node = next((x for x in tree.sections if x.id == from_id), None)
    if not node:
        node = next((x for x in tree.chains if x.id == from_id), None)
    if not node:
        node = next((x for x in tree.rules if x.id == from_id), None)
    assert node

    if style:
        assert_that([(link.end.id, link.style) for link in node.links]).described_as(
            f"chain {from_id} contains link to {to_node_id} with style {style}"
        ).contains((to_node_id, style))
    else:
        assert_that([link.end.id for link in node.links]).described_as(
            f"chain {from_id} contains link to {to_node_id}"
        ).contains(to_node_id)


def assert_rule_links_to(tree: RootNode, rule_id: str, to_node_id: str):
    rule = next((x for x in tree.rules if x.id == rule_id), None)
    assert rule
    assert_that(rule.links).described_as(
        f"rule {rule_id} contains link to {to_node_id}"
    ).extracting("end").extracting("id").contains(to_node_id)


class TestBasics:
    def test_make_tree_when_using_default_rules_set_when_not_using_filters_default_sections_are_present_in_tree(
        self, default_tree_root: RootNode
    ):
        tree = default_tree_root

        assert_that(tree.sections).is_length(4).extracting("name").contains_only(
            "raw", "mangle", "filter", "nat"
        )
        assert_that(tree.sections).extracting("tree_root").contains_only(tree)
        assert_that(tree.sections).extracting("parent").contains_only(tree)

    def test_make_tree_when_using_default_rules_set_when_not_using_filters_default_chains_are_present_in_tree(
        self, default_tree_root: RootNode
    ):
        tree = default_tree_root

        assert_that(tree.chains).is_length(18).extracting("name").contains(
            "prerouting", "srcnat", "dstnat", "input", "output"
        )
        assert_that(tree.chains).extracting("tree_root").contains_only(tree)

    def test_make_tree_when_using_default_rules_set_when_not_using_filters_default_rules_are_present_in_tree(
        self, default_tree_root: RootNode
    ):
        tree = default_tree_root

        assert_that(tree.rules).is_length(length=32).extracting("tree_root").contains_only(tree)

    def test_make_tree_when_using_section_filter_filter_section_elements(
        self, default_rule_list: list[ParserRule]
    ):
        sut = TreeMaker(default_rule_list, elements=[GraphSelector("filter")])
        tree = sut.make_tree()

        assert_that(tree.sections).extracting("name").contains("filter")
        section = tree.sections[0]
        assert_that(tree.chains).extracting("parent").contains(section)

    def test_make_tree_when_using_chains_filter_elements_for_filtered_chain(
        self, default_rule_list: list[ParserRule]
    ):
        sut = TreeMaker(default_rule_list, elements=[GraphSelector(":input")])
        tree = sut.make_tree()

        assert_that(tree.sections).extracting("name").contains("filter", "mangle")
        assert_that(tree.chains).extracting("name").contains("input", "terminal-chain")
        assert_that(tree.rules).extracting("chain").contains("input", "terminal-chain")

    def test_make_tree_when_prune_disabled_rules_is_true_disabled_rules_are_removed(
        self, default_rule_list: list[ParserRule]
    ):
        sut = TreeMaker(default_rule_list, elements=[], prune_disabled_rules=True)
        tree = sut.make_tree()

        assert_that(tree.rules).extracting("disabled").does_not_contain("yes")
        assert_that(tree.rules).is_not_empty()

    def test_make_tree_when_prune_log_rules_is_true_log_rules_are_removed(
        self, default_rule_list: list[ParserRule]
    ):
        sut = TreeMaker(default_rule_list, elements=[], prune_log_rules=True)
        tree = sut.make_tree()

        assert_that(tree.rules).extracting("action").does_not_contain("log")

    def test_make_tree_rules_index_follows_order_in_file(
        self, rule_list_ordered_tests: list[ParserRule]
    ):
        sut = TreeMaker(rule_list_ordered_tests, elements=[])
        tree = sut.make_tree()
        for rule in tree.rules:
            assert_that(str(rule.index)).is_equal_to(rule.comment)

    def test_make_tree_when_selector_has_remove_prune_elements(
        self, default_rule_list: list[ParserRule]
    ):
        sut = TreeMaker(
            default_rule_list,
            elements=[
                GraphSelector("nat:dstnat", "-"),
                GraphSelector("nat"),
                GraphSelector("filter", mod="-"),
            ],
        )
        tree = sut.make_tree()

        assert_that(tree.sections).extracting("name").does_not_contain("filter")
        assert_that(tree.sections).extracting("name").contains("nat")
        assert_that(tree.chains).extracting("id").does_not_contain("nat:dstnat")

    def test_multiple_selector_picks_first_one(self, default_rule_list: list[ParserRule]):
        sut = TreeMaker(
            default_rule_list,
            elements=[
                GraphSelector("nat:dstnat", "+"),
                GraphSelector("nat:dstnat", "-"),
            ],
        )
        tree = sut.make_tree()

        assert_that(tree.sections).extracting("name").contains("nat")
        assert_that(tree.chains).extracting("id").contains("nat:dstnat")


class TestLinking:
    def test_make_tree_when_using_default_rules_set_rules_are_wired_in_chains_based_on_rule_index(
        self, default_tree_root: RootNode
    ):
        # Act
        tree = default_tree_root

        # Assert
        section = tree.section("filter")
        assert section
        chain = section.chain("rule-order")
        assert chain
        rules = {x.comment: x for x in chain.rules}
        assert_rule_links_to(tree, rules["1"].id, rules["2"].id)
        assert_rule_links_to(tree, rules["2"].id, rules["3"].id)
        assert_rule_links_to(tree, rules["3"].id, rules["4"].id)

    def test_make_tree_when_using_default_rules_set_rules_are_wired_according_packet_flows(
        self, default_tree_root: RootNode
    ):
        tree = default_tree_root

        # prerouting
        assert_links_to(tree, "raw:prerouting", "mangle:prerouting")
        assert_links_to(tree, "mangle:prerouting", "nat:dstnat")

        # input
        assert_links_to(tree, "mangle:input", "filter:input")

        # forward
        assert_links_to(tree, "mangle:forward", "filter:forward")

        # output
        assert_links_to(tree, "raw:output", "mangle:output")
        assert_links_to(tree, "mangle:output", "filter:output")

        # postrouting
        assert_links_to(tree, "mangle:postrouting", "nat:srcnat")

    def test_make_tree_when_using_default_rules_set_jump_rules_links_to_target_in_chain(
        self, default_tree_root: RootNode
    ):
        tree = default_tree_root

        # find the rule
        section = tree.section("filter")
        assert section
        chain = section.chain("forward")
        assert chain
        rule: Rule = [
            x for x in chain.rules if x.action == "jump" and x.jump_target == "jump-chain"
        ][0]
        assert rule
        links = rule.links
        assert_that(links).is_length(1)
        target_chain = links[0].end.parent
        assert_that(target_chain).has_name("jump-chain").has_parent(rule.parent.parent)

    def test_implicit_returning_nodes_create_link_to_next_rule_in_parent_chain(
        self, default_tree_root: RootNode
    ):
        tree = default_tree_root
        section = tree.section("filter")
        assert section

        # target end rule
        return_chain = section.chain("forward")
        assert return_chain
        return_rule = [x for x in return_chain.rules if x.action != "jump"][2]
        assert return_rule

        # find the returning rule
        chain = section.chain("jump-chain")
        assert chain
        rule = chain.rules[-1]
        links = rule.links
        assert_that(links).is_length(1)
        assert_that(links[0].end).is_equal_to(return_rule)

    def test_make_tree_when_using_default_rules_set_terminal_node_in_flow_chain_has_link_to_next_section(
        self, default_tree_root: RootNode
    ):
        tree = default_tree_root

        # find the rule
        assert_links_to(tree, "filter:forward", "mangle:postrouting")

    def test_when_node_has_matchers_then_links_include_yes_no_label(
        self, default_tree_root: RootNode
    ):
        # Assert
        section = default_tree_root.section("filter")
        assert section
        chain = section.chain("input")
        assert chain

        rule: Rule = next(
            x for x in chain.rules if x.action == "jump" and x.jump_target == "terminal-chain"
        )
        links = rule.links
        assert_that(links).is_length(2)

        no_link = next(x for x in links if x.end.parent == chain)
        yes_link = next(x for x in links if x.end.parent != chain)

        # link to next rule has otherwise as label
        assert_that(no_link).has_label("otherwise")

        # rule to jump chain has if yes
        assert_that(yes_link).has_label("if yes")

    def test_when_start_node_is_return_link_has_return_as_label(self, default_tree_root: RootNode):
        # Assert
        section = default_tree_root.section("filter")
        assert section
        chain = section.chain("jump-chain")
        assert chain

        rule: Rule = next(x for x in chain.rules if x.action == "return")
        links = rule.links
        assert_that(links).is_length(2)

        return_link = next(x for x in links if x.end.parent != chain)

        # rule to jump chain has if yes
        assert_that(return_link).has_label("return")

    def test_when_start_node_nonterminal_end_node_link_has_return_as_label(
        self, default_tree_root: RootNode
    ):
        # Assert
        section = default_tree_root.section("filter")
        assert section
        chain = section.chain("jump-chain")
        assert chain

        rule: Rule = chain.rules[1]
        assert_that(rule.links[0]).has_label("return")


class TestAddFlowElements:
    def test_make_tree_when_add_flow_elements_is_true_when_using_filters_adds_flow_elements_that_connect_to_filtered_elements(
        self, add_flow_rule_list
    ):
        tree = TreeMaker(
            add_flow_rule_list, [GraphSelector("filter:input")], add_flow_elements=True
        ).make_tree()

        assert_that(tree.sections).extracting("name").contains_only(
            "raw", "mangle", "filter", "nat"
        )
        assert_that(tree.sections).extracting("parent").contains_only(tree)

    def test_make_tree_when_add_flow_elements_is_true_when_flow_elements_added_flow_chain_contains_dummy_rule(
        self, add_flow_rule_list
    ):
        tree = TreeMaker(
            add_flow_rule_list, [GraphSelector("filter:input")], add_flow_elements=True
        ).make_tree()

        section = tree.section("mangle")
        assert section
        chain = section.chain("prerouting")
        assert chain
        assert_that(chain.rules).extracting("comment").contains_only("rule added for packet flow")

    def test_flow_elements_links_in_flow_chains(self, default_rule_list):
        # Arrange
        sut = TreeMaker(default_rule_list, [], add_flow_elements=True)

        # Act
        tree = sut.make_tree()

        # Assert
        # pretouting flow - raw:prerouting -> mangle:prerouting -> nat:dstnat
        assert_links_to(tree, "raw:prerouting", "mangle:prerouting")
        assert_links_to(tree, "mangle:prerouting", "nat:dstnat")

        # input flow - mangle:input -> filter:input
        assert_links_to(tree, "mangle:input", "filter:input")

        # forward flow - mangle:forward -> filter:forward
        assert_links_to(tree, "mangle:forward", "filter:forward")

        # output flow - raw:output -> mangle:output -> filter:output
        assert_links_to(tree, "raw:output", "mangle:output")
        assert_links_to(tree, "mangle:output", "filter:output")

        # postrouting flow - mangle:postrouting -> nat:srcnat
        assert_links_to(tree, "mangle:postrouting", "nat:srcnat")

    def test_flow_elements_links_between_flow_chains(self, default_rule_list):
        # Arrange
        sut = TreeMaker(default_rule_list, [], add_flow_elements=True)

        # Act
        tree = sut.make_tree()

        # Assert
        # prerouting -> input
        assert_links_to(tree, "nat:dstnat", "mangle:input", LINK_STYLE_INVISIBLE)
        assert_links_to(tree, "nat:dstnat:rule2", "mangle:input:rule3")

        # prerouting -> forward
        assert_links_to(tree, "nat:dstnat", "mangle:forward", LINK_STYLE_INVISIBLE)
        assert_links_to(tree, "nat:dstnat:rule2", "mangle:forward:rule9")

        # forward -> postrouting
        assert_links_to(tree, "filter:forward", "mangle:postrouting", LINK_STYLE_INVISIBLE)
        assert_links_to(tree, "filter:forward:rule15", "mangle:postrouting:rule19")

        # output -> postrouting
        assert_links_to(tree, "filter:output", "mangle:postrouting", LINK_STYLE_INVISIBLE)
        assert_links_to(tree, "filter:output:rule18", "mangle:postrouting:rule19")

    def test_ensure_all_flow_element_depending_on_graph_chain(self, add_flow_rule_list):
        tree = TreeMaker(
            add_flow_rule_list,
            [GraphSelector("filter:input"), GraphSelector(":", "-")],
            add_flow_elements=True,
        ).make_tree()

        assert_that(tree.chains).extracting("id").contains_only(
            "raw:prerouting", "mangle:prerouting", "nat:dstnat", "mangle:input", "filter:input"
        )

    def test_ensure_all_flow_element_linked_properly(self, add_flow_rule_list):
        tree = TreeMaker(
            add_flow_rule_list, [GraphSelector("filter:input")], add_flow_elements=True
        ).make_tree()

        # Assert
        # raw ->|prerouting| mangle ->|dstnat| nat ->|input| mangle ->|input| filter
        assert_links_to(tree, "raw:prerouting:rule3", "mangle:prerouting:rule4")
        assert_links_to(tree, "mangle:prerouting:rule4", "nat:dstnat:rule2")
        assert_links_to(tree, "nat:dstnat:rule2", "mangle:input:rule5")
        assert_links_to(tree, "mangle:input:rule5", "filter:input:rule0")
